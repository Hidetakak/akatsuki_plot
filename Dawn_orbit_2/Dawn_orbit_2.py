# -*- coding: utf-8 -*-

import os
import sys
import csv
import datetime

import numpy as np
from numpy.linalg import norm
from scipy.integrate import ode
import scipy.interpolate as intp
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker

import spiceypy as spice

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbit
import SpiceyPyExt as spiceExt


# 3Dの投影方法を平行投影にするには、↓のコメントアウトを解除
# デフォルトでは透視投影となっている
# from mpl_toolkits.mplot3d import proj3d
# def orthogonal_transformation(zfront, zback):
#     a = 2/(zfront-zback)
#     b = -1*(zfront+zback)/(zfront-zback)
#     c = zback
#     return np.array([[1,0,0,0],
#                      [0,1,0,0],
#                      [0,0,a,b],
#                      [0,0,0,c]])
# proj3d.persp_transformation = orthogonal_transformation

# 元データ
KERNEL_LIST_FILENAME = "kernel_list.txt" # SPICE Kernelリスト
ORBITAL_ELEMENT_FILENAME = "dawn_orbital_element_for_plot.csv" # Dawnの軌道要素のファイル名
PLANET_ORBITAL_ELEMENT_FILENAME = "planet_orbital_element.csv" # 惑星の軌道要素のファイル名


# イベント日時
EVENT_TIMING_DT = {'LAUNCH' : datetime.datetime(2007, 9, 27, 13, 0, 0), # SPICEデータ記録開始
                   'IES_DRIVE_START' : datetime.datetime(2007, 10, 6), # イオンエンジン運転開始
                   'IES_CUT_OFF_BEFORE_MGA' : datetime.datetime(2008,11,21), # 火星スウィングバイ前のイオンエンジン停止
                   'MGA' : datetime.datetime(2009,2,18, 0, 27, 58), # 火星スウィングバイ
                   'IES_IGN_AFTER_MGA' : datetime.datetime(2009,6,7), # 火星スウィングバイ後のイオンエンジン運転再開
                   'VESTA_ARR' : datetime.datetime(2011,7,16), # Vesta到着
                   'VESTA_DEP' : datetime.datetime(2012,9,5),  # Vesta出発                   
                   'CERES_ARR' : datetime.datetime(2015,3,6),  # Ceres到着
                   'END' : datetime.datetime(2016,6,17)}       # SPICEデータ記録終了
EVENT_TIMING_ET = {}            # EVENT_TIMING_DTをETに変換したもの

# SPICEカーネルで用いる名称
#   ターゲット名
SS_BARYCENTER = "SOLAR SYSTEM BARYCENTER" # 太陽系の重心
DAWN = "DAWN"                   # DAWN探査機
SUN = "SUN"                     # 太陽
EARTH = "EARTH BARYCENTER"      # 地球（地球、月の重心）
MARS = "MARS BARYCENTER"        # 火星（火星と衛星の重心）
JUPITER = "JUPITER BARYCENTER"   # 木星（木星と衛星の重心）
CERES = "1 CERES"               # セレス
VESTA = "4 VESTA"               # ヴェスタ
#   座標系の名前
INERTIAL_FRAME = "ECLIPJ2000"     # 慣性座標系(黄道座標系)
DAWN_FRAME = "DAWN_SPACECRAFT"    # DAWNの座標系
DAWN_FT_FRAME = ["DAWN_FT1",      # DAWNのイオンエンジン3つの座標系
                 "DAWN_FT2",
                 "DAWN_FT3"]
DAWN_SA_FRAME = ['DAWN_SA+Y', 'DAWN_SA-Y'] # DAWNの太陽電池パネルの座標系

# 計算条件
DELTA_T_CRUISE_CALC = 1 * 60 * 60       # 惑星間軌道での計算間隔[sec]
DELTA_T_ASTR_CALC = 60 * 10             # 小惑星周回中の計算間隔[sec]
DELTA_T_MGA_CALC = 10                   # 火星スウィングバイ中の計算間隔[sec]
MGA_TIME_SPAN = 6 * 60 * 60             # 火星スウィングバイの計算期間[±sec]

# 重力源情報 (SPICEデータの名称、GM値[km^3 / s^-2]
G_SOURCES = [{'name':SUN,   'GM':132712440041.939400},
             {'name':EARTH, 'GM':398600.435436},
             {'name':MARS,  'GM':42828.375214},
             {'name':JUPITER, 'GM':126712764.800000},
             {'name':CERES, 'GM':62.809393},
             {'name':VESTA, 'GM':17.288009}]

# イオンエンジン運転判定の閾値
DELTA_V_IES_MIN_TH = 0.001           # [km/s/Day] イオンエンジンによる増速量の下限値
DELTA_V_IES_MAX_TH = 0.008           # [km/s/Day] イオンエンジンによる増速量の上限値
# イオンエンジンの運転期間
IES_DRIVE_PERIOD_DT = [[EVENT_TIMING_DT['IES_DRIVE_START'], EVENT_TIMING_DT['IES_CUT_OFF_BEFORE_MGA']],
                       [EVENT_TIMING_DT['IES_IGN_AFTER_MGA'], EVENT_TIMING_DT['VESTA_ARR']],
                       [EVENT_TIMING_DT['VESTA_DEP'], EVENT_TIMING_DT['CERES_ARR']]]
IES_DRIVE_PERIOD_ET = []        # IES_DRIVE_PERIOD_DTをETに変換したもの

DV_THRST_ANGLE_TH = 2.5         # インエンジンの噴射方向とΔVの角度の閾値[deg]
                                # この角度より小さければ、イオンエンジンを使用していると判定する
DV_VESTA_ORBIT = 0.23           # Vesta軌道でのイオンエンジンによる増速量[km/s]
DV_CERES_ORBIT = 0.48           # Ceres軌道でのイオンエンジンによる増速量[km/s]
                                
# 描画のセッティング
#  ファイル名
CRUISE_PLOT_FILENAME = "CRUISE_" # 惑星間軌道でのプロットのファイル名
LOCAL_PLOT_FILENAME = "LOCAL_"   # 小惑星周辺のプロットのファイル名
MGA_PLOT_FILENAME = "MGA_"       # 火星スウィングバイ中のファイル名
# プロット間隔
DELTA_T_CRUISE_PLOT = 6 * 60 * 60            # 惑星間軌道での描画間隔[sec]
DELTA_T_ASTR_PLOT = 3 * 60 * 60              # 小惑星周回中の描画間隔[sec]
DELTA_T_MGA_PLOT = 30                        # 火星スウィングバイ中の描画間隔[sec]
GRADATION_LEVEL_ASTR_PLOT = 10               # 小惑星周回中に軌跡を何段階でグラデーションを掛けるか
DISPLAY_PERIOD_ASTR_PLOT = 5 * 24 * 60 * 60  # 小惑星周回中に軌跡を表示する時間[sec]
# 増速量のプロットスケール
DV_PLOT_SCALE = 1.5e8 / 0.01      # 0.01[km/s/Day]が1.5e8[km]の長さにプロットされるように
DV_ARROW_HEAD_WIDTH = 1.5e7       # 増速量矢印の頭の幅
# 色(R,G,B)
#  face : 天体・探査機のマークの塗りつぶし色、edge : マークの輪郭色、track : 軌跡の色
PLANET_COLORS = {SUN:{'face':[1, 1, 0],
                        'edge':[1, 1, 0],
                        'track':[1, 1, 0]},
                 EARTH:{'face':[0.19, 0.52, 0.61],
                          'edge':[0.57, 0.8, 0.86],
                          'track':[0.19, 0.52, 0.61]},
                 MARS:{'face':[0.58, 0.21, 0.21],
                         'edge':[0.85, 0.59, 0.58],
                         'track':[0.58, 0.21, 0.21]},
                 JUPITER:{'face':[0, 0, 0, 0],
                          'edge':[0, 0, 0, 0],
                          'track':[0, 0, 0, 0]},
                 VESTA:{'face':[0.46, 0.58, 0.23],
                          'edge':[0.76, 0.84, 0.61],
                          'track':[0.46, 0.58, 0.23]},
                 CERES:{'face':[0.375, 0.29, 0.48],
                          'edge':[0.7, 0.63, 0.78],
                          'track':[0.375, 0.29, 0.48]}}
DAWN_COLOR = {'face':[0.96, 0.59, 0.27],
              'edge':[0.96, 0.59, 0.27],
              'track':[0.96, 0.59, 0.27]}
#  IESの状態による色分け 0 : IES運転休止中、1～3 : IES運転中、4 : 小惑星周回中
IES_TEXT_COLOR = [[0.5, 0.5, 0.5], [1, 0.5, 0.5], [0.5, 1, 0.5], [0.5, 0.5, 1], [0.5, 0.5, 0.5]]
IES_TRACK_COLOR = [[0.3, 0.3, 0.3], [1, 0.13, 0.13], [0.13, 1, 0.13], [0.13, 0.13, 1], [1, 1, 1, 0]]
IES_ARROW_COLOR = IES_TRACK_COLOR
# 描画に使う惑星の半径情報[km]
RADIUS = {MARS:3397, VESTA:250, CERES:470}

# Blender用の情報
BLENDER_EXPORT_FILENAME = "datas_for_blender.csv" # ブレンダー用の数値情報ファイル名


def d_dt_state(t, state_v):
    """
    state vector(位置と速度の6要素からなるベクトル)の微分を与える
    :param t:時刻[t]
    :type t: float
    :param state_v:state vector
    :type state_v: numpy.ndarray
    :return d_state_v: state_vの時間微分
    :type d_state_v: numpy.ndarray
    """

    d_state_v = np.zeros(6)     # state_vの微分
    d_state_v[:3] = state_v[3:] # 位置の微分 = 速度

    for g_source in G_SOURCES:  # それぞれの重力源について計算を行う
        pos = spiceExt.spkezr(targ = g_source['name'],
                              et = t,
                              ref = INERTIAL_FRAME,
                              abcorr = "NONE",
                              obs = SS_BARYCENTER)[0][:3] # 重力源の位置
        pos_from_g = state_v[:3] - pos          # 重力源基準の探査機の位置
        d_state_v[3:] += -(g_source['GM'] * pos_from_g) / norm(pos_from_g) ** 3 # 重力源によって探査機に生じる加速度

    return d_state_v


def calc_pos_v():
    """
    Dawn探査機、天体の位置と速度を求める
    :return:
        Dawn探査機の位置、速度（慣性座標系)
        惑星の位置（慣性座標系)
    :rtype: tuple
    """
    # 計算を行う時刻の一覧を作成
    et_array = np.arange(EVENT_TIMING_ET['LAUNCH'],
                         EVENT_TIMING_ET['END'],
                         DELTA_T_CRUISE_CALC)

    # 探査機の位置速度を求める（慣性座標系基準）
    print("Dawn探査機の位置、速度を抽出中...", end = "", flush = True)
    dawn_pv_in_i_frame = spiceExt.spkezr(targ = DAWN, et = et_array,
                                         ref = INERTIAL_FRAME, abcorr = "NONE",
                                         obs = SS_BARYCENTER)[0] # DAWNの位置、速度
    print("完了")
    # 天体の位置を求める（慣性座標系基準）
    print("惑星の位置、速度を抽出中...", end = "", flush = True)
    planet_pos_in_i_frame = {}
    for planet in G_SOURCES[1:]: # 各天体(太陽を除く)の位置を求める（慣性座標系基準）
        pos = spiceExt.spkezr(targ = planet['name'], et = et_array,
                              ref = INERTIAL_FRAME, abcorr = "NONE",
                              obs = SS_BARYCENTER)[0]
        planet_pos_in_i_frame[planet['name']] = pos
    print("完了")

    return et_array, dawn_pv_in_i_frame, planet_pos_in_i_frame
    

def calc_ies_status2(dawn_pv_in_i_frame, planet_pv_in_i_frame, et_array):
    n = et_array.shape[0]
    acc = np.zeros((n, 3))
    for xyz in range(3):
        y_intp = intp.splrep(et_array, dawn_pv_in_i_frame[:, xyz + 3])
        dy_intp = intp.splder(y_intp, 1)
        acc[:, xyz] = intp.splev(et_array, dy_intp)
    # 惑星から働く重力による加速度を求める
    acc_from_planet = np.zeros((n, 3))
    for g_source in G_SOURCES[1:]:
        r = planet_pv_in_i_frame[g_source['name']][:, :3] - dawn_pv_in_i_frame[:, :3]
        r_abs = norm(r, axis = 1).reshape((n, 1))
        acc_from_planet += g_source['GM'] * r / r_abs ** 3
    # 太陽から働く重力による加速度を求める
    r = -dawn_pv_in_i_frame[:, :3]
    r_abs = norm(r, axis = 1).reshape((n, 1))
    acc_from_planet += G_SOURCES[0]['GM'] * r / r_abs ** 3

    dawn_dv_in_i_frame = acc - acc_from_planet
    dawn_dv_in_i_frame *= (24 * 60 * 60) # 1日あたりの増速量[km/sec/day]
    # 増速量が過大、過小の場合はイオンエンジンの噴射ではないと判断する
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1)                  # 増速量のノルム
    dawn_dv_in_i_frame[(norm_dv < DELTA_V_IES_MIN_TH) + (DELTA_V_IES_MAX_TH < norm_dv), :] = 0
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1)                  # 増速量のノルムを更新
    # イオンエンジンの運転期間以外はイオンエンジンの噴射ではないと判断する
    dawn_dv_in_i_frame[((et_array < IES_DRIVE_PERIOD_ET[0][0])
                        + ((et_array > IES_DRIVE_PERIOD_ET[0][1]) * (et_array < IES_DRIVE_PERIOD_ET[1][0]))
                        + ((et_array > IES_DRIVE_PERIOD_ET[1][1]) * (et_array < IES_DRIVE_PERIOD_ET[2][0]))
                        + (et_array > IES_DRIVE_PERIOD_ET[2][1])), :] = 0
    print("完了")
    
    # Dawn探査機の姿勢を抽出する
    print("Dawn探査機の姿勢データを抽出中...")
    i_frame_to_dawn_frame = spiceExt.pxform(INERTIAL_FRAME, DAWN_FRAME, et_array) # 慣性座標系からDawn座標系への変換行列
    print("完了")

    print("使用したイオンエンジンを特定中...", end = "", flush = True)
    # Dawn探査機座標系での増速量を計算する
    dawn_dv_in_dawn_frame = np.zeros((et_array.shape[0], 3)) # Dawn座標系でのΔV
    for idx in range(et_array.shape[0]):
        dawn_dv_in_dawn_frame[idx, :] = i_frame_to_dawn_frame[idx, :, :].dot(dawn_dv_in_i_frame[idx, :3].T).ravel()

    # イオンエンジンの噴射方向と増速量の間の角度を計算する
    n_ies = len(DAWN_FT_FRAME)   # Dawnについているイオンエンジンの数
    angle_between_dv_ft = np.zeros((et_array.shape[0], n_ies)) # 加速方向と各イオンエンジンとの間の角度
    for ft_num, ft_frame in enumerate(DAWN_FT_FRAME):
        # Dawn座標系での、各イオンエンジンの推力の方向
        # 推力方向は、各イオンエンジンの座標系では(0, 0, -1)方向→Dawn座標系に変換する
        rotate_matrix = spiceExt.pxform(ft_frame, DAWN_FRAME, EVENT_TIMING_ET['LAUNCH'])
        thrust_in_dawn_frame = rotate_matrix.dot(np.array([[0],[0],[-1]]))
        # 増速量と推力方向の角度を計算する
        inner_product = np.sum((dawn_dv_in_dawn_frame * thrust_in_dawn_frame.T) , axis = 1) # ベクトルの内積
        angle = np.rad2deg(np.arccos(inner_product / norm_dv)) # 角度 = acos(内積 / ノルムの積) [deg]
        angle_between_dv_ft[:, ft_num] = angle
    plt.plot(et_array, angle_between_dv_ft, marker = "o")
    plt.show()
        
    # イオンエンジンの噴射ステータスを判定する
    # 0: イオンエンジン停止
    # 1～: 運転しているイオンエンジンの番号
    # 4: 小惑星周回中
    ies_status = np.zeros((et_array.shape[0]), dtype = int)
    # 小惑星周回中の場合をマークする
    ies_status[(EVENT_TIMING_ET['VESTA_ARR'] < et_array)
               * (et_array < EVENT_TIMING_ET['VESTA_DEP'])] = 4
    ies_status[EVENT_TIMING_ET['CERES_ARR'] < et_array] = 4
    # 各エンジンの噴射方向と増速量の方向が一致していたら、そのイオンエンジンが運転されていると判断する
    for ft_num in range(len(DAWN_FT_FRAME)):
        ies_status[angle_between_dv_ft[:, ft_num] < DV_THRST_ANGLE_TH] = ft_num + 1
        
    # 増速量の情報を更新する
    # 増速量のベクトルがどのエンジンの噴射方向とも一致しない場合は、
    # イオンエンジンによる増速ではないと判断する→増速量を0に書き換える
    dawn_dv_in_i_frame[ies_status == 0, :] = 0
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1) # 増速量のノルムを更新
    # ベスタ軌道での増速量を反映させる(ベスタ軌道離脱時に加算する)
    norm_dv[np.searchsorted(et_array, EVENT_TIMING_ET['VESTA_DEP'])] = DV_VESTA_ORBIT / DELTA_T_CRUISE_CALC * (24 * 60 * 60)

    # 増速量の累計値を求める
    total_dv_by_ies = np.zeros_like(norm_dv)
    for idx in range(total_dv_by_ies.shape[0]):
        total_dv_by_ies[idx] = np.sum(norm_dv[:idx + 1])
    total_dv_by_ies *= DELTA_T_CRUISE_CALC / (24 * 60 * 60)
    plt.plot(et_array, total_dv_by_ies)
    plt.show()
    print("完了")

    return dawn_dv_in_i_frame, total_dv_by_ies, i_frame_to_dawn_frame, ies_status
    
    
    # return dawn_dv_in_i_frame, total_dv_by_ies, i_frame_to_dawn_frame, ies_status



def calc_ies_status(dawn_pv_in_i_frame, et_array):
    """
    Dawn探査機のイオンエンジンの運転状態を計算する
    :param dawn_pv_in_i_frame: Dawn探査機の位置と速度（慣性座標系)
    :type dawn_pv_in_i_frame: numpy.ndarray
    :param et_array: 計算を行う時刻の一覧（SpiceのEphemeris Time）
    :type et_array: numpy.ndarray
    :return:
        Dawn探査機の各時刻における増速量 [km/s/Day]
        Dawn探査機のΔV合計値 [km/s]
        Dawn探査機の姿勢を表現する行列
        Dawn探査機のイオンエンジン運転状態
    :return type: tuple
    """

    n = dawn_pv_in_i_frame.shape[0] # 計算点数
    
    # 探査機の増速量を求める
    print("Dawn探査機のイオンエンジンによる増速量を計算中...", end = "", flush = True)
    dawn_dv_in_i_frame = np.zeros((n, 3)) # Dawn探査機の増速量（慣性座標系）
    for idx in range(1, n):
        is_IES_period = False
        for period in IES_DRIVE_PERIOD_ET: # イオンエンジン運転期間中か判定をする
            if period[0] <= et_array[idx - 1] and et_array[idx - 1] <= period[1]:
                is_IES_period = True
                break
        if is_IES_period == False: # イオンエンジン運転期間中でない場合は計算をスキップする
            continue
        # 常微分方程式により1ステップ分慣性飛行をした場合の速度を求める
        ode_rk = ode(d_dt_state)
        ode_rk.set_initial_value(y = dawn_pv_in_i_frame[idx - 1, :], t = et_array[idx - 1]) # 1つ前のステップを初期条件とする
        ode_rk.integrate(t = et_array[idx]) # 初期条件から現在の位置速度を予測する
        pv_wo_delta_v = ode_rk.y         # 噴射を行わない場合の位置、速度
        # 増速量 = SPICEデータから抽出した速度 - 慣性飛行の場合の速度
        dawn_dv_in_i_frame[idx, :] = dawn_pv_in_i_frame[idx, 3:] - pv_wo_delta_v[3:]
        del ode_rk
        
    dawn_dv_in_i_frame /= (DELTA_T_CRUISE_CALC / (24 * 60 * 60)) # 1日あたりの増速量で正規化
    
    # 増速量が過大、過小の場合はイオンエンジンの噴射ではないと判断する
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1)                  # 増速量のノルム
    dawn_dv_in_i_frame[(norm_dv < DELTA_V_IES_MIN_TH) + (DELTA_V_IES_MAX_TH < norm_dv), :] = 0
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1)                  # 増速量のノルムを更新
    print("完了")
    
    # Dawn探査機の姿勢を抽出する
    print("Dawn探査機の姿勢データを抽出中...")
    i_frame_to_dawn_frame = spiceExt.pxform(INERTIAL_FRAME, DAWN_FRAME, et_array) # 慣性座標系からDawn座標系への変換行列
    print("完了")

    print("使用したイオンエンジンを特定中...", end = "", flush = True)
    # Dawn探査機座標系での増速量を計算する
    dawn_dv_in_dawn_frame = np.zeros((et_array.shape[0], 3)) # Dawn座標系でのΔV
    for idx in range(et_array.shape[0]):
        dawn_dv_in_dawn_frame[idx, :] = i_frame_to_dawn_frame[idx, :, :].dot(dawn_dv_in_i_frame[idx, :3].T).ravel()

    # イオンエンジンの噴射方向と増速量の間の角度を計算する
    n_ies = len(DAWN_FT_FRAME)   # Dawnについているイオンエンジンの数
    angle_between_dv_ft = np.zeros((et_array.shape[0], n_ies)) # 加速方向と各イオンエンジンとの間の角度
    for ft_num, ft_frame in enumerate(DAWN_FT_FRAME):
        # Dawn座標系での、各イオンエンジンの推力の方向
        # 推力方向は、各イオンエンジンの座標系では(0, 0, -1)方向→Dawn座標系に変換する
        rotate_matrix = spiceExt.pxform(ft_frame, DAWN_FRAME, EVENT_TIMING_ET['LAUNCH'])
        thrust_in_dawn_frame = rotate_matrix.dot(np.array([[0],[0],[-1]]))
        # 増速量と推力方向の角度を計算する
        inner_product = np.sum((dawn_dv_in_dawn_frame * thrust_in_dawn_frame.T) , axis = 1) # ベクトルの内積
        angle = np.rad2deg(np.arccos(inner_product / norm_dv)) # 角度 = acos(内積 / ノルムの積) [deg]
        angle_between_dv_ft[:, ft_num] = angle
    plt.plot(et_array, angle_between_dv_ft, marker = "o")
    plt.show()
        
    # イオンエンジンの噴射ステータスを判定する
    # 0: イオンエンジン停止
    # 1～: 運転しているイオンエンジンの番号
    # 4: 小惑星周回中
    ies_status = np.zeros((et_array.shape[0]), dtype = int)
    # 小惑星周回中の場合をマークする
    ies_status[(EVENT_TIMING_ET['VESTA_ARR'] < et_array)
               * (et_array < EVENT_TIMING_ET['VESTA_DEP'])] = 4
    ies_status[EVENT_TIMING_ET['CERES_ARR'] < et_array] = 4
    # 各エンジンの噴射方向と増速量の方向が一致していたら、そのイオンエンジンが運転されていると判断する
    for ft_num in range(len(DAWN_FT_FRAME)):
        ies_status[angle_between_dv_ft[:, ft_num] < DV_THRST_ANGLE_TH] = ft_num + 1
        
    # 増速量の情報を更新する
    # 増速量のベクトルがどのエンジンの噴射方向とも一致しない場合は、
    # イオンエンジンによる増速ではないと判断する→増速量を0に書き換える
    dawn_dv_in_i_frame[ies_status == 0, :] = 0
    norm_dv = norm(dawn_dv_in_i_frame, axis = 1) # 増速量のノルムを更新
    # ベスタ軌道での増速量を反映させる(ベスタ軌道離脱時に加算する)
    norm_dv[np.searchsorted(et_array, EVENT_TIMING_ET['VESTA_DEP'])] = DV_VESTA_ORBIT / DELTA_T_CRUISE_CALC * (24 * 60 * 60)

    # 増速量の累計値を求める
    total_dv_by_ies = np.zeros_like(norm_dv)
    for idx in range(total_dv_by_ies.shape[0]):
        total_dv_by_ies[idx] = np.sum(norm_dv[:idx + 1])
    total_dv_by_ies *= DELTA_T_CRUISE_CALC / (24 * 60 * 60)
    print("完了")

    return dawn_dv_in_i_frame, total_dv_by_ies, i_frame_to_dawn_frame, ies_status
    

def calc_pos_v_asteroid(asteroid, start_et, end_et):
    """
    小惑星周回起動中の探査機の位置、速度を求める
    :param asteroid: 小惑星の名前
    :type asteroid: str
    :param start_et: 計算開始時刻（ET）
    :type start_et: float
    :param end_et: 計算終了時刻（ET）
    :type end_et: float
    :return:
      et_array : 計算を実施した時刻の一覧
      dawn_pos_v_local : 小惑星に対するDawnの位置、速度
    :return type: tuple
    """
    # 計算時刻の準備
    et_array = np.arange(start_et, end_et, DELTA_T_ASTR_CALC)

    # 位置速度計算
    dawn_pos_v_local = spiceExt.spkezr(targ = DAWN, et = et_array,
                                       ref = INERTIAL_FRAME, abcorr = "NONE",
                                       obs = asteroid)[0]
    return et_array, dawn_pos_v_local


def _local_plot(et_array, pos_v, save_path, filename, astr_color, axis_limit, astr_r):
    """
    小惑星近傍の軌跡を作成し保存する
    :param et_array: 時刻の一覧
    :type et_array: numpy.ndarray
    :param pos_v: 探査機の位置、速度
    :type pos_v: numpy.ndarray
    :param save_path: 保存先のパス
    :type save_path: str
    :param filename: ファイル名
    :type filename: str
    :param astr_color: 小惑星の描画色
    :type astr_color:tuple
    :param axis_limit: 各軸の範囲
    :type axis_limit: list
    :param astr_r: 小惑星の半径[km] 0の時はmarker'o'で代用する
    :type astr_r: float
    """

    # 保存先のフォルダがなかったら作成する
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)
    
    # 描画間隔の計算をする
    plot_idx_step = int(DELTA_T_ASTR_PLOT / DELTA_T_ASTR_CALC) # 計算済みデータをいくつおきに描画するか
    # グラデーション1階調あたりの計算済みデータの数
    plot_idx_step_gradation = int(DISPLAY_PERIOD_ASTR_PLOT / GRADATION_LEVEL_ASTR_PLOT / DELTA_T_ASTR_CALC)
    
    # グラデーションの色の計算
    dawn_track_color = []
    for level in range(GRADATION_LEVEL_ASTR_PLOT):
        alpha = 1.0 / GRADATION_LEVEL_ASTR_PLOT * (GRADATION_LEVEL_ASTR_PLOT - level)
        dawn_track_color.append((DAWN_COLOR['track'][0], DAWN_COLOR['track'][1], DAWN_COLOR['track'][2], alpha))

    # 球面の準備
    if astr_r > 0:
        sphere_grid = get_sphere_grid(astr_r)
    
    # プロット作成
    fig = plt.figure()
    plt.style.use('dark_background')
    frame = 0
    for idx in range(0, et_array.shape[0], plot_idx_step):
        frame += 1
        save_file_name = "{}{}_{:05d}.png".format(save_path, filename, frame)
        print(save_file_name)
        ax = fig.gca(projection = "3d", frame_on = True)
        ax.set_xlabel('X [km]')
        ax.set_ylabel('Y [km]')
        ax.set_zlabel('Z [km]')
        ax.set_xlim(axis_limit[0], axis_limit[1])
        ax.set_ylim(axis_limit[2], axis_limit[3])
        ax.set_zlim(axis_limit[4], axis_limit[5])
        ax.set_xticks([axis_limit[0], axis_limit[0] / 2, 0, axis_limit[1] / 2, axis_limit[1]])
        ax.set_yticks([axis_limit[2], axis_limit[2] / 2, 0, axis_limit[3] / 2, axis_limit[3]])
        ax.set_zticks([axis_limit[4], axis_limit[4] / 2, 0, axis_limit[5] / 2, axis_limit[5]])
        ax.set_aspect('equal')
        ax.w_xaxis.set_pane_color((0.05, 0.05, 0.05, 1.0)) 
        ax.w_yaxis.set_pane_color((0.05, 0.05, 0.05, 1.0)) 
        ax.w_zaxis.set_pane_color((0.15, 0.15, 0.15, 1.0)) 
        for gradation in range(GRADATION_LEVEL_ASTR_PLOT):
            max_idx = idx - gradation * plot_idx_step_gradation
            min_idx = max_idx - plot_idx_step_gradation
            if max_idx < 0:
                break
            if min_idx < 0:
                min_idx = 0
            ax.plot(pos_v[min_idx:max_idx + 1, 0],
                    pos_v[min_idx:max_idx + 1, 1],
                    pos_v[min_idx:max_idx + 1, 2],
                    color = dawn_track_color[gradation],
                    zorder = (100 - gradation),
                    lw = 2)
        if astr_r > 0:
            ax.plot_wireframe(sphere_grid[0], sphere_grid[1], sphere_grid[2],
                              rstride = 12, cstride = 12, color=astr_color)
        else:
            ax.plot([0], [0], [0], color = astr_color, marker = "o")

        # テキスト出力
        date_text = spiceExt.et2datetime(et_array[idx]).strftime("%Y-%m-%d %H:%M:%S (UTC)")
        ax.text2D(0.7, 0.9, date_text, transform = ax.transAxes, fontsize = 15)
        fig.savefig(save_file_name, dpi = 100)

        # グラフを消去
        plt.clf()
    return


def get_sphere_grid(radius):
    """
    球面を表現するグリッドデータを作る
    :param radius: 半径
    :type radisu: float
    :return: 球面のメッシュ
    :return type: list
    """
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    # Cartesian coordinates that correspond to the spherical angles:
    # (this is the equation of an ellipsoid):
    sphere_x = radius * np.outer(np.cos(u), np.sin(v))
    sphere_y = radius * np.outer(np.sin(u), np.sin(v))
    sphere_z = radius * np.outer(np.ones_like(u), np.cos(v))
    return [sphere_x, sphere_y, sphere_z]


def local_plots():
    """
    小惑星周りのプロットをする
    """

    # プロットのセッティング
    vesta_plot_setting = [{'path':'.//Vesta_plot//',
                           'filename':'Vesta',
                           'astr_color':PLANET_COLORS[VESTA]['edge'],
                           'axis_limit':[-5e3, 5e3, -5e3, 5e3, -5e3, 5e3],
                           'astr_r':RADIUS[VESTA]},
                          {'path':'.//Vesta_plot_closeup//',
                           'filename':'Vesta_closeup',
                           'astr_color':PLANET_COLORS[VESTA]['edge'],
                           'axis_limit':[-1e3, 1e3, -1e3, 1e3, -1e3, 1e3],
                           'astr_r':RADIUS[VESTA]}]
                          
    ceres_plot_setting = [{'path':'.//Ceres_plot//',
                           'filename':'Ceres',
                           'astr_color':PLANET_COLORS[CERES]['edge'],
                           'axis_limit':[-4e4, 4e4, -4e4, 4e4, -4e4, 4e4],
                           'astr_r':0},
                          {'path':'.//Ceres_plot_closeup//',
                           'filename':'Ceres_closeup',
                           'astr_color':PLANET_COLORS[CERES]['edge'],
                           'axis_limit':[-5e3, 5e3, -5e3, 5e3, -5e3, 5e3],
                           'astr_r':RADIUS[CERES]},
                          {'path':'.//Ceres_plot_closeup2//',
                           'filename':'Ceres_closeup2',
                           'astr_color':PLANET_COLORS[CERES]['edge'],
                           'axis_limit':[-1e3, 1e3, -1e3, 1e3, -1e3, 1e3],
                           'astr_r':RADIUS[CERES]}]

    # Vesta周りの軌道を計算、描画
    et_array, dawn_pos_v_local = calc_pos_v_asteroid(VESTA,
                                                     EVENT_TIMING_ET['VESTA_ARR'],
                                                     EVENT_TIMING_ET['VESTA_DEP'])

    for plot_setting in vesta_plot_setting:
        _local_plot(et_array = et_array, pos_v = dawn_pos_v_local,
                    save_path = plot_setting['path'],
                    filename = plot_setting['filename'],
                    astr_color = plot_setting['astr_color'],
                    axis_limit = plot_setting['axis_limit'],
                    astr_r = plot_setting['astr_r'])
    
    # Ceres周りの軌道を計算、描画
    et_array, dawn_pos_v_local = calc_pos_v_asteroid(CERES,
                                                     EVENT_TIMING_ET['CERES_ARR'],
                                                     EVENT_TIMING_ET['END'])
    for plot_setting in ceres_plot_setting:
        _local_plot(et_array = et_array, pos_v = dawn_pos_v_local,
                    save_path = plot_setting['path'],
                    filename = plot_setting['filename'],
                    astr_color = plot_setting['astr_color'],
                    axis_limit = plot_setting['axis_limit'],
                    astr_r = plot_setting['astr_r'])
    return


def mga_plot():
    """
    火星スウィングバイ中のプロットを作成する
    """

    # 保存先のフォルダがなかったら作成する
    save_path = ".//mga//"      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    # MGA中の軌道を計算する
    mga_s_et = EVENT_TIMING_ET['MGA'] - MGA_TIME_SPAN # 計算開始時刻
    mga_e_et = EVENT_TIMING_ET['MGA'] + MGA_TIME_SPAN # 計算終了時刻
    et_array = np.arange(mga_s_et, mga_e_et, DELTA_T_MGA_CALC)
    dawn_vs_mars = spiceExt.spkezr(DAWN, et_array, INERTIAL_FRAME, "NONE", MARS)[0]
    dawn_vs_sun = spiceExt.spkezr(DAWN, et_array, INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]
    mars_vs_sun = spiceExt.spkezr(MARS, et_array, INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]
    origin_pos = spiceExt.spkezr(MARS, EVENT_TIMING_ET['MGA'], INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]
    dawn_vs_sun[:, :3] -= origin_pos[:3].reshape((1,3)) # スウィングバイの瞬間の火星の位置を原点とする
    mars_vs_sun[:, :3] -= origin_pos[:3].reshape((1,3))
    dawn_v_vs_sun = norm(dawn_vs_sun[:, 3:], axis = 1) # 軌道速度の大きさ
    dawn_v_vs_mars = norm(dawn_vs_mars[:, 3:], axis = 1) # 火星との相対速度の大きさ
    time_mga = et_array - EVENT_TIMING_ET['MGA'] # スウィングバイの瞬間の時刻を0とする

    # 火星の球面を準備する
    sphere_grid = get_sphere_grid(RADIUS[MARS])
    
    # プロットを実施する
    plot_idx_step = int(DELTA_T_MGA_PLOT / DELTA_T_MGA_CALC) # 計算済みデータをいくつおきに描画するか
    frame = 0
    fig = plt.figure(figsize = (12.8, 7.2))
    plt.style.use('dark_background')
    fig.patch.set_facecolor('black')
    
    for idx in range(0, time_mga.shape[0], plot_idx_step):
        frame += 1
        save_file_name = "{}{}1_{:05d}.png".format(save_path, MGA_PLOT_FILENAME, frame)
        print(save_file_name)

        # 描画領域、軸の設定
        gs = gridspec.GridSpec(13, 19)

        left_sun = plt.subplot(gs[0:6, 0:2]) # 左上（軌道図）
        top_sun = plt.subplot(gs[0:6, 3:9])
        bottom_sun = plt.subplot(gs[7:9, 3:9])

        ortho_mars = plt.subplot(gs[0:9, 10:19], projection = '3d') # 右上（火星基準の軌道図）

        v_vs_sun = plt.subplot(gs[10:13, 0:9]) # 左下（軌道速度）
        v_vs_mars = plt.subplot(gs[10:13, 10:19]) # 右下（火星との相対速度）

        # 軌道図（左側面図）の設定
        left_sun.set_xlim([-1.5e5, 1.5e5])
        left_sun.set_ylim([-5e5, 5e5])
        left_sun.set_aspect('equal')
        left_sun.set_xlabel('Z [km]')
        left_sun.set_ylabel('Y [km]')
        left_sun.set_xticks((-1.5e5, 1.5e5))
        left_sun.set_yticks((-5e5, -2.5e5, 0, 2.5e5, 5e5))
        left_sun.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
        left_sun.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
        # 軌道図（上面図）の設定
        top_sun.set_xlim([-5e5, 5e5])
        top_sun.set_ylim([-5e5, 5e5])
        top_sun.set_aspect('equal')
        top_sun.set_xticks((-5e5, -2.5e5, 0, 2.5e5, 5e5))
        top_sun.set_yticks((-5e5, -2.5e5, 0, 2.5e5, 5e5))
        top_sun.xaxis.set_major_formatter(ticker.NullFormatter())
        top_sun.yaxis.set_major_formatter(ticker.NullFormatter())
        # 軌道図（下面図）の設定
        bottom_sun.set_xlim([-5e5, 5e5])
        bottom_sun.set_ylim([-1.5e5, 1.5e5])
        bottom_sun.set_aspect('equal')
        bottom_sun.set_xlabel('X [km]')
        bottom_sun.set_ylabel('Z [km]')
        bottom_sun.set_xticks((-5e5, 0, 5e5))
        bottom_sun.set_yticks((-1.5e5, 1.5e5))
        bottom_sun.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
        bottom_sun.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
        # 軌道速度プロットの設定
        v_vs_sun.set_xlim([-MGA_TIME_SPAN, MGA_TIME_SPAN])
        v_vs_sun.set_ylim([24, 28])
        v_vs_sun.set_yticks((24, 26, 28))
        v_vs_sun.set_xlabel('Time[sec]')
        v_vs_sun.set_ylabel('Velocity[km/s]')
        # 火星との相対速度プロットの設定
        v_vs_mars.set_xlim([-MGA_TIME_SPAN, MGA_TIME_SPAN])
        v_vs_mars.set_ylim([2, 6])
        v_vs_mars.set_yticks((2, 4, 6))
        v_vs_mars.set_xlabel('Time[sec]')
        v_vs_mars.set_ylabel('Velocity[km/s]')
        # 火星基準の軌道図の設定
        ortho_mars.set_xlim([-4e4, 4e4])
        ortho_mars.set_ylim([-4e4, 4e4])
        ortho_mars.set_zlim([-0.6e5, 0.2e5])
        ortho_mars.set_xticks((-4e4, -2e4, 0, 2e4, 4e4))
        ortho_mars.set_yticks((-4e4, -2e4, 0, 2e4, 4e4))
        ortho_mars.set_zticks((-6e4, -4e4, -2e4, 0, 2e4))
        ortho_mars.set_aspect('equal')
        ortho_mars.set_xlabel('X [km]')
        ortho_mars.set_ylabel('Y [km]')
        ortho_mars.set_zlabel('Z [km]')
        ortho_mars.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.0e'))
        ortho_mars.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0e'))
        ortho_mars.zaxis.set_major_formatter(ticker.FormatStrFormatter('%.0e'))
        ortho_mars.view_init(elev = 30, azim = -120)
        ortho_mars.w_xaxis.set_pane_color((0.05, 0.05, 0.05, 1.0)) 
        ortho_mars.w_yaxis.set_pane_color((0.05, 0.05, 0.05, 1.0)) 
        ortho_mars.w_zaxis.set_pane_color((0.15, 0.15, 0.15, 1.0)) 

        # グラフの描画
        #  軌道図（左側面図）
        left_sun.plot(dawn_vs_sun[:idx+1, 2], dawn_vs_sun[:idx+1, 1], color = DAWN_COLOR['track'], lw = 2, zorder = 20)
        left_sun.plot(mars_vs_sun[:idx+1, 2], mars_vs_sun[:idx+1, 1], color = PLANET_COLORS[MARS]['track'], lw = 2, zorder = 10)
        #  軌道図（上面図）
        top_sun.plot(dawn_vs_sun[:idx+1, 0], dawn_vs_sun[:idx+1, 1], color = DAWN_COLOR['track'], lw = 2, zorder = 20)
        top_sun.plot(mars_vs_sun[:idx+1, 0], mars_vs_sun[:idx+1, 1], color = PLANET_COLORS[MARS]['track'], lw = 2, zorder = 10)
        #  軌道図（下面図）
        bottom_sun.plot(dawn_vs_sun[:idx+1, 0], dawn_vs_sun[:idx+1, 2], color = DAWN_COLOR['track'], lw = 2, zorder = 20)
        bottom_sun.plot(mars_vs_sun[:idx+1, 0], mars_vs_sun[:idx+1, 2], color = PLANET_COLORS[MARS]['track'], lw = 2, zorder = 10)
        #  軌道速度
        v_vs_sun.plot(time_mga[:idx+1], dawn_v_vs_sun[:idx+1], color = DAWN_COLOR['track'], lw = 2)
        #  火星基準の軌道図
        ortho_mars.plot(dawn_vs_mars[:idx+1, 0], dawn_vs_mars[:idx+1, 1], dawn_vs_mars[:idx+1, 2], color = DAWN_COLOR['track'], lw = 2, zorder = 20)
        ortho_mars.plot_wireframe(sphere_grid[0], sphere_grid[1], sphere_grid[2],
                                  rstride = 12, cstride = 12, color = PLANET_COLORS[MARS]['edge'])
        #  火星との相対速度
        v_vs_mars.plot(time_mga[:idx+1], dawn_v_vs_mars[:idx+1], color = DAWN_COLOR['track'], lw = 2)

        # 日付の書き込み
        date_text = spiceExt.et2datetime(et_array[idx]).strftime("%Y-%m-%d %H:%M:%S (UTC)")
        time_text = "Mars flyby {:>+5.0f} [sec]".format(time_mga[idx])
        ortho_mars.text2D(0.67, 0.9, date_text, transform = ortho_mars.transAxes, fontsize = 15)
        ortho_mars.text2D(0.67, 0.82, time_text, transform = ortho_mars.transAxes, fontsize = 15)

        # 画像保存とプロット消去
        plt.savefig(save_file_name)
        plt.clf()

    return


def vec2EulerXYZ(vec):
    """
    ベクトルの指す方位をオイラー角(XYZ)に変換する
    :param vec: 方向ベクトル
    :vec type: numpy.ndarray
    :return: オイラー角[deg]
    :return type: numpy.ndarray
    """

    # 方向ベクトル（正規化）
    n = vec.shape[0]            # データ点数
    direction = vec / norm(vec, axis = 1).reshape((n, 1))
    # 仰角、方位角を求める
    elev = np.rad2deg(np.arcsin(direction[:, 2])) # 仰角
    azim = np.rad2deg(np.arctan2(direction[:, 1], direction[:, 0])) # 方位角
    # オイラー角にする
    eulerXYZ = np.zeros((n, 3))
    eulerXYZ[:, 1] = -elev
    eulerXYZ[:, 2] = azim
    return eulerXYZ

def export_data_for_blender(dawn_pv_in_i_frame, earth_pv_in_i_frame, ies_status,
                            dawn_frame_to_i_frame, sa_frame_to_dawn_frame):
    """
    Blenderでの3Dレンダリングに必要な情報を書き出す
    :param dawn_pv_in_i_frame: 慣性座標系太陽基準のDawnの位置
    :type dawn_pv_in_i_frame: numpy.ndarray
    :param ies_status: イオンエンジン運転状況
    :type ies_status: numpy.ndarray
    :param dawn_frame_to_i_frame: Dawn座標系→慣性座標系の回転行列
    :type dawn_frame_to_i_frame: numpy.ndarray
    :param sa_frame_to_dawn_frame: 太陽電池パネル座標系→Dawn座標系の回転行列
    :type sa_frame_to_dawn_frame: numpy.ndarray
    """

    n = dawn_pv_in_i_frame.shape[0] # 計算点数
    # 回転行列をクォータニオンに変換する
    dawn_attitude_q = spiceExt.m2q(dawn_frame_to_i_frame)
    sa_attitude_q = spiceExt.m2q(sa_frame_to_dawn_frame)
    # 太陽の方向を計算する
    sun_direction_eulerXYZ = vec2EulerXYZ(-dawn_pv_in_i_frame[:, :3])
    # 地球の方向を計算する
    earth_direction_eulerXYZ = vec2EulerXYZ(earth_pv_in_i_frame[:, :3] - dawn_pv_in_i_frame[:, :3])

    with open(BLENDER_EXPORT_FILENAME, "w") as export_file:
        # ラベル行の書き込み
        export_file.write("太陽の方向（オイラー角XYZ[deg]）,,,")
        export_file.write("地球の方向（オイラー角XYZ[deg]）,,,")
        export_file.write("Dawnの姿勢（クォータニオン）,,,,")
        export_file.write("太陽電池の角度（クォータニオン）,,,,")
        export_file.write("イオンエンジンの運転状態\n")
        # データの書き込み
        for idx in range(n):
            for euler_angle in sun_direction_eulerXYZ[idx, :]:
                export_file.write("{:.5e},".format(euler_angle)) # 太陽の方向（オイラー角[deg]）
            for euler_angle in earth_direction_eulerXYZ[idx, :]:
                export_file.write("{:.5e},".format(euler_angle)) # 地球の方向（オイラー角[deg]）
            for dawn_q in dawn_attitude_q[idx, :]:
                export_file.write("{:.5e},".format(dawn_q)) # Dawnの姿勢（クォータニオン）
            for sa_q in sa_attitude_q[idx, :]:
                export_file.write("{:.5e},".format(sa_q)) # 太陽電池の角度（クォータニオン）
            export_file.write("{:d}".format(ies_status[idx])) # イオンエンジンの運転状態
            export_file.write("\n")
    return


def read_orbital_element():
    """
    ファイルからdawnの軌道要素を読み込む
    :return: 軌道要素
    :return type: dict
    """
    
    with open(ORBITAL_ELEMENT_FILENAME, "r") as csv_file:
        reader = csv.reader(csv_file)
        next(reader)            # 1行目はヘッダなので読み飛ばす
        epoch = []
        semi_major_axis = []
        eccentricity = []
        period = []
        inclination = []
        ascending_node = []
        arg_periapsis = []
        mean_anomaly = []
        for one_line in reader:
            epoch.append(datetime.datetime.strptime(one_line[0], "%Y/%m/%d %H:%M"))
            semi_major_axis.append(float(one_line[1]))
            eccentricity.append(float(one_line[2]))
            period.append(float(one_line[3]))
            inclination.append(float(one_line[4]))
            ascending_node.append(float(one_line[5]))
            arg_periapsis.append(float(one_line[6]))
            mean_anomaly.append(float(one_line[7]))
        orbital_element = {}
        orbital_element['a'] = semi_major_axis
        orbital_element['epoch'] = epoch
        orbital_element['e'] = eccentricity
        orbital_element['i'] = inclination
        orbital_element['p'] = period
        orbital_element['Omega'] = ascending_node
        orbital_element['w'] = arg_periapsis
        orbital_element['ma'] = mean_anomaly

        return orbital_element


def get_planet_orbit_shape():
    """
    ファイルから天体の軌道要素を読み込み、軌道の形状を求める
    :return: 各天体の軌道の形状
    :return type: dict
    """
    with open(PLANET_ORBITAL_ELEMENT_FILENAME, "r") as csv_file:
        reader = csv.reader(csv_file)
        next(reader)            # 1行目はヘッダなので読み飛ばす
        orbit_shape = {}
        for one_line in reader:
            oe = orbit.orbitalElement(semi_major_axis = float(one_line[2]),
                                      eccentricity = float(one_line[3]),
                                      period = float(one_line[4]),
                                      inclination = float(one_line[5]),
                                      ascending_node = float(one_line[6]),
                                      argument_periapsis = float(one_line[7]),
                                      epoch = datetime.datetime.strptime(one_line[1], "%Y/%m/%d %H:%M"),
                                      mean_anomaly = float(one_line[8]))
            shape = oe.get_orbital_shape(180)
            orbit_shape[one_line[0]] = shape
    return orbit_shape
    
    
def interplanetary_plot():
    """
    惑星間航行中のプロットを行う
    """

    # Dawn、惑星の位置、速度を抽出する
    et_array, dawn_pv_in_i_frame, planet_pv_in_i_frame = calc_pos_v()

    # Dawnのイオンエンジンの運転状態を計算する
    dawn_dv_in_i_frame, total_dv_by_ies, i_frame_to_dawn_frame, ies_status = calc_ies_status(dawn_pv_in_i_frame, et_array)
    # dawn_dv_in_i_frame, total_dv_by_ies, i_frame_to_dawn_frame, ies_status = calc_ies_status2(dawn_pv_in_i_frame, planet_pv_in_i_frame, et_array)

    # Dawn、太陽電池パネルの姿勢を計算する
    dawn_frame_to_i_frame = spiceExt.pxform(DAWN_FRAME, INERTIAL_FRAME, et_array)
    sa_frame_to_dawn_frame = spiceExt.pxform(DAWN_SA_FRAME[0], DAWN_FRAME, et_array)

    # Blender描画用のデータを出力する
    step = int(DELTA_T_CRUISE_PLOT / DELTA_T_CRUISE_CALC) # 描画間隔
    export_data_for_blender(dawn_pv_in_i_frame = dawn_pv_in_i_frame[::step, :3],
                            earth_pv_in_i_frame = planet_pv_in_i_frame[EARTH][::step, :3],
                            ies_status = ies_status[::step],
                            dawn_frame_to_i_frame = dawn_frame_to_i_frame[::step, :, :],
                            sa_frame_to_dawn_frame = sa_frame_to_dawn_frame[::step, :, :])

    # Dawnの軌道要素を読み込む
    orbital_element = read_orbital_element()

    # 惑星の軌道要素を読み込み、軌道の形状を取得する
    planet_orbit_shape = get_planet_orbit_shape()

    # マイルストーンでの惑星位置を算出
    mars_mga_pos = spiceExt.spkezr(MARS, EVENT_TIMING_ET['MGA'], INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]
    vesta_arrival_pos = spiceExt.spkezr(VESTA, EVENT_TIMING_ET['VESTA_ARR'], INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]
    ceres_arrival_pos = spiceExt.spkezr(CERES, EVENT_TIMING_ET['CERES_ARR'], INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0]

    # 保存先のフォルダがなかったら作成する
    save_path = ".//cruise//"      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    # 描画準備
    fig = plt.figure(figsize = (12, 7.2))
    frame = 0
    n = et_array.shape[0]       # 総計算点数
    start_idx = 0
    end_idx = n
    # start_idx = end_idx - step * 2
        
    for idx in range(start_idx, end_idx, step):
        save_file_name = "{}{}_{:05d}.png".format(save_path, CRUISE_PLOT_FILENAME, frame + 1)
                
        # 描画の軸のセットアップ
        plt.style.use('dark_background')
        fig.patch.set_facecolor('black')
        gs = gridspec.GridSpec(8, 1)
        plt.subplots_adjust(hspace = 1.5)
    
        top_sun = plt.subplot(gs[0:6, 0])
        top_sun.set_aspect('equal')
        top_sun.set_xlim([-5e8, 5e8])
        top_sun.set_ylim([-5e8, 5e8])
        top_sun.set_xlabel('X [km]')
        top_sun.set_ylabel('Y [km]')
        top_sun.set_xticks([-5e8, -2.5e8, 0, 2.5e8, 5e8])
        top_sun.set_yticks([-5e8, -2.5e8, 0, 2.5e8, 5e8])

        bottom_sun = plt.subplot(gs[6:, 0])
        bottom_sun.set_aspect('equal')
        bottom_sun.set_xlim([-5e8, 5e8])
        bottom_sun.set_ylim([-1.3e8, 1.3e8])
        bottom_sun.set_xlabel('X [km]')
        bottom_sun.set_ylabel('Z [km]')
        bottom_sun.set_xticks([-5e8, -2.5e8, 0, 2.5e8, 5e8])
        bottom_sun.set_yticks([-1e8, 0, 1e8])

        # Dawnの描画
        top_sun.plot(dawn_pv_in_i_frame[idx, 0], dawn_pv_in_i_frame[idx, 1],
                     marker = "s", markeredgewidth = 2,
                     markerfacecolor = DAWN_COLOR['face'],
                     markeredgecolor = DAWN_COLOR['edge'],
                     zorder = 220)
        bottom_sun.plot(dawn_pv_in_i_frame[idx, 0], dawn_pv_in_i_frame[idx, 2],
                        marker = "s", markeredgewidth = 2,
                        markerfacecolor = DAWN_COLOR['face'],
                        markeredgecolor = DAWN_COLOR['edge'],
                        zorder = 220)
        for thruster_num in range(len(IES_TRACK_COLOR)): # 軌跡はIESの運転状態ごとに描画する
            pv = dawn_pv_in_i_frame[:idx, :3]
            status = ies_status[:idx]
            ies_track = pv[status == thruster_num, :]
            zorder = 50 - thruster_num
            top_sun.plot(ies_track[:, 0], ies_track[:, 1],
                         lw = 0, marker = ".", markerfacecolor = IES_TRACK_COLOR[thruster_num], markeredgecolor = (1,1,1,0),
                         markersize = 2, zorder = zorder)
            # 下面図に軌跡を書くとごちゃごちゃするので省略
            # bottom_sun.plot(ies_track[:, 0], ies_track[:, 2],
            #                 lw = 0, marker = ".", markerfacecolor = IES_TRACK_COLOR[thruster_num], markeredgecolor = (1,1,1,0),
            #                 markersize = 2, zorder = zorder)
        # 増速量（矢印）をプロットする
        if (ies_status[idx] >= 1 and ies_status[idx] <= 3):
            top_sun.arrow(dawn_pv_in_i_frame[idx, 0], dawn_pv_in_i_frame[idx, 1],
                          dawn_dv_in_i_frame[idx, 0] * DV_PLOT_SCALE, dawn_dv_in_i_frame[idx, 1] * DV_PLOT_SCALE,
                          head_width = DV_ARROW_HEAD_WIDTH, width = DV_ARROW_HEAD_WIDTH / 3,
                          fc = IES_ARROW_COLOR[ies_status[idx]],
                          ec = IES_ARROW_COLOR[ies_status[idx]], zorder = 210)
            bottom_sun.arrow(dawn_pv_in_i_frame[idx, 0], dawn_pv_in_i_frame[idx, 2],
                             dawn_dv_in_i_frame[idx, 0] * DV_PLOT_SCALE, dawn_dv_in_i_frame[idx, 2] * DV_PLOT_SCALE,
                             head_width = DV_ARROW_HEAD_WIDTH, width = DV_ARROW_HEAD_WIDTH / 3,
                             fc = IES_ARROW_COLOR[ies_status[idx]],
                             ec = IES_ARROW_COLOR[ies_status[idx]], zorder = 210)

        # Dawnの慣性軌道、予測位置を描画する
        dawn_orbit = orbit.orbitalElement(semi_major_axis = orbital_element['a'][frame],
                                          eccentricity = orbital_element['e'][frame],
                                          period = orbital_element['p'][frame],
                                          inclination = orbital_element['i'][frame],
                                          ascending_node = orbital_element['Omega'][frame],
                                          argument_periapsis = orbital_element['w'][frame],
                                          epoch = orbital_element['epoch'][frame],
                                          mean_anomaly = orbital_element['ma'][frame])
        dawn_orbit_shape = dawn_orbit.get_orbital_shape(180)
        if et_array[idx] > EVENT_TIMING_ET['VESTA_DEP']:
            dawn_predict_pos = dawn_orbit.get_position(EVENT_TIMING_DT['CERES_ARR'])
            planet_predict_pos = ceres_arrival_pos
            markeredgecolor = PLANET_COLORS[CERES]['edge']
        elif et_array[idx] > EVENT_TIMING_ET['MGA']:
            dawn_predict_pos = dawn_orbit.get_position(EVENT_TIMING_DT['VESTA_ARR'])
            planet_predict_pos = vesta_arrival_pos
            markeredgecolor = PLANET_COLORS[VESTA]['edge']
        else:
            dawn_predict_pos = dawn_orbit.get_position(EVENT_TIMING_DT['MGA'])
            planet_predict_pos = mars_mga_pos
            markeredgecolor = PLANET_COLORS[MARS]['edge']

        top_sun.plot(dawn_orbit_shape[:, 0], dawn_orbit_shape[:, 1],
                     color = DAWN_COLOR['track'], lw = 2, zorder = 10)
        bottom_sun.plot(dawn_orbit_shape[:, 0], dawn_orbit_shape[:, 2],
                        color = DAWN_COLOR['track'], lw = 2, zorder = 10)
        top_sun.plot(dawn_predict_pos[0], dawn_predict_pos[1], marker = "s", zorder = 200,
                     markerfacecolor = (0,0,0,1), markeredgecolor = DAWN_COLOR['edge'], markeredgewidth = 2)
        top_sun.plot(planet_predict_pos[0], planet_predict_pos[1], marker = "o", zorder = 100,
                     markerfacecolor = (0,0,0,1), markeredgecolor = markeredgecolor, markeredgewidth = 1)
        bottom_sun.plot(dawn_predict_pos[0], dawn_predict_pos[2], marker = "s", zorder = 200,
                        markerfacecolor = (0,0,0,1), markeredgecolor = DAWN_COLOR['edge'], markeredgewidth = 2)
        bottom_sun.plot(planet_predict_pos[0], planet_predict_pos[2], marker = "o", zorder = 100,
                        markerfacecolor = (0,0,0,1), markeredgecolor = markeredgecolor, markeredgewidth = 1)
        
        # 惑星の描画
        for name, pos in planet_pv_in_i_frame.items():
            top_sun.plot(pos[idx, 0], pos[idx, 1],
                         marker = "o", markeredgewidth = 1,
                         markerfacecolor = PLANET_COLORS[name]['face'],
                         markeredgecolor = PLANET_COLORS[name]['edge'],
                         zorder = 150)
            bottom_sun.plot(pos[idx, 0], pos[idx, 2],
                            marker = "o", markeredgewidth = 1,
                            markerfacecolor = PLANET_COLORS[name]['face'],
                            markeredgecolor = PLANET_COLORS[name]['edge'],
                            zorder = 150)
            top_sun.plot(planet_orbit_shape[name][:,0], planet_orbit_shape[name][:,1],
                         color = PLANET_COLORS[name]['track'], lw = 1,
                         zorder = 0)
            bottom_sun.plot(planet_orbit_shape[name][:,0], planet_orbit_shape[name][:,2],
                            color = PLANET_COLORS[name]['track'], lw = 1,
                            zorder = 0)
        # 太陽の描画
        top_sun.plot([0], [0],
                     color = PLANET_COLORS['SUN']['face'],
                     marker = "o", markersize = 15)
        # 太陽を下面図に書くと、前後関係がうまく表示できないので省略
        # bottom_sun.plot([0], [0],
        #              color = PLANET_COLORS['SUN']['face'],
        #              marker = "o", markersize = 15)

        # 日時、ΔV、イオンエンジン運転状態の表示
        date_text = spiceExt.et2datetime(et_array[idx]).strftime("%Y-%m-%d %H:%M:%S (UTC)")
        dv_text = "Total Delta V : {:2.2f} [km/s]".format(total_dv_by_ies[idx])
        
        top_sun.text(1.05, 0.9, date_text, transform = top_sun.transAxes, fontsize = 15)
        top_sun.text(1.05, 0.82, dv_text, transform = top_sun.transAxes, fontsize = 15)

        if ies_status[idx] == 0:
            ies_text = "Coasting"
        elif ies_status[idx] == 4:
            ies_text = "Orbit around asteroid"
        else:
            ies_text = "Thrusting (Thruster {})".format(ies_status[idx])
        top_sun.text(1.05, 0.74, ies_text,
                     transform = top_sun.transAxes, color = IES_TEXT_COLOR[ies_status[idx]], fontsize = 15)
            
        # 出力
        fig.savefig(save_file_name, dpi = 100)
        fig.clf()
        frame += 1


if __name__ == "__main__":

    # SPICEカーネル情報を読み込む
    print("SPICE Kernel情報を読み込み中...", end = "", flush = True)
    spice.furnsh(KERNEL_LIST_FILENAME)
    print("完了")

    # イベント日時をETに変換する
    for key, value in EVENT_TIMING_DT.items():
        EVENT_TIMING_ET[key] = spiceExt.datetime2et(value)
    # イオンエンジン運転期間をETに変換する
    for timing in IES_DRIVE_PERIOD_DT:
        start_et = spiceExt.datetime2et(timing[0])
        end_et = spiceExt.datetime2et(timing[1])
        IES_DRIVE_PERIOD_ET.append([start_et, end_et])
    
    # local_plots()               # 小惑星周回軌道の描画
    # mga_plot()                  # 火星スウィングバイの描画
    interplanetary_plot()       # 惑星間航行の描画
    
