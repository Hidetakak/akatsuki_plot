import csv
import numpy as np
import bpy
import os

#data file name
DATA_FILE_NAME = '.\\datas_for_blender.csv'

# object name
CAMERA = bpy.data.objects['Camera_manipulator']
SUN = bpy.data.objects['Sun_manipulator']
DAWN = bpy.data.objects['Dawn_SC']
DAWN_SA = bpy.data.objects['Dawn_SA']
EARTH = bpy.data.objects['earth_direction']
IES = [bpy.data.objects['Ion_Thrust_01'],
       bpy.data.objects['Ion_Thrust_02'],
       bpy.data.objects['Ion_Thrust_03']]

# frame rate
STEP = 1

def _update_ies_thrust(ies_object, status):
    """
    :param ies_object: ies object name
    :ies_object type: str
    :param status: status of ies  True -> on
    :type status: bool
    """
    ies_object.hide_render = not status
    ies_object.hide = not status
    ies_object.keyframe_insert(data_path = 'hide_render')
    ies_object.keyframe_insert(data_path = 'hide')
    for child in ies_object.children:
        child.hide_render = not status
        child.hide = not status
        child.keyframe_insert(data_path = 'hide_render')
        child.keyframe_insert(data_path = 'hide')
    return


def update_ies(ies_status):
    """
    :param ies_status: status of all ies's
    :type ies_status: int
    """
    # thruster all off
    for ies_object in IES:
        _update_ies_thrust(ies_object, False)

    if ies_status == 0 or ies_status == 4:
        return

    _update_ies_thrust(IES[ies_status - 1], True)
    return

def update_sun_direction(sun_direction):
    """
    :param sun_direction: sun direction (EulerXYZ)[deg]
    :sun_direction type: numpy.ndarray
    """
    sun_direction = np.deg2rad(sun_direction)
    SUN.rotation_mode = 'XYZ'
    SUN.rotation_euler = list(sun_direction)
    SUN.keyframe_insert(data_path = 'rotation_euler')
    return

def update_earth_direction(earth_direction):
    """
    :param earth_direction: earth direction (EulerXYZ)[deg]
    :earth_direction type: numpy.ndarray
    """
    earth_direction = np.deg2rad(earth_direction)
    EARTH.rotation_mode = 'XYZ'
    EARTH.rotation_euler = list(earth_direction)
    EARTH.keyframe_insert(data_path = 'rotation_euler')
    return


def update_Dawn_attitude(dawn_attitude, sa_attitude):
    """
    :param dawn_attitude: dawn attitude (quaternion)
    :type dawn_attitude: numpy.ndarray
    :param sa_attitude: sa attitude (quaternion)
    :type sa_attitude: numpy.ndarray
    """
    DAWN.rotation_quaternion = list(dawn_attitude)
    DAWN_SA.rotation_quaternion = list(sa_attitude)
    DAWN.keyframe_insert(data_path = 'rotation_quaternion')
    DAWN_SA.keyframe_insert(data_path = 'rotation_quaternion')
    
    return
    
if __name__ == '__main__':
    # read data file
    os.chdir('C:/home/hidetaka/program/akatsuki_plot/Dawn_orbit_2')
    calc_data = np.loadtxt(fname = DATA_FILE_NAME, delimiter = ',', skiprows = 1)
    
    sun_direction = calc_data[:, :3]
    earth_direction = calc_data[:, 3:6]
    dawn_attitude = calc_data[:, 6:10]
    sa_attitude = calc_data[:, 10:14]
    ies_status = calc_data[:, 14]
    
    #frame setting
    total_frame = int(calc_data.shape[0] / STEP)
    
    bpy.data.scenes['Scene'].frame_start = 1
    bpy.data.scenes['Scene'].frame_end = total_frame
    
    current_frame = 0
    
    for idx in range(0, calc_data.shape[0], STEP):
        current_frame += 1
        print(current_frame)
        bpy.data.scenes['Scene'].frame_current = current_frame
        update_sun_direction(sun_direction[idx, :])
        update_earth_direction(earth_direction[idx, :])
        update_Dawn_attitude(dawn_attitude[idx, :], sa_attitude[idx, :])
        update_ies(int(ies_status[idx]))
    
