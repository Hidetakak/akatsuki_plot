  % ニコニコ動画に投稿する動画用の素材作成プログラム %

# ニコニコ動画に投稿する動画用の素材作成プログラム

## 概要
惑星・探査機の軌道を計算し、連番の画像ファイルなどを出力するソフト群

## 動作に必要なソフト、モジュール
+ Python開発環境(Anaconda推奨)
 - Python 3.0以降
 - Numpy
 - Scipy
 - matplotlib
+ Python版SPICE toolkit

## 投稿した動画
 + [金星探査機「あかつき」の軌道(2010/5～2015/11)](http://www.nicovideo.jp/watch/sm24742391)
 + [小惑星探査機Dawnのイオンエンジンによる軌道変更](http://www.nicovideo.jp/watch/sm25335318)
 + [はやぶさ2の位置とイオンエンジンによる軌道変更(2014/12/3～2018/6/5)](http://www.nicovideo.jp/watch/sm25690379)
 + [2015/12/7 金星探査機「あかつき」金星軌道投入時の軌道変更](http://www.nicovideo.jp/watch/sm27052792)
 + [小惑星探査機Dawnの軌道(2007/9/27～2016/6/17)](http://www.nicovideo.jp/watch/sm30330065)
 + [小惑星探査機「はやぶさ」の目に映った光景](http://www.nicovideo.jp/watch/sm30452060)
 + [はやぶさ2の地球スウィングバイをCGで再現](http://www.nicovideo.jp/watch/sm30543190)
 + [はやぶさ2の軌道・姿勢をCGで再現(打ち上げ～地球スウィングバイ) 前編](http://www.nicovideo.jp/watch/sm30961600)
 + [はやぶさ2の軌道・姿勢をCGで再現(打ち上げ～地球スウィングバイ) 後編](http://www.nicovideo.jp/watch/sm30962083)

