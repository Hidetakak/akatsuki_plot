# -*- coding:utf-8 -*-

"""
SPICEカーネルから、はやぶさ、イトカワの位置、姿勢情報を抽出する
"""

import os
import sys
import datetime
import argparse

import numpy as np
from numpy.linalg import norm, inv
import spiceypy as spice
import matplotlib.pyplot as plt

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import SpiceyPyExt as spiceExt


# カーネルリスト
KERNEL_LIST = 'kernel_list2.txt'

# Body name
HAYABUSA = 'HAYABUSA'
ITOKAWA = 'ITOKAWA'
SS_BARYCENTER = "SOLAR SYSTEM BARYCENTER" # 太陽系の重心
SUN = "SUN"                     # 太陽
EARTH = "EARTH BARYCENTER"      # 地球（地球、月の重心）

#   座標系の名前
INERTIAL_FRAME = 'ECLIPJ2000'     # 慣性座標系(黄道座標系)
HAYABUSA_FRAME = 'HAYABUSA_SC_BUS_PRIME' # はやぶさ機体
ITOKAWA_FRAME = 'ITOKAWA_FIXED'          # イトカワ
AMICA_FRAME = 'HAYABUSA_AMICA'     # ONC-T/AMICA

# 計算期間のデフォルト値
START_STR = '2005,9,11,5,52,43'
END_STR = '2005,11,19,23,55,07'
DELTA_T = 30                    # [sec]

# その他の3D描画設定
B_CAM_OFFSET = 0.025        # 後方からのカメラ位置（はやぶさからのバック分[km])
B_CAM_AZIM_OFFSET = -10     # 後方からもカメラ位置（方位角のずらし分[deg])
B_CAM_ELEV_OFFSET = 10      # 後方からもカメラ位置（方位角のずらし分[deg])
# AMICA_NEAR_RATE = 1/4.0     # AMICA代替カメラの位置（本来のAMICA距離の何倍に配置するか）

# 軌道の軌跡の計算パラメータ
START_PLOT_STR = START_STR      # 軌跡プロットの開始時刻
END_PLOT_STR = '2005,11,4,7,5'  # 軌跡プロットの終了時刻（ここまでは連続データがある）
DELTA_T_PLOT = 60               # 軌跡プロットの間隔[sec]

# 軌道の軌跡のプロットパラメータ
PLOT_FILENAME = 'haya_pos'
HAYABUSA_COLOR = {'face':[0.96, 0.59, 0.27, 1],
                  'edge':[0.96, 0.59, 0.27, 1],
                  'track':[0.96, 0.59, 0.27, 0.5]}
ITOKAWA_COLOR = {'face':[0.7,0.7, 0.7, 1],
                  'edge':[0.7, 0.7, 0.7, 1]}

# プロットの目盛線
R_GRID = [5, 10, 15, 20, 25]        # イトカワからの半径
THETA_DIV = 180                     # 円をかくときの円周の分割数
THEATA_GRID = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330] # イトカワからの方向
GRID_COLOR = [0.3, 0.3, 0.3]

def plot_track(start_dt, end_dt, step, path_name):

    # 全期間の軌跡を計算しておく
    start_plot_dt = csvstr2datetime(START_PLOT_STR)
    end_plot_dt = csvstr2datetime(END_PLOT_STR)
    et_array_plot = np.arange(spiceExt.datetime2et(start_plot_dt),
                              spiceExt.datetime2et(end_plot_dt), step)
    n_plot = et_array_plot.shape[0]
    h_vs_i_pos = spiceExt.spkezr(HAYABUSA, et_array_plot, INERTIAL_FRAME, "NONE", ITOKAWA)[0][:, :3]
    e_vs_i_pos = spiceExt.spkezr(EARTH, et_array_plot, INERTIAL_FRAME, "NONE", ITOKAWA)[0][:, :3]
    e_vs_i_pos /= norm(e_vs_i_pos, axis = 1).reshape((n_plot, 1))
    # 各時刻ごとに座標変換を行う
    itokawa_south = spice.pxform(ITOKAWA_FRAME, INERTIAL_FRAME, et_array_plot[0]).dot(np.array([0,0,-1])) # イトカワ南極方向
    track = np.zeros((n_plot, 3))
    for idx in range(n_plot):
        plot_y = -e_vs_i_pos[idx,:] # 地球方向が-y
        plot_z = itokawa_south     # イトカワ南極方向がz
        plot_x = np.cross(plot_y, plot_z) # x方向は右手系で決める
        i2p_mat = np.zeros((3, 3))      # 座標変換行列（プロット座標系→慣性座標系）
        i2p_mat[:,0] = plot_x
        i2p_mat[:,1] = plot_y
        i2p_mat[:,2] = plot_z
        p2i_mat = inv(i2p_mat)  # 座標変換行列（慣性座標系→プロット座標系）
        plot_xyz = p2i_mat.dot(h_vs_i_pos[idx, :])
        track[idx, :] = plot_xyz
        
    # 各時刻における軌跡を計算する
    et_array = np.arange(spiceExt.datetime2et(start_dt), spiceExt.datetime2et(end_dt), step)
    n = et_array.shape[0]
    
    # hayabusa_pos = spiceExt.spkezr(HAYABUSA, et_array, INERTIAL_FRAME, "NONE", ITOKAWA)[0][:, :3]
    # earth_pos = spiceExt.spkezr(EARTH, et_array, INERTIAL_FRAME, "NONE", ITOKAWA)[0][:, :3]
    # earth_pos /= norm(earth_pos, axis = 1).reshape((n, 1)) # 大きさを1に正規化

    # 保存先のフォルダを決める
    start_str = spiceExt.et2datetime(et_array[0]).strftime("%Y%m%dT%H%M%S")
    stop_str = spiceExt.et2datetime(et_array[-1]).strftime("%Y%m%dT%H%M%S")
    step = et_array[1] - et_array[0]

    save_path = ".//{}_{}_{:.0f}_{}_01//track//".format(start_str, stop_str, step, path_name)
    # 保存先のフォルダがなかったら作成する
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    frame = 0
    fig = plt.figure(figsize = (4,7.2))
    plt.style.use('dark_background')

    r_grid = []
    theta_grid = []
    
    for r in R_GRID:
        theta = np.deg2rad(np.arange(0, 360, 360/THETA_DIV))
        xy = np.zeros((THETA_DIV, 2))
        xy[:,0] = r * np.cos(theta)
        xy[:,1] = r * np.sin(theta)
        r_grid.append(xy)
    for theta in THEATA_GRID:
        theta_grid.append(np.array([[0, 0],[30 * np.cos(np.deg2rad(theta)), 30 * np.sin(np.deg2rad(theta))]]))
    
    for idx in range(n):
        frame += 1
        idx_plot = np.searchsorted(et_array_plot, et_array[idx]) # どの列までプロットするか
        h_plot = track[idx_plot, :]
        png_filename = '{}{}_{:04d}.png'.format(save_path, PLOT_FILENAME, frame)

        xy_plot = plt.subplot(2,1,1)
        xy_plot.set_xlim([-15, 15])
        xy_plot.set_ylim([-25, 5])
        xy_plot.set_aspect('equal')
        xy_plot.set_xlabel('X [km]')
        xy_plot.set_ylabel('Y [km]')
        xy_plot.set_xticks((-15, -10, -5, 0, 5, 10, 15))
        xy_plot.set_yticks((-25, -20, -15, -10, -5, 0, 5))

        xy_plot.plot(h_plot[0], h_plot[1], marker = "o", zorder = 30,
                     markerfacecolor = HAYABUSA_COLOR['face'],
                     markeredgecolor = HAYABUSA_COLOR['edge'])
        xy_plot.plot([0], [0], marker = "o", markersize = 10, zorder = 20,
                     markerfacecolor = ITOKAWA_COLOR['face'],
                     markeredgecolor = ITOKAWA_COLOR['edge'])
        xy_plot.plot(track[:idx_plot, 0], track[:idx_plot, 1], color = HAYABUSA_COLOR['track'], zorder = 10)
        for xy in r_grid:
            xy_plot.plot(xy[:,0], xy[:,1], color = GRID_COLOR, zorder = 0)
        for xy in theta_grid:
            xy_plot.plot(xy[:,0], xy[:,1], color = GRID_COLOR, zorder = 0)

        xz_plot = plt.subplot(2,1,2)
        xz_plot.set_xlim([-15, 15])
        xz_plot.set_ylim([-15, 15])
        xz_plot.set_aspect('equal')
        xz_plot.set_xlabel('X [km]')
        xz_plot.set_ylabel('Z [km]')
        xz_plot.set_xticks((-15, -10, -5, 0, 5, 10, 15))
        xz_plot.set_yticks((-15, -10, -5, 0, 5, 10, 15))

        xz_plot.plot(h_plot[0], h_plot[2], marker = "o", zorder = 30,
                     markerfacecolor = HAYABUSA_COLOR['face'],
                     markeredgecolor = HAYABUSA_COLOR['edge'])
        xz_plot.plot([0], [0], marker = "o", markersize = 10, zorder = 20,
                     markerfacecolor = ITOKAWA_COLOR['face'],
                     markeredgecolor = ITOKAWA_COLOR['edge'])
        xz_plot.plot(track[:idx_plot, 0], track[:idx_plot, 2], color = HAYABUSA_COLOR['track'], zorder = 10)
        xz_plot.plot([-20,20],[0, 0], color = GRID_COLOR, zorder = 0)
        xz_plot.plot([0,0],[20, -20], color = GRID_COLOR, zorder = 0)
        

        # 画像保存とプロット消去
        fig.savefig(png_filename)
        fig.clf()
        
    return


    



def extract_pos_and_quaternion(begin_dt, end_dt, delta_t,
                               target_body, target_frame):
    """
    Targetの位置、姿勢を求める
    位置は太陽系重心位置基準、座標系は黄道座標系
    姿勢はクォータニオンで表現する
    :param begin_dt: 計算を開始する時刻
    :type begin_dt: datetime.datetime
    :param end_dt: 計算を終了する時刻
    :type end_dt: datetime.datetime
    :param delta_t: 計算間隔[sec]
    :type delta_t: float
    :param target_body: 計算対象の名称(SPICEのBody名)
    :type target_body: str
    :param target_frame: 計算対象の座標系(SPICEのFrame名)
    :type target_frame: str
    :return: 位置と姿勢と時刻をまとめたdictデータ
      ['pos'] : 位置(x, y, z) [km]
      ['quaternion'] : 姿勢(クォータニオン) [rad]
      ['et'] : 時刻(Ephemeris time) [sec]
    :rtype: dict
    """
    et_array = np.arange(spiceExt.datetime2et(begin_dt),
                         spiceExt.datetime2et(end_dt), delta_t) # 計算対象の時刻
    pos = spiceExt.spkezr(target_body, et_array, INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0][:, :3] # posのみ
    attitude = spiceExt.pxform(target_frame, INERTIAL_FRAME, et_array) # 姿勢変換行列
    quaternion = spiceExt.m2q(attitude) # 行列→クォータニオンに変換
    result = {}
    result['pos'] = pos
    result['quaternion'] = quaternion
    result['et'] = et_array
    return result
    


def calc_sun_direction_q(itokawa_info):
    """
    イトカワからみた太陽の方位を計算する
    基準状態：+X方向に太陽
    基準状態からの回転量をクォータニオンで表現する
    """
    n = itokawa_info['pos'].shape[0]
    sun_vec = -itokawa_info['pos']
    sun_vec /= norm(sun_vec, axis = 1).reshape((n, 1))
    azim = np.arctan2(sun_vec[:, 1], sun_vec[:, 0])
    elev = np.arcsin(sun_vec[:, 2])
    sun_direction_q = spiceExt.m2q(spiceExt.eul2m(-azim, elev, np.zeros(n), 3, 2, 1))
    # 座標系の回転は、1 z軸周りにazim、2 y軸周りに-elev、3 x軸周りは操作しない
    # ただし、spiceのeul2m関数で算出される行列は、「座標系の回転行列」ではなく「座標変換行列」である
    # すなわち、元の座標系で(u, v, w)で表現されるベクトルは、回転後の座標系では(u', v', w')となるとき
    # (u', v', w')^T = m (u, v, w)^T となるmが返ってくる
    # 一方、blenderでの回転操作は「座標系の回転行列」m'から生成したクォータニオンを指定する必要がある
    # m'とmの関係はm' = inv(m)である
    # ここでm'は1 x軸周りに操作しない、2 y軸まわりにelev、3 z軸まわりに-azim回転によって得ることができる
    return sun_direction_q


def calc_back_camera_info(hayabusa_info, itokawa_info):
    """
    後方追尾カメラの位置、姿勢を計算する
    """
    et_array = hayabusa_info['et']
    n = et_array.shape[0]
    hay_vec = hayabusa_info['pos'] - itokawa_info['pos']
    hay_distance = norm(hay_vec, axis = 1).reshape((n, 1))

    hay_vec_1 = hay_vec / hay_distance # 長さ1に正規化
    azim = np.arctan2(hay_vec_1[:, 1], hay_vec_1[:, 0]) + np.deg2rad(B_CAM_AZIM_OFFSET)
    elev = np.arcsin(hay_vec_1[:, 2]) + np.deg2rad(B_CAM_ELEV_OFFSET)
    
    bcam_pos = np.zeros((n, 3))
    bcam_pos[:, 0] = B_CAM_OFFSET * np.cos(elev) * np.cos(azim)
    bcam_pos[:, 1] = B_CAM_OFFSET * np.cos(elev) * np.sin(azim)
    bcam_pos[:, 2] = B_CAM_OFFSET * np.sin(elev)
    bcam_pos += hay_vec

    bcam_pos_1 = bcam_pos / norm(bcam_pos, axis = 1).reshape((n, 1))
    azim = np.arctan2(bcam_pos_1[:, 1], bcam_pos_1[:, 0])
    elev = np.arcsin(bcam_pos_1[:, 2])
    bcam_q = spiceExt.m2q(spiceExt.eul2m(-azim, elev, np.zeros(n), 3, 2, 1))
    bcam_info = {'pos':bcam_pos, 'quaternion':bcam_q}

    return bcam_info


# def calc_amica_near_info(hayabusa_info, itokawa_info):
#     q = spiceExt.m2q(spiceExt.pxform(AMICA_FRAME, INERTIAL_FRAME,hayabusa_info['et']))
#     pos = (hayabusa_info['pos']  - itokawa_info['pos']) * AMICA_NEAR_RATE
#     amica_near_info = {'pos':pos, 'quaternion':q}
#     return amica_near_info


def export_for_blender(hayabusa_info, itokawa_info, bcam_info, filename):
    """
    :param hayabusa_info: はやぶさの情報（位置、姿勢）
    :type hayabusa_info: dict
    :param itokawa_info: イトカワの情報（位置、姿勢）
    :type itokawa_info: dict
    :param bcam_info: 後方追尾カメラの情報（位置、姿勢）
    :type bcam_info: dict
    :param amica_near_info: AMICA代替カメラの情報（位置、姿勢）
    :type amica_near_info: dict
    :param filename: 書き出すファイル名
    :type filename: str
    """
    et_array = hayabusa_info['et'] # 時刻の一覧
    n = et_array.shape[0]          # 総データ数
    rel_pos_hayabusa_to_itokawa = hayabusa_info['pos'] - itokawa_info['pos'] # イトカワ基準のはやぶさの位置
    # イトカワからみた太陽の方角（クォータニオンで表現する）
    sun_direction_q = calc_sun_direction_q(itokawa_info)

    is_period_with_data = False # 位置データなどが正しく抽出できている期間か
    file_idx = 0                # 出力ファイルの通し番号

    start_str = spiceExt.et2datetime(et_array[0]).strftime("%Y%m%dT%H%M%S")
    stop_str = spiceExt.et2datetime(et_array[-1]).strftime("%Y%m%dT%H%M%S")
    step = et_array[1] - et_array[0]
    
    for idx in range(n):
        if np.isnan(rel_pos_hayabusa_to_itokawa[idx, 0]): # はやぶさの位置が正しく抽出されていない場合
            if is_period_with_data:                       # 抽出されている→されていないに変化した場合
                is_period_with_data = False
                output_file.close()                       # ファイルクローズ
            continue
        if not is_period_with_data: # はやぶさの位置が正しく抽出されていない→されているに変化した場合
            is_period_with_data = True
            # 新しいファイルを生成する
            file_idx += 1
            output_filename = "{}_{}_{:.0f}_{}_{:02d}.csv".format(start_str, stop_str, step, filename, file_idx)
            output_file = open(output_filename, "w")
            # 見出しを書き込む
            output_file.write("utc,hayabusa_pos,,,hayabusa_quaternion,,,,")
            output_file.write("itokawa_pos,,,itokawa_quaternion,,,,")
            output_file.write("sun_position,,,sun_direction,,,,")
            output_file.write("BackCamera_pos,,,BackCamera_quaternion,,,,\n")
        # データをファイルに記入する
        output_file.write(spiceExt.et2datetime(et_array[idx]).isoformat()) # 日付
        for xyz_pos in rel_pos_hayabusa_to_itokawa[idx, :]: # はやぶさの位置(x, y, z)
            output_file.write(",{:.5e}".format(xyz_pos))
        for wxyz_quaterion in hayabusa_info['quaternion'][idx, :]: # はやぶさの姿勢(qw, qx, qy, qz)
            output_file.write(",{:.5e}".format(wxyz_quaterion))
        output_file.write(",0,0,0") # イトカワの位置(x, y, z)
        for wxyz_quaterion in itokawa_info['quaternion'][idx, :]: # イトカワの姿勢(qw, qx, qy, qz)
            output_file.write(",{:.5e}".format(wxyz_quaterion))
        output_file.write(",0,0,0") # 太陽の位置(x, y, z) 方角のみが意味を持つのでダミーの値
        for wxyz_quaterion in sun_direction_q[idx, :]: # 太陽の方向(qw, qx, qy, qz)
            output_file.write(",{:.5e}".format(wxyz_quaterion))
        for xyz_pos in bcam_info['pos'][idx, :]: # 後方カメラの位置(x, y, z)
            output_file.write(",{:.5e}".format(xyz_pos))
        for wxyz_quaterion in bcam_info['quaternion'][idx, :]: # 後方カメラの姿勢(qw, qx, qy, qz)
            output_file.write(",{:.5e}".format(wxyz_quaterion))
        # for xyz_pos in amica_near_info['pos'][idx, :]: # AMICA代替カメラの位置(x, y, z)
        #     output_file.write(",{:.5e}".format(xyz_pos))
        # for wxyz_quaterion in amica_near_info['quaternion'][idx, :]: # AMICA代替カメラの姿勢(qw, qx, qy, qz)
        #     output_file.write(",{:.5e}".format(wxyz_quaterion))
        output_file.write("\n")
    if is_period_with_data:
        output_file.close()
    return
        

def csvstr2datetime(csv_str):
    """
    コンマ区切りの文字列からdatetimeオブジェクトを生成する
    :param csv_str: コンマ区切りの文字列(year, month, day, hour, minute, second)
    :type csv_str: str
    :return: datetimeオブジェクト
    :rtype: datetime
    """
    num_list = [int(s) for s in csv_str.split(',')]
    dt_param = [0, 0, 0, 0, 0, 0]
    for idx, num in enumerate(num_list):
        dt_param[idx] = num
    dt = datetime.datetime(dt_param[0], dt_param[1], dt_param[2], dt_param[3], dt_param[4], dt_param[5])
    return dt


if __name__ == "__main__":
    # コマンドラインオプションの処理
    parser = argparse.ArgumentParser(description='イトカワランデブー中のはやぶさの情報を抽出する')
    parser.add_argument('--start', dest='start', type = str,
                        help = '抽出を開始する時刻(year, month, day, hour, minute, second)')
    parser.add_argument('--stop', dest='stop', type = str,
                        help = '抽出を終了する時刻(year, month, day, hour, minute, second)')
    parser.add_argument('--step', dest='step', type = float, default = DELTA_T,
                        help = '間隔[sec]')
    parser.add_argument('--out', dest='out_filename', type = str, default = "forBlender",
                        help = 'Blenderに渡すCSVファイル名(拡張子なし)')
    args = parser.parse_args()
    start_dt = csvstr2datetime(args.start)
    stop_dt = csvstr2datetime(args.stop)
    
    # カーネル読み込み
    spice.furnsh(KERNEL_LIST)
    
    # はやぶさ、イトカワの情報を抽出する
    hayabusa_info = extract_pos_and_quaternion(start_dt, stop_dt, args.step, HAYABUSA, HAYABUSA_FRAME)
    itokawa_info = extract_pos_and_quaternion(start_dt, stop_dt, args.step, ITOKAWA, ITOKAWA_FRAME)
    bcam_info = calc_back_camera_info(hayabusa_info, itokawa_info)
    # amica_near_info = calc_amica_near_info(hayabusa_info, itokawa_info)
    
    # Blender用の情報を書き出す
    export_for_blender(hayabusa_info, itokawa_info, bcam_info, args.out_filename)

    # プロットを作成する
    plot_track(start_dt, stop_dt, args.step, args.out_filename)
