import csv
import numpy as np
import bpy
import os
import fnmatch

#data file name
# CSV_FILE_NAME = '20051119T121000_20051120T000000_30_decent_01.csv'
# CSV_FILE_NAME = '20051022T161000_20051022T180000_30_tour_01.csv'
CSV_FILE_NAME = '20051119T120000_20051120T000000_30_decent_01.csv'
data_source = ""

# object name
HAYABUSA = bpy.data.objects['HAYABUSA_SC']
SUN = bpy.data.objects['Sun_Manipulator']
ITOKAWA = bpy.data.objects['Itokawa F0786432']
B_CAM = bpy.data.objects['b_cam_control']
AMICA_NEAR = bpy.data.objects['AMICA_near_control']

CAMERA_LIST = {'AMICA':bpy.data.objects['AMICA'],
               'ONC-W':bpy.data.objects['ONC-W'],
               'B-CAM':bpy.data.objects['b_cam']}
#               'AMICA_near':bpy.data.objects['AMICA_near']} 

#SCALE
KM_PER_GRID = 0.1
GRID_PER_KM = 1 / KM_PER_GRID

# frame rate
STEP = 1


def set_sun_direction(sun_direction):
    """
    :param sun_direction: sun direction (EulerXYZ)[deg]
    :sun_direction type: numpy.ndarray
    """
    sun_direction = np.deg2rad(sun_direction)
    SUN.rotation_mode = 'XYZ'
    SUN.rotation_euler = list(sun_direction)
    SUN.keyframe_insert(data_path = 'rotation_euler')
    return


def list_csv_file():
    """
    読み込むcsvファイルを選択する
    """
    # 現在のディレクトリの中のcsvファイル名の一覧を取得する
    csvfiles_in_cwd = fnmatch.filter(os.listdir('./'), '*.csv')
    return csvfiles_in_cwd


def read_from_csv(csv_filename):
    """
    csvファイルからデータを読み込む
    """
    utc_str = []
    hayabusa_pos_list = []
    hayabusa_q_list = []
    itokawa_pos_list = []
    itokawa_q_list = []
    sun_pos_list = []
    sun_q_list = []
    b_cam_pos_list = []
    b_cam_q_list = []
    #amica_near_pos_list = []
    #amica_near_q_list = []
    with open(csv_filename, "r") as csvfile:
        csv_reader = csv.reader(csvfile)
        next(csv_reader)        # 1行目はヘッダなので読み飛ばす
        for oneline in csv_reader:
            utc_str.append(oneline[0])
            hayabusa_pos_list.append([float(s) for s in oneline[1:4]])
            hayabusa_q_list.append([float(s) for s in oneline[4:8]])
            itokawa_pos_list.append([float(s) for s in oneline[8:11]])
            itokawa_q_list.append([float(s) for s in oneline[11:15]])
            sun_pos_list.append([float(s) for s in oneline[15:18]])
            sun_q_list.append([float(s) for s in oneline[18:22]])
            b_cam_pos_list.append([float(s) for s in oneline[22:25]])
            b_cam_q_list.append([float(s) for s in oneline[25:29]])
            #amica_near_pos_list.append([float(s) for s in oneline[29:32]])
            #amica_near_q_list.append([float(s) for s in oneline[32:36]])
    # 読み込んだ数値をnumpyの配列にする
    hayabusa_pos = np.array(hayabusa_pos_list)
    hayabusa_q = np.array(hayabusa_q_list)
    itokawa_pos = np.array(itokawa_pos_list)
    itokawa_q = np.array(itokawa_q_list)
    sun_pos = np.array(sun_pos_list)
    sun_q = np.array(sun_q_list)
    b_cam_pos = np.array(b_cam_pos_list)
    b_cam_q = np.array(b_cam_q_list)
    #amica_near_pos = np.array(amica_near_pos_list)
    #amica_near_q = np.array(amica_near_q_list)

    hayabusa_info = {'pos':hayabusa_pos, 'quaternion':hayabusa_q}
    itokawa_info = {'pos':itokawa_pos, 'quaternion':itokawa_q}
    sun_info = {'pos':sun_pos, 'quaternion':sun_q}
    b_cam_info = {'pos':b_cam_pos, 'quaternion':b_cam_q}
    #amica_near_info = {'pos':amica_near_pos, 'quaternion':amica_near_q}
    
    return utc_str, hayabusa_info, itokawa_info, sun_info, b_cam_info
    

def set_pos_and_attitude(target_object, pos, attitude):
    """
    オブジェクトの位置、姿勢を設定する
    """
    target_object.location = list(pos * GRID_PER_KM)
    target_object.rotation_quaternion = list(attitude)
    target_object.keyframe_insert(data_path = 'location')
    target_object.keyframe_insert(data_path = 'rotation_quaternion')
    return


def clear_animation():
    # clear all animation data
    for ob in bpy.data.objects:
        ob.animation_data_clear()

    # clear frame setting
    bpy.data.scenes['Scene'].frame_start = 1
    bpy.data.scenes['Scene'].frame_end = 1
    bpy.data.scenes['Scene'].frame_set(1)
    bpy.context.scene.timeline_markers.clear()  

    # clear data source filename
    global data_source
    data_source = ""

    return    

def set_animation(csv_filename):

    clear_animation()

    # read data file
    #os.chdir('C:/home/hidetaka/program/akatsuki_plot/hayabusa_onboard_cam')
    os.chdir(os.path.split(os.path.split(__file__)[0])[0])
    utc_str, hayabusa_info, itokawa_info, sun_info, b_cam_info = read_from_csv(csv_filename)
    global data_source
    data_source = os.path.splitext(csv_filename)[0]
        
    # フレームの範囲を設定
    total_frame = int(len(utc_str) / STEP)
    bpy.data.scenes['Scene'].frame_start = 1
    bpy.data.scenes['Scene'].frame_end = total_frame
    current_frame = 0

    # 各フレームにアニメーションの内容を設定していく
 
    for idx in range(0, len(utc_str), STEP):
        current_frame += 1
        bpy.data.scenes['Scene'].frame_set(current_frame)
        set_pos_and_attitude(HAYABUSA, hayabusa_info['pos'][idx, :], hayabusa_info['quaternion'][idx, :])
        set_pos_and_attitude(ITOKAWA, itokawa_info['pos'][idx, :], itokawa_info['quaternion'][idx, :])
        set_pos_and_attitude(SUN, sun_info['pos'][idx, :], sun_info['quaternion'][idx, :])
        set_pos_and_attitude(B_CAM, b_cam_info['pos'][idx, :], b_cam_info['quaternion'][idx, :])
        #set_pos_and_attitude(AMICA_NEAR, amica_near_info['pos'][idx, :], amica_near_info['quaternion'][idx, :])
        bpy.context.scene.timeline_markers.new(utc_str[idx], frame = current_frame)
    
    bpy.data.scenes['Scene'].frame_set(1)
    
    return


def render_animation():
    original_path = os.path.split(os.path.split(__file__)[0])[0]
    print(original_path)
    if bpy.data.scenes['Scene'].frame_end == 1:
        return
    filepath1 = bpy.context.scene.timeline_markers[0].name.replace(':','-')
    filepath2 = bpy.context.scene.timeline_markers[-1].name.replace(':','-')
    #filepath = '{}\\{}_{}'.format(original_path, filepath1, filepath2)
    filepath = '{}\\{}'.format(original_path, data_source)
    for k, v in CAMERA_LIST.items():
        bpy.context.scene.camera = v
        filepath_full = '{}\\{}'.format(filepath, k)
        # 保存先のフォルダがなかったら作成する
        if not os.path.exists(filepath_full):
            print("画像を出力するディレクトリが存在しないので作成します", flush = True)
            os.makedirs(filepath_full)
        print("rendering ... (CAMERA = {})".format(k), flush = True)
        bpy.data.scenes["Scene"].render.filepath = "{}\\{}".format(filepath_full, k)
        bpy.ops.render.render(animation=True)
        os.chdir(original_path)
    return

if __name__ == "__main__":
    set_animation(CSV_FILE_NAME)
