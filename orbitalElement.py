import math
import numpy as np
from numpy.linalg import norm
import scipy.optimize
import datetime

SECOND_PER_DAY = 24 * 60 * 60

def KeplarsEquation(eccentric_anomaly, mean_anomaly, eccentricity):            # ケプラー方程式
    """
    eccentric_anomaly : 離心近点離角[rad] 未知数
    mean_anomaly      : 平均近点離角[rad] 既知
    eccentricity     : 離心率 既知
    """
    return mean_anomaly - eccentric_anomaly + eccentricity * math.sin(eccentric_anomaly)

def KeplarsEquationPrime(eccentric_anomaly, mean_anomaly, eccentricity): # ケプラー方程式の1階微分
    return -1 + eccentricity * math.cos(eccentric_anomaly)

class orbitalElement:
    def __init__(self, semi_major_axis, eccentricity, period, inclination,
                 ascending_node, argument_periapsis, epoch, mean_anomaly):
        """
        semi_major_axis    : 軌道長半径
        eccentricity       : 離心率
        period             : 軌道周期[日]
        inclination        : 軌道傾斜角[deg]
        ascending_node     : 昇降点黄[赤]経 [deg]
        argument_periapsis : 近点離角[deg]
        epoch              : 元期(datetimeクラスで与える)
        mean_anomaly       : 元期における平均近点角[deg]
        """
        # 引数から軌道要素の数値を格納する
        self.semi_major_axis = semi_major_axis
        self.eccentricity = eccentricity
        self.period = period
        self.inclination = inclination
        self.ascending_node = ascending_node
        self.argument_periapsis = argument_periapsis
        self.epoch = epoch
        self.mean_anomaly_epoch = mean_anomaly
        
        self._update_valuables()
        
        return
        
    def _update_valuables(self):
        """
        軌道計算に関わる変数、行列を更新する
        """
        # 軌道計算のための行列、変数の準備
        self.inclination_rad = math.radians(self.inclination)
        self.ascending_node_rad = math.radians(self.ascending_node)
        self.argument_periapsis_rad = math.radians(self.argument_periapsis)
        self.mean_anomaly_epoch_rad = math.radians(self.mean_anomaly_epoch)
        
        rot_z_argument_periapsis = np.array([[math.cos(self.argument_periapsis_rad), -math.sin(self.argument_periapsis_rad), 0],
                                             [math.sin(self.argument_periapsis_rad),  math.cos(self.argument_periapsis_rad), 0],
                                             [0,                                 0,                                1]])
        rot_x_incination = np.array([[1, 0,                          0],
                                     [0, math.cos(self.inclination_rad), -math.sin(self.inclination_rad)],
                                     [0, math.sin(self.inclination_rad),  math.cos(self.inclination_rad)]])
        rot_z_asceding_node = np.array([[math.cos(self.ascending_node_rad), -math.sin(self.ascending_node_rad), 0],
                                        [math.sin(self.ascending_node_rad),  math.cos(self.ascending_node_rad), 0],
                                        [0,                             0,                            1]])
        self._rot_matrix = rot_z_asceding_node.dot(rot_x_incination.dot(rot_z_argument_periapsis))
        self.semi_minor_axis = self.semi_major_axis * math.sqrt(1 - self.eccentricity ** 2) # 軌道短半径
        self.mean_anomaly_per_day = 2 * math.pi / self.period # 1日当たりの平均近点角の増分[rad]
        self.mean_anomaly_per_second = self.mean_anomaly_per_day / SECOND_PER_DAY # 1秒当たりの平均近点角の増分[rad]
        self.r_apogee = self.semi_major_axis * (1 + self.eccentricity) # 遠日点距離
        self.v_apogee = (self.semi_major_axis * math.sqrt(1 - self.eccentricity ** 2) /
                         (1 + self.eccentricity) * self.mean_anomaly_per_second) # 遠日点通過速度[/sec]
        self.r_perigee = self.semi_major_axis * (1 - self.eccentricity) # 近日点距離
        self.v_perigee = self.v_apogee * self.r_apogee / self.r_perigee # 近日点通過速度[/sec]
        
        return


    def get_position_and_velocity(self, date):
        """
        ある時刻における天体の座標と速度を取得する
        date : 時刻(datetimeクラス)
        """
        # 指定時刻における平均近点角を求める
        days_from_epoch = (date - self.epoch).total_seconds() / SECOND_PER_DAY # 元期からの経過日数
        mean_anomaly = self.mean_anomaly_epoch_rad + days_from_epoch * self.mean_anomaly_per_day # 指定時刻における平均近点角
    
        # ケプラー方程式を解き、離心近点角を求める
        eccentric_anomaly = scipy.optimize.newton(func = KeplarsEquation, x0 = mean_anomaly, fprime = KeplarsEquationPrime,
                                                  args = (mean_anomaly, self.eccentricity))
        
        # 座標回転前の位置を求める
        pos_before_rot = np.zeros((3,1))
        pos_before_rot[0] = self.semi_major_axis * (math.cos(eccentric_anomaly) - self.eccentricity)
        pos_before_rot[1] = self.semi_minor_axis * math.sin(eccentric_anomaly)

        # 座標回転前の速度を得る
        velocity_before_rot = np.zeros((3,1))
        eccentric_anomaly_per_second = self.mean_anomaly_per_second / (1 - self.eccentricity * math.cos(eccentric_anomaly))
        velocity_before_rot[0] =  -self.semi_major_axis * eccentric_anomaly_per_second * math.sin(eccentric_anomaly)
        velocity_before_rot[1] =  self.semi_minor_axis * eccentric_anomaly_per_second * math.cos(eccentric_anomaly)
                
        # 座標回転を行い、座標値、速度を得る
        return self._rot_matrix.dot(pos_before_rot), self._rot_matrix.dot(velocity_before_rot)

        
    def get_position(self, date):
        """
        ある時刻における天体の座標を取得する
        date : 時刻(datetimeクラス)
        """
        return self.get_position_and_velocity(date)[0]

        
    def get_orbital_shape(self, divide_num):
        """
        軌道の形状をプロットするために、
        軌道形状のデータ列を取得する
        devide_num : 軌道1周分を何分割して出力するか
        """
        eccentric_anomaly = np.zeros(divide_num + 1)
        eccentric_anomaly[0: -1] = np.arange(0, 2 * math.pi, 2 * math.pi / divide_num)
        pos_before_rot = np.zeros((3, divide_num + 1))
        pos_before_rot[0, :] = self.semi_major_axis * (np.cos(eccentric_anomaly) - self.eccentricity)
        pos_before_rot[1, :] = self.semi_minor_axis * np.sin(eccentric_anomaly)

        return (self._rot_matrix.dot(pos_before_rot)).T
        

class JulianDay:
    """
    ユリウス通日を扱うクラス
    getJulianDay() : ユリウス通日を返す
    getDatetime() : グレゴリオ暦に変換してdatetime.datetimeオブジェクトを返す
    """
    def __init__(self, arg):
        """
        引数の形によって、初期化のやり方を変える
        arg(float or int) : argをユリウス通日として初期化する
        arg(datetimeオブジェクト) : argをグレゴリオ暦として初期化する
        """
        if isinstance(arg, datetime.datetime): # argがグレゴリオ暦の日付・時間の場合
            self._datetimeToJulianDay(arg)
            return
        if isinstance(arg, int) or isinstance(arg, float): # argが数値の場合
            self.jd = arg
            return
        return
        

    def _datetimeToJulianDay(self, datetime_glegorian):
        """
        グレゴリオ暦からユリウス通日を求める
        http://en.wikipedia.org/wiki/Julian_day
        """
        year = datetime_glegorian.year
        month = datetime_glegorian.month
        day = datetime_glegorian.day
        sub_day = datetime_glegorian.microsecond # microsecond
        sub_day = sub_day / 1e6 + datetime_glegorian.second # second
        sub_day = sub_day / 60.0 + datetime_glegorian.minute # minute
        sub_day = sub_day / 60.0 + datetime_glegorian.hour   # hour
        sub_day = sub_day / 24.0                             # day
        
        a = math.floor((14 - month) / 12)
        y = year + 4800 - a
        m = month + 12 * a - 3
        jdn = (day + math.floor((153 * m + 2) / 5) + 365 * y +
               math.floor(y / 4) - math.floor(y / 100) +
               math.floor(y / 400) - 32045)
        self.jd = jdn - 0.5 + sub_day
        return
               
    def getDatetime(self):
        """
        ユリウス通日からdatetimeオブジェクトを生成する
        http://mysteryart.web.fc2.com/library/calsmpl/cldttojd.html
        """

        jd = self.jd + 0.5
        z = math.floor(jd)
        sub_day = jd - z
        aa = math.floor((z - 1867216.25) / 36524.25)
        a = math.floor(z + 1 + aa - math.floor(aa / 4))
        b = a + 1524
        c = math.floor((b - 122.1) / 365.25)
        k = math.floor(365.25 * c)
        e = math.floor((b - k) / 30.6001)

        D = math.floor(b - k - math.floor(30.6001 * e))
        if e < 13.5:
            M = e - 1
        else:
            M = e - 13
        if M > 2.5:
            Y = c - 4716
        else:
            Y = c - 4715

        ms = int(sub_day * 24 * 60 * 60 * 1e6)
        sec = int(math.floor(ms / 1e6))
        ms = int(ms % 1e6)
        minute = int(math.floor(sec / 60))
        sec = int(sec % 60)
        hour = int(math.floor(minute / 60))
        minute = int(minute % 60)

        return datetime.datetime(Y, M, D, hour, minute, sec, ms)

        
    def getJuliunDay(self):
        return self.jd
        


def make_orbital_element_from_pos_v(pos, v, dt, GM):
    """
    ある時刻の位置、速度から軌道要素を求める
    :param pos: 位置ベクトル
    :type pos: numpy.ndarray
    :param v: 速度ベクトル
    :type v: numpy.ndarray
    :param dt: 日時
    :type dt: datetime.datetime
    :param GM: 中心天体のGM値
    :type GM: float
    """

    # 軌道傾斜角、昇交点黄経を求める
    n_orbital_plane = np.cross(pos, v) # 軌道面の法線ベクトル
    n_orbital_plane /= norm(n_orbital_plane)
    incl = np.arccos(n_orbital_plane[2]) # 軌道傾斜角[rad]
    ascending_node = np.arctan2(n_orbital_plane[1], n_orbital_plane[0]) + np.deg2rad(90) # 昇交点黄経[rad]
    # 軌道面をXY平面に変換する
    inv_incl_matrix = np.array([[1, 0, 0],
                            [0, np.cos(incl), np.sin(incl)],
                            [0, -np.sin(incl),  np.cos(incl)]])
    inv_ascending_node_matrix = np.array([[np.cos(ascending_node), np.sin(ascending_node), 0],
                                      [-np.sin(ascending_node),  np.cos(ascending_node), 0],
                                      [0, 0, 1]])
    inv_orbital_plane_matrix = inv_incl_matrix.dot(inv_ascending_node_matrix)
    pos_xy_plane = inv_orbital_plane_matrix.dot(pos)
    v_xy_plane = inv_orbital_plane_matrix.dot(v)

    # 近日点、遠日点の位置、速度を求める
    s = np.cross(pos_xy_plane, v_xy_plane)[2] # 動径、速度が作る平行四辺形の面積→常に一定の値
    e_by_m = norm(v_xy_plane)**2 / 2 - GM / norm(pos_xy_plane) # 力学的エネルギー / 質量→常に一定の値
    GM_by_s = GM / s
    v_p = GM_by_s + math.sqrt(GM_by_s **2 + 2 * e_by_m) # 近日点通過速度
    v_a = GM_by_s - math.sqrt(GM_by_s **2 + 2 * e_by_m) # 遠日点通過速度
    r_p = s / v_p                                       # 近日点距離
    r_a = s / v_a                                       # 遠日点距離
    # 軌道長半径、離心率を求める
    semi_major_axis = (r_p + r_a) / 2                   # 軌道長半径
    eccentricity = (semi_major_axis - r_p) / semi_major_axis # 離心率
    # 軌道周期を求める
    mean_motion = v_p * (1 - eccentricity) / (semi_major_axis * math.sqrt(1 - eccentricity**2)) # 平均運動
    period = 2 * math.pi / mean_motion # 軌道周期[sec]
    period_day = period / (24 * 60 * 60) # 軌道周期[日]

    # 近点引数を求める
    sin_e = pos_xy_plane.dot(v_xy_plane) / (semi_major_axis**2 * eccentricity * mean_motion) # sinE
    cos_e = -1/(2*eccentricity) * (norm(pos_xy_plane)**2 / semi_major_axis ** 2
                                   -1 + eccentricity**2 * sin_e **2 - eccentricity** 2)
    eccentric_anomaly = np.arctan2(sin_e, cos_e) # 離心近点離角
    argument_periapsis = (np.arctan2(pos_xy_plane[1], pos_xy_plane[0]) -
                          np.arctan2(semi_major_axis * math.sqrt(1 - eccentricity ** 2) * sin_e,
                                     semi_major_axis * (cos_e - eccentricity)))
    argument_periapsis = np.mod(argument_periapsis, 2*math.pi)
    # 平均近点離角を求める
    mean_anomaly = np.mod(eccentric_anomaly - eccentricity * sin_e, 2*math.pi)

    result = orbitalElement(semi_major_axis = semi_major_axis,
                            eccentricity = eccentricity,
                            period = period_day,
                            inclination = np.rad2deg(incl),
                            ascending_node = np.rad2deg(ascending_node),
                            argument_periapsis = np.rad2deg(argument_periapsis),
                            epoch = dt,
                            mean_anomaly = np.rad2deg(mean_anomaly))
    return result
