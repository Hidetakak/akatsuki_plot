# -*- coding:utf-8 -*-

"""
SpiceyPyの拡張
"""

import datetime
import re
import numpy as np
import spiceypy as spice


def spkezr(targ, et, ref, abcorr, obs):
    """
    spiceypy.spkezrの拡張
    etにnumpy arrayを指定できるようにした。
    
    :param targ: Target body name
    :type targ: str
    :param et: Obsever epoch
    :type et: float, list of float or numpy array
    :param ref: Reference frame of output state vector
    :type ref: str
    :param abcorr: Aberrations correction flag
    :type abcorr: str
    :param obs: observing body name
    :type obs: str
    :return:
        state of target
        One way light time between observer and target
    :rtype tuple
    """

    if type(et) == float:
        return spice.spkezr(targ, et, ref, abcorr, obs)
        
    if type(et) == np.ndarray:
        n = et.ravel().shape[0]          # 計算すべき時刻の数
        et_seq = et.ravel()              # etを1次元に並べる
    if type(et) == list:                 # etがfloatのlistの場合も同様に
        n = len(et)
        et_seq = et
    
    state = np.zeros((n, 6))
    lt = np.zeros(n)
    for idx, each_et in enumerate(et_seq):
        try:
            each_state, each_lt = spice.spkezr(targ = targ, et = float(each_et),
                                               ref = ref, abcorr = abcorr, obs = obs)
            state[idx, :] = each_state
            lt[idx] = each_lt
        except:                 # spkファイルから位置・速度を抽出できない場合は、NaNを返す
            state[idx, :] = np.NaN
            lt[idx] = np.NaN
    return (state, lt)


def et2datetime(et):
    """
    spiceのetからpythonのdatetimeを得る
    閏秒の場合（hh:mm:60）は hh:mm:59にする
    :param et: ephemeris time
    :type et: float
    :return:
        datetime instance
    :rtype: datetime.datetime
    """

    utc = list(map(int, re.split(r'[\-T:]', spice.et2utc(et, "ISOC", 0)))) # 年月日時分秒のリスト
    if utc[5] == 60:
        utc[5] -= 1             # うるう秒の処理
    return datetime.datetime(utc[0], utc[1], utc[2], utc[3], utc[4], utc[5])


def datetime2et(dt):
    """
    pythonのdatetimeオブジェクトからspiceのetを求める
    :param dt: datetimeオブジェクト
    :type dt: datetime.datetime
    :return:
        ephemeris time
    :rtype: spice.et
    """
    return spice.utc2et(dt.isoformat())


def pxform(from_frame, to_frame, et):
    """
    時刻etにおけるfrom_frameからto_frameへの回転行列を得る
    :param from_frame: 変換前の座標系の名前
    :type from_frame: str
    :param to_frame: 変換後の座標系の名前
    :type to_frame: str
    :param et: 時刻
    :type et: float, list of float or numpy array
    """

    if type(et) == float:
        return spice.pxform(from_frame, to_frame, et)
        
    if type(et) == np.ndarray:
        n = et.ravel().shape[0]          # 計算すべき時刻の数
        et_seq = et.ravel()              # etを1次元に並べる
    if type(et) == list:                 # etがfloatのlistの場合も同様に
        n = len(et)
        et_seq = et

    rotate_matrix = np.zeros((n, 3, 3)) # 回転行列を格納する配列
    for idx, each_et in enumerate(et_seq):
        try:                    # 行列が計算得られない場合があるため
            r_mat = spice.pxform(from_frame, to_frame, each_et)
        except:                 # 行列が得られない場合は直前の時刻の行列を用いる
            if idx != 0:
                rotate_matrix[idx, :, :] = rotate_matrix[idx - 1, :, :]
            print("回転行列計算不能 {}".format(spice.et2utc(each_et, "ISOC", 0))) # デバッグ用
            continue
        rotate_matrix[idx, :, :] = r_mat

    return rotate_matrix
    

def m2q(from_m):
    """
    回転行列をクオータニオンに変換する
    spiceypyのm2qの次元を拡張したもの
    spiceypyのm2qは3x3の回転行列→クオータニオン(4要素)のみに対応
    nx3x3の回転行列の配列→nx4のクオータニオンの配列への変換を可能にした
    :param from_m: 回転行列
    :type from_m: numpy.ndarray
    :return: クオータニオン
    :rtype: numpy.ndarray
    """
    if from_m.ndim == 2:
        return spice.m2q(from_m)
    n = from_m.shape[0]
    q = np.zeros((n, 4))
    for idx in range(n):
        try:
            q[idx, :] = spice.m2q(from_m[idx, :, :])
        except:
            print(from_m[idx, :, :])
    return q


def q2m(q):
    """
    クォータニオンを回転行列に変換する
    spiceypyのq2mの次元を拡張したもの
    spiceypyのq2mはクオータニオン(4要素)→3x3の回転行列のみに対応
    nx4のクオータニオンの配列→nx3x3の回転行列の配列への変換を可能にした
    :param q: クォータニオン
    :type q: numpy.ndarray
    :return: 回転行列
    :rtype: numpy.ndarray
    """
    if q.ndim == 1:
        return spice.q2m(q)
    n = q.shape[0]
    m = np.zeros((n, 3, 3))
    for idx in range(n):
        m[idx, :, :] = spice.q2m(q[idx])
    return m


def m2eul(r, axis3, axis2, axis1):
    """
    回転行列をオイラー角に変換する
    spiceypyのm2eulの次元を拡張したもの
    spiceypyのm2qは3x3の回転行列→オイラー角(3要素)のみに対応
    nx3x3の回転行列の配列→nx3のオイラー角の配列への変換を可能にした
    :param r: 回転行列
    :type from_m: numpy.ndarray
    :param axis3: 3番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :param axis2: 2番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :param axis1: 1番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :return: オイラー角
    :rtype: numpy.ndarray
    """
    if r.ndim == 2:
        return spice.m2eul(r, axis3, axis2, axis1)
    n = r.shape[0]
    eul = np.zeros((n, 3))
    for idx in range(n):
        try:
            eul[idx, :] = np.array(m2eul(r[idx, :, :], axis3, axis2, axis1))
        except:
            print("{}番目の行列{}は回転行列ではありません".format(idx, r[idx, :, :]))
    return eul


def eul2m(angle3, angle2, angle1, axis3, axis2, axis1):
    """
    オイラー角を回転行列に変換する
    spiceypyのeul2mの次元を拡張したもの
    spiceypyのm2qはオイラー角(3要素)→3x3の回転行列のみに対応
    nx3のオイラー角の配列→nx3x3の回転行列の配列への変換を可能にした
    :param angle3: 3番目に回す回転角[rad]
    :type angle3: float or numpy.adarray
    :param angle2: 2番目に回す回転角[rad]
    :type angle2: float or numpy.adarray
    :param angle1: 1番目に回す回転角[rad]
    :type angle1: float or numpy.adarray
    :param axis3: 3番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :param axis2: 2番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :param axis1: 1番目に回す軸の番号(1～3) ゼロオリジンではないことに注意
    :type axis3: int
    :return: 回転行列
    :rtype: numpy.ndarray
    """
    if type(angle1) == float or type(angle1) == int:
        return spice.eul2m(angle3, angle2, angle1, axis3, axis2, axis1)
    n = angle1.shape[0]
    m = np.zeros((n, 3, 3))
    for idx in range(n):
        m[idx, :, :] = spice.eul2m(angle3[idx], angle2[idx], angle1[idx],
                                   axis3, axis2, axis1)
    return m
        
