# -*- coding: utf-8 -*-

"""
はやぶさ2の軌道・イオンエンジンによる増速量をプロットする
"""


import csv
import os
import sys
import datetime
import argparse
import numpy as np
import numpy.linalg
import copy
import matplotlib.pyplot as plt
import bisect


# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbit


# 各イベントの日時
launch_date = datetime.datetime(2014,12,3,12,0,0) # 打ち上げ
earth_flyby_date = datetime.datetime(2015,12,3)  # 地球スウィングバイ
asteroid_arrival_date = datetime.datetime(2018, 6, 5) # 小惑星到着
propulsion_for_EGA_start_date = datetime.datetime(2015,1,10) # 地球スウィングバイのためのイオンエンジン運転開始
propulsion_for_EGA_end_date = datetime.datetime(2015,6,30) # 地球スウィングバイのためのイオンエンジン運転終了
propulsion_for_asteroid_start_date = datetime.datetime(2016,4,1) # 小惑星ランデブーのためのイオンエンジン運転開始
propulsion_for_asteroid_end_date = datetime.datetime(2018,6,5) # 小惑星ランデブーのためのイオンエンジン運転終了
haya2_2ndlap_start_date = earth_flyby_date
haya2_3rdlap_start_date = datetime.datetime(2017,2,3)


# プロットに関わる設定
PLOT_START_DATE = datetime.datetime(2014,6,1) # プロットを開始する日時
ORBIT_SHAPE_DIVIDE_NUM = 180    # 軌道形状を描画する際、軌道を何分割して描画するか
DELTA_V_SCALE = 5e10            # 増速量のベクトルをプロットする倍率
DELTA_V_LOWER_TH = 5e-5          # 増速量をプロットする下限値
SUN_COLOR = "yellow"
HAYA2_COLOR = "orange"
HAYA2_COLOR_PALE = "#ffcf89"
# HAYA2_TRACK_COLOR_1STLAP = "#fabf8f"
# HAYA2_TRACK_COLOR_2NDLAP = "#92cddc"
# HAYA2_TRACK_COLOR_3RDLAP = "#b1a0c7"
HAYA2_TRACK_COLOR_1STLAP = "#b1a0c7"
HAYA2_TRACK_COLOR_2NDLAP = "#b1a0c7"
HAYA2_TRACK_COLOR_3RDLAP = "#b1a0c7"
HAYA2_ARROW_COLOR = "red"
EARTH_COLOR = "blue"
EARTH_COLOR_PALE = "#8080ff"
MARS_COLOR = "#ff5050"
MARS_COLOR_PALE = "#ff8080"
ASTEROID_COLOR = "#00ff00"
ASTEROID_COLOR_PALE = "#80ff80"
PLOT_IMAGE_SAVE_PATH = ".\\plot_images\\"


def main(haya2_filename, planets_filename, output_csv_filename):

    # csvファイルから惑星・小惑星の軌道要素のデータを読み込む（地球、火星、1999JU3の順）
    with open(planets_filename, "r") as planets_orbit_csv:
        reader = csv.reader(planets_orbit_csv)
        planets_orbital_element = []
        planets_orbital_shape = []
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for current_line_csv in reader:
            orbital_element_current_line = orbit.orbitalElement(semi_major_axis = float(current_line_csv[12]),
                                                                eccentricity = float(current_line_csv[3]),
                                                                period = float(current_line_csv[14]) / orbit.SECOND_PER_DAY,
                                                                inclination = float(current_line_csv[5]),
                                                                ascending_node = float(current_line_csv[6]),
                                                                argument_periapsis = float(current_line_csv[7]),
                                                                epoch = orbit.JulianDay(float(current_line_csv[1])).getDatetime(),
                                                                mean_anomaly = float(current_line_csv[10]))
            planets_orbital_element.append(copy.deepcopy(orbital_element_current_line))
            orbital_shape_current_line = orbital_element_current_line.get_orbital_shape(ORBIT_SHAPE_DIVIDE_NUM)
            planets_orbital_shape.append(copy.deepcopy(orbital_shape_current_line))

    
    # csvファイルからはやぶさ2の軌道要素の時系列データを読み込む
    mission_date_list = []       # 位置・速度を計算する時刻のリスト
    with open(haya2_filename, "r") as haya2_orbit_csv:
        haya2_orbital_element = []
        reader = csv.reader(haya2_orbit_csv)
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for current_line_csv in reader:
            current_time = orbit.JulianDay(float(current_line_csv[0])).getDatetime() # 日時：ユリウス通日→datetimeオブジェクトに変換
            mission_date_list.append(current_time)
            orbital_element_current_line = orbit.orbitalElement(semi_major_axis = float(current_line_csv[11]),
                                                                eccentricity = float(current_line_csv[2]),
                                                                period = float(current_line_csv[13]) / orbit.SECOND_PER_DAY,
                                                                inclination = float(current_line_csv[4]),
                                                                ascending_node = float(current_line_csv[5]),
                                                                argument_periapsis = float(current_line_csv[6]),
                                                                epoch = current_time,
                                                                mean_anomaly = float(current_line_csv[9]))
            haya2_orbital_element.append(copy.deepcopy(orbital_element_current_line))

    # 打ち上げ前のはやぶさ2の軌道情報を作る
    time_step = mission_date_list[1] - mission_date_list[0]
    pre_launch_mission_date_list = []
    pre_launch_haya2_orbital_element = []
    current_date = PLOT_START_DATE
    while current_date < mission_date_list[0]:
        pre_launch_mission_date_list.append(current_date)
        pre_launch_haya2_orbital_element.append(copy.deepcopy(planets_orbital_element[0]))
        current_date += time_step

    mission_date_list = pre_launch_mission_date_list + mission_date_list
    haya2_orbital_element = pre_launch_haya2_orbital_element + haya2_orbital_element

    
    # 軌道要素から、位置・速度・増速量の時系列データを計算して行列に格納する
    haya2_pos_velocity = np.zeros((len(haya2_orbital_element), 9)) # 各時刻につき x, y, z, vx, vy, vz, dlt_vx, dlt_vy, dlt_vz の9要素を計算する
    planets_pos = np.zeros((haya2_pos_velocity.shape[0], 9))     # 地球、火星、1999JU3それぞれのx, y, z 計9要素
    for index in range(len(haya2_orbital_element)):
        # はやぶさ2の位置・速度計算・増速量
        pos, velocity = haya2_orbital_element[index].get_position_and_velocity(mission_date_list[index])
        if index == 0:
            deltaV = np.zeros((3,1))
        else:
            deltaV = velocity - haya2_orbital_element[index - 1].get_position_and_velocity(mission_date_list[index])[1]
        
        haya2_pos_velocity[index, 0:3] = pos.T
        haya2_pos_velocity[index, 3:6] = velocity.T
        haya2_pos_velocity[index, 6:9] = deltaV.T

        # 惑星・小惑星の位置計算
        for planet_index in range(len(planets_orbital_element)):
            pos = planets_orbital_element[planet_index].get_position(mission_date_list[index])
            planets_pos[index, (planet_index * 3) : ((planet_index + 1) * 3)] = pos.T

    # 計算結果をCSVファイルに出力する
    with open(output_csv_filename, mode = "w") as output_csv:
        # ヘッダ行を出力する
        output_csv.write("Date,")
        output_csv.write("Earth_x,Earth_y,Earth_z,")
        output_csv.write("Mars_x,Mars_y,Mars_z,")
        output_csv.write("1999JU3_x,1999JU3_y,1999JU3_z,")
        output_csv.write("Hayabusa2_x,Hayabusa2_y,Hayabusa2_z,Hayabusa2_vx,Hayabusa2_vy,Hayabusa2_vz,")
        output_csv.write("Hayabusa2_delta_vx,Hayabusa2_delta_vy,Hayabusa2_delta_vz\n")
        output_csv.write("," + "[km]," * 12)
        output_csv.write("[km/sec]," * 3)
        output_csv.write("[km/sec/step]," * 3 + "\n")

        for index in range(len(mission_date_list)):
            # 日付の出力
            output_csv.write(mission_date_list[index].strftime("%Y/%m/%d %H:%M:%S,"))
            # 惑星の位置を出力する
            for column in range(planets_pos.shape[1]):
                output_csv.write("{:.10e},".format(planets_pos[index, column]))
            # はやぶさ2の位置、速度、増速量を出力する
            for column in range(haya2_pos_velocity.shape[1]):
                output_csv.write("{:.10e},".format(haya2_pos_velocity[index, column]))
            output_csv.write("\n")
            
    # 計算結果をプロットしていく
    fig = plt.figure()
    if not os.path.exists(PLOT_IMAGE_SAVE_PATH):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(PLOT_IMAGE_SAVE_PATH)
    haya2_1stlap_start_index = bisect.bisect(mission_date_list, launch_date)
    haya2_2ndlap_start_index = bisect.bisect(mission_date_list, haya2_2ndlap_start_date)
    haya2_3rdlap_start_index = bisect.bisect(mission_date_list, haya2_3rdlap_start_date)
    for index in range(0, len(mission_date_list), 1):
        print("{:05d} / {:05d}".format(index, len(mission_date_list)), end = "", flush = True)
        # グラフの準備
        ax = fig.add_subplot(111)
        ax.axis([-3e8, 3e8, -3e8, 3e8])
        ax.set_aspect('equal')
        plt.xticks([-3e8, -2e8, -1e8, 0, 1e8, 2e8, 3e8])
        plt.yticks([-3e8, -2e8, -1e8, 0, 1e8, 2e8, 3e8])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # グラフの描画
        ax.plot(planets_orbital_shape[0][:, 0], planets_orbital_shape[0][:, 1], color = EARTH_COLOR_PALE, zorder = 1) # 地球軌道
        ax.plot(planets_orbital_shape[1][:, 0], planets_orbital_shape[1][:, 1], color = MARS_COLOR_PALE, zorder = 1) # 火星軌道
        ax.plot(planets_orbital_shape[2][:, 0], planets_orbital_shape[2][:, 1], color = ASTEROID_COLOR_PALE, zorder = 1) # 1999JU3軌道

        ax.plot(0, 0, color = 'yellow', marker = "o", markersize = 10, zorder = 5) # 太陽
        ax.plot(planets_pos[index, 0], planets_pos[index, 1], color = EARTH_COLOR, marker = "o", markersize = 8, zorder = 5) # 地球
        ax.plot(planets_pos[index, 3], planets_pos[index, 4], color = MARS_COLOR, marker = "o", markersize = 8, zorder = 5) # 火星
        ax.plot(planets_pos[index, 6], planets_pos[index, 7], color = ASTEROID_COLOR, marker = "o", markersize = 8, zorder = 5) # 1999JU3

        if mission_date_list[index] > launch_date: # 打ち上げ後の場合ははやぶさ2の情報を描画する
            # 目標天体とDawnの会合予想時刻での予測位置を描画
            # if propulsion_for_EGA_start_date < mission_date_list[index] and mission_date_list[index] < earth_flyby_date: # EGA前
            #     earth_flyby_pos = planets_orbital_element[0].get_position(earth_flyby_date)
            #     haya2_flyby_pos = haya2_orbital_element[index].get_position(earth_flyby_date)
            #     ax.plot(earth_flyby_pos[0], earth_flyby_pos[1], marker = "o", markersize = 8,
            #             markerfacecolor = EARTH_COLOR_PALE, markeredgecolor = EARTH_COLOR, zorder = 5)
            #     ax.plot(haya2_flyby_pos[0], haya2_flyby_pos[1], marker = "s", markersize = 8,
            #             markerfacecolor = HAYA2_COLOR_PALE, markeredgecolor = HAYA2_COLOR, zorder = 5)
            if earth_flyby_date < mission_date_list[index] and mission_date_list[index] < asteroid_arrival_date: # 1999JU3アプローチ中
                asteroid_rendez_vous_pos = planets_orbital_element[2].get_position(asteroid_arrival_date)
                haya2_rendez_vous_pos = haya2_orbital_element[index].get_position(asteroid_arrival_date)
                ax.plot(asteroid_rendez_vous_pos[0], asteroid_rendez_vous_pos[1], marker = "o", markersize = 8,
                        markerfacecolor = ASTEROID_COLOR_PALE, markeredgecolor = ASTEROID_COLOR, zorder = 8)
                ax.plot(haya2_rendez_vous_pos[0], haya2_rendez_vous_pos[1], marker = "s", markersize = 8,
                        markerfacecolor = HAYA2_COLOR_PALE, markeredgecolor = HAYA2_COLOR, zorder = 8)

            # はやぶさ2の軌道(弾道軌道）を描画
            # if propulsion_for_EGA_start_date < mission_date_list[index]:
            if launch_date < mission_date_list[index]:
                haya2_orbit_shape_current_time = haya2_orbital_element[index].get_orbital_shape(ORBIT_SHAPE_DIVIDE_NUM)
                ax.plot(haya2_orbit_shape_current_time[:, 0], haya2_orbit_shape_current_time[:, 1], color = HAYA2_COLOR_PALE, zorder = 7, linewidth = 2)

            # はやぶさ2の軌跡を描画
            if index < haya2_2ndlap_start_index: # 現在の周回が1週目なら
                ax.plot(haya2_pos_velocity[haya2_1stlap_start_index:index, 0],
                        haya2_pos_velocity[haya2_1stlap_start_index:index, 1],
                        color = HAYA2_TRACK_COLOR_1STLAP, zorder = 7, linewidth = 2)
            else:
                ax.plot(haya2_pos_velocity[haya2_1stlap_start_index:haya2_2ndlap_start_index, 0],
                        haya2_pos_velocity[haya2_1stlap_start_index:haya2_2ndlap_start_index, 1],
                        color = HAYA2_TRACK_COLOR_1STLAP, zorder = 7, linewidth = 2)
                if index < haya2_3rdlap_start_index: # 現在の周回が2週目なら
                    ax.plot(haya2_pos_velocity[haya2_2ndlap_start_index:index, 0],
                            haya2_pos_velocity[haya2_2ndlap_start_index:index, 1],
                            color = HAYA2_TRACK_COLOR_2NDLAP, zorder = 7, linewidth = 2)
                else:               # 現在の周回が3週目なら
                    ax.plot(haya2_pos_velocity[haya2_2ndlap_start_index:haya2_3rdlap_start_index, 0],
                            haya2_pos_velocity[haya2_2ndlap_start_index:haya2_3rdlap_start_index, 1],
                            color = HAYA2_TRACK_COLOR_2NDLAP, zorder = 7, linewidth = 2)
                    ax.plot(haya2_pos_velocity[haya2_3rdlap_start_index:index, 0],
                            haya2_pos_velocity[haya2_3rdlap_start_index:index, 1],
                            color = HAYA2_TRACK_COLOR_3RDLAP, zorder = 7, linewidth = 2)

            # はやぶさ2のイオンエンジンによる増速ベクトルを描画
            if (((propulsion_for_EGA_start_date < mission_date_list[index] and mission_date_list[index] < propulsion_for_EGA_end_date) or
                 (propulsion_for_asteroid_start_date < mission_date_list[index] and mission_date_list[index] < propulsion_for_asteroid_end_date)) and
                numpy.linalg.norm(haya2_pos_velocity[index, 6:9]) > DELTA_V_LOWER_TH):
                ax.arrow(haya2_pos_velocity[index, 0], haya2_pos_velocity[index, 1],
                         haya2_pos_velocity[index, 6] * DELTA_V_SCALE, haya2_pos_velocity[index, 7] * DELTA_V_SCALE,
                         head_width = 1e7, fc = HAYA2_ARROW_COLOR , ec = HAYA2_ARROW_COLOR, zorder = 15)

            # はやぶさ2の位置を描画
            ax.plot(haya2_pos_velocity[index, 0], haya2_pos_velocity[index, 1], marker = "s", markersize = 8, color = HAYA2_COLOR, zorder = 10)

        # 日付を出力
        plt.text(1.5e8, 2.5e8, mission_date_list[index].strftime("%Y/%m/%d"))
        
        # グラフ出力
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("orbit_{:05d}.png".format(index))
        fig.savefig(output_png_file_name, dpi = 100)

        # グラフを消去
        plt.clf()
        print("\b" * 13, end = "", flush = True)
    return


if __name__ == "__main__":
    # コマンドラインオプションの処理
    parser = argparse.ArgumentParser()
    parser.add_argument("--haya2_orbit",
                        action = "store",
                        default = "20150228_hayabusa2_orbital_element.csv",
                        help = "はやぶさ2の軌道要素のCSVファイル名",
                        type = str)
    parser.add_argument("--planets_orbit",
                        action = "store",
                        default = "planets_orbital_elements.csv",
                        help = "地球、火星、1999JU3の軌道要素のCSVファイル名",
                        type = str)
    parser.add_argument("--output_csv",
                        action = "store",
                        default = "calc_result.csv",
                        help = "計算結果を出力するCSVファイル名",
                        type = str)
    args = parser.parse_args()         # 引数解析

    main(haya2_filename = args.haya2_orbit, planets_filename = args.planets_orbit, output_csv_filename = args.output_csv)
