# -*- coding:utf-8 -*-

"""
地球を固定した座標系ではやぶさ2の動きをプロットする
"""

import csv
import os
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
import math
import datetime
import bisect

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbit





# プロットに関わる設定
HAYA2_COLOR = "orange"
HAYA2_COLOR_PALE = "#ffcf89"
HAYA2_PREDICT_COLOR = "#b1a0c7"
HAYA2_TRACK_COLOR = "blue"
HAYA2_TRACK_IES_ON_COLOR = "red"

EARTH_COLOR = "blue"
PLOT_IMAGE_SAVE_PATH = ".\\earth_fix_plot_images\\" # プロットを格納するフォルダ

IES_DRIVE_DATE = [datetime.datetime(2014,12,3,0,0,0),datetime.datetime(2015,1,11,6,0,0), datetime.datetime(2015,1,21,0,0,0),
                  datetime.datetime(2015,3,6,6,0,0), datetime.datetime(2015,3,27,0,0,0),
                  datetime.datetime(2015,6,5,0,0,0), datetime.datetime(2015,6,12,0,0,0)]

def main(datelist, sc_orb_elem_list, earth_orb_elem, csv_filename, png_filename, step):
    num_date = len(datelist)    # 計算する時間データの数
    orbital_shape_array = np.zeros((num_date, num_date, 3))
    # orbital_shape_earth_fix_array = np.zeros((num_date, num_date, 3))

    # 各時点での弾道軌道データを計算する
    print("各時点での慣性軌道データを計算中 ... ", end = "", flush = True)
    for date_index in range(num_date):
        for calc_date_index in range(num_date):
            orbital_shape_array[date_index, calc_date_index, :] = sc_orb_elem_list[date_index].get_position(datelist[calc_date_index]).ravel()
    print("完了")
            
    # 地球の位置との差分をとる
    print("太陽地球固定座標系へ変換中 ... ", end = "", flush = True)
    orbital_shape_earth_fix_array = np.zeros((num_date, num_date, 3))
    tracking_earth_fix = np.zeros((num_date, 3))
    for date_index in range(num_date):
        earth_pos = earth_orb_elem.get_position(datelist[date_index])
        orbital_shape_earth_fix_array[:, date_index, :] = orbital_shape_array[:, date_index, :] - earth_pos.T
        earth_direction = math.atan2(earth_pos[1,0], earth_pos[0,0])
        rotate_matrix = np.array([[math.cos(earth_direction), math.sin(earth_direction), 0],
                                  [-math.sin(earth_direction), math.cos(earth_direction), 0],
                                  [0, 0, 1]])
        orbital_shape_earth_fix_array[:, date_index, :] = rotate_matrix.dot(orbital_shape_earth_fix_array[:, date_index, :].T).T
        tracking_earth_fix[date_index, :] = orbital_shape_earth_fix_array[date_index, date_index, :]
    print("完了")
    output_csv(datelist = datelist, output_filename = csv_filename, orbital_shape_array = orbital_shape_earth_fix_array, tracking = tracking_earth_fix)
    output_png(datelist = datelist, orbital_shape = orbital_shape_earth_fix_array, tracking = tracking_earth_fix, step = step, output_filename = png_filename)

    return
    
def output_csv(datelist, output_filename, orbital_shape_array, tracking):
    # csvファイルに出力
    print("csvファイルに出力中 ... ", end = "", flush = True)
    num_date = tracking.shape[0]
    with open(output_filename, "w") as output:
        output.write("時刻,いつ時点での軌道データか\n")
        output.write(",")
        for column in range(num_date):
            output.write("{},,,".format(datelist[column].strftime("%Y/%m/%d %H:%M")))
        output.write("\n")
        output.write(",")
        for column in range(num_date):
            output.write("x,y,z,")
        output.write("\n")
        for calc_date_index in range(num_date):
            output.write("{},".format(datelist[calc_date_index].strftime("%Y/%m/%d %H:%M")))
            for date_index in range(num_date):
                output.write("{:.10e},{:.10e},{:.10e},".format(orbital_shape_array[date_index, calc_date_index, 0],
                                                               orbital_shape_array[date_index, calc_date_index, 1],
                                                               orbital_shape_array[date_index, calc_date_index, 2]))
            output.write("\n")
        output.write("\n\n時刻,位置,,,最近接地点\n")
        for calc_date_index in range(num_date):
            output.write("{},".format(datelist[calc_date_index].strftime("%Y/%m/%d %H:%M:%S")))
            output.write("{:.10e},{:.10e},{:.10e},".format(tracking[calc_date_index, 0],
                                                           tracking[calc_date_index, 1],
                                                           tracking[calc_date_index, 2]))
            output.write("{:.10e},{:.10e},{:.10e}\n".format(orbital_shape_array[calc_date_index, -1, 0],
                                                            orbital_shape_array[calc_date_index, -1, 1],
                                                            orbital_shape_array[calc_date_index, -1, 2]))
                                                                    
    print("完了")
    return


def output_png(datelist, orbital_shape, tracking, step, output_filename):
    fig = plt.figure()
    if not os.path.exists(PLOT_IMAGE_SAVE_PATH):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(PLOT_IMAGE_SAVE_PATH)

    ies_on_off_index = []
    for ies_turn in range(len(IES_DRIVE_DATE)):
        ies_on_off_index.append(bisect.bisect(datelist, IES_DRIVE_DATE[ies_turn]))

    png_index = 0
    for index in range(0, len(datelist), step):
        print("{:05d} / {:05d}".format(index + 1, len(datelist)), end = "", flush = True)
        # グラフの準備
        ax = fig.add_subplot(111)
        ax.axis([-3e7, 3e7, -6e7, 1e7])
        ax.set_aspect('equal')
        plt.xticks([-3e7, -2e7, -1e7, 0, 1e7, 2e7, 3e7])
        plt.yticks([-6e7, -5e7, -4e7, -3e7, -2e7, -1e7, 0, 1e7])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # グラフの描画

        ax.plot(0, 0, color = EARTH_COLOR, marker = "o", markersize = 10, zorder = 5) # 地球
        ax.plot(orbital_shape[index, index:, 0], orbital_shape[index, index:, 1], color = HAYA2_PREDICT_COLOR) # この先の予測軌道

        if index < ies_on_off_index[1]: # 1回目の運転前
            ax.plot(tracking[0:index, 0], tracking[0:index, 1], color = HAYA2_TRACK_COLOR)
            plt.text(1e7, 0.2e7, "Coast", color = "blue")
        elif index < ies_on_off_index[2]: # 1回目の運転中
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:index, 0], tracking[ies_on_off_index[1]:index, 1], color = HAYA2_TRACK_IES_ON_COLOR)
            plt.text(1e7, 0.2e7, "Thrust", color = "red")
        elif index < ies_on_off_index[3]: # 2回目の運転前
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:ies_on_off_index[2], 0], tracking[ies_on_off_index[1]:ies_on_off_index[2], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[2]:index, 0], tracking[ies_on_off_index[2]:index, 1], color = HAYA2_TRACK_COLOR)
            plt.text(1e7, 0.2e7, "Coast", color = "blue")
        elif index < ies_on_off_index[4]: # 2回目の運転中
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:ies_on_off_index[2], 0], tracking[ies_on_off_index[1]:ies_on_off_index[2], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[2]:ies_on_off_index[3], 0], tracking[ies_on_off_index[2]:ies_on_off_index[3], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[3]:index, 0], tracking[ies_on_off_index[3]:index, 1], color = HAYA2_TRACK_IES_ON_COLOR)
            plt.text(1e7, 0.2e7, "Thrust", color = "red")
        elif index < ies_on_off_index[5]: # 3回目の運転前
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:ies_on_off_index[2], 0], tracking[ies_on_off_index[1]:ies_on_off_index[2], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[2]:ies_on_off_index[3], 0], tracking[ies_on_off_index[2]:ies_on_off_index[3], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[3]:ies_on_off_index[4], 0], tracking[ies_on_off_index[3]:ies_on_off_index[4], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[4]:index, 0], tracking[ies_on_off_index[4]:index, 1], color = HAYA2_TRACK_COLOR)
            plt.text(1e7, 0.2e7, "Coast", color = "blue")
        elif index < ies_on_off_index[6]: # 3回目の運転中
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:ies_on_off_index[2], 0], tracking[ies_on_off_index[1]:ies_on_off_index[2], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[2]:ies_on_off_index[3], 0], tracking[ies_on_off_index[2]:ies_on_off_index[3], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[3]:ies_on_off_index[4], 0], tracking[ies_on_off_index[3]:ies_on_off_index[4], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[4]:ies_on_off_index[5], 0], tracking[ies_on_off_index[4]:ies_on_off_index[5], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[5]:index, 0], tracking[ies_on_off_index[5]:index, 1], color = HAYA2_TRACK_IES_ON_COLOR)
            plt.text(1e7, 0.2e7, "Thrust", color = "red")
        else: # 3回目の運転後
            ax.plot(tracking[0:ies_on_off_index[1], 0], tracking[0:ies_on_off_index[1], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[1]:ies_on_off_index[2], 0], tracking[ies_on_off_index[1]:ies_on_off_index[2], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[2]:ies_on_off_index[3], 0], tracking[ies_on_off_index[2]:ies_on_off_index[3], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[3]:ies_on_off_index[4], 0], tracking[ies_on_off_index[3]:ies_on_off_index[4], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[4]:ies_on_off_index[5], 0], tracking[ies_on_off_index[4]:ies_on_off_index[5], 1], color = HAYA2_TRACK_COLOR)
            ax.plot(tracking[ies_on_off_index[5]:ies_on_off_index[6], 0], tracking[ies_on_off_index[5]:ies_on_off_index[6], 1], color = HAYA2_TRACK_IES_ON_COLOR)
            ax.plot(tracking[ies_on_off_index[6]:index, 0], tracking[ies_on_off_index[6]:index, 1], color = HAYA2_TRACK_COLOR)
            plt.text(1e7, 0.2e7, "Coast", color = "blue")
            
        ax.plot(tracking[index, 0], tracking[index, 1], marker = "s", color = HAYA2_COLOR, markersize = 8) # 現在位置
        ax.plot(orbital_shape[index, -1, 0], orbital_shape[index, -1, 1], marker = "s",
                markeredgecolor = HAYA2_COLOR, markerfacecolor = HAYA2_COLOR_PALE, markersize = 8) # 地球最近接地点

        # 日付を出力
        plt.text(1e7, 0.5e7, datelist[index].strftime("%Y/%m/%d"))
        
        # グラフ出力
        output_png_file_name = "{}{}_{:04d}.png".format(PLOT_IMAGE_SAVE_PATH, output_filename, png_index + 1)
        fig.savefig(output_png_file_name, dpi = 80)
        png_index += 1

        # グラフを消去
        plt.clf()
        print("\b" * 13, end = "", flush = True)

    
if __name__ == "__main__":
    # コマンドライン引数を処理する
    parser = argparse.ArgumentParser()
    parser.add_argument("--haya2_orbit",
                        action = "store",
                        default = "20150228_hayabusa2_orbital_element_EGA.csv",
                        help = "はやぶさ2の軌道要素のCSVファイル名",
                        type = str)
    parser.add_argument("--planets_orbit",
                        action = "store",
                        default = "planets_orbital_elements.csv",
                        help = "地球、火星、1999JU3の軌道要素のCSVファイル名",
                        type = str)
    parser.add_argument("--output_csv",
                        action = "store",
                        default = "earth_fix.csv",
                        help = "計算結果を出力するCSVファイル名",
                        type = str)
    parser.add_argument("--output_png",
                        action = "store",
                        default = "earth_fix",
                        help = "計算結果を出力するpng連番ファイル名",
                        type = str)
    parser.add_argument("--png_step",
                        action = "store",
                        default = 1,
                        help = "プロットを行う間隔",
                        type = int)

    args = parser.parse_args()         # 引数解析

    # csvファイルを読み込み配列に展開する
    with open(args.haya2_orbit, "r") as haya2_orbit_csv:
        datelist = []
        orbital_element_list = []
        haya2_orbit_reader = csv.reader(haya2_orbit_csv)
        next(haya2_orbit_reader)   # 1行目はヘッダなので読み飛ばす
        for oneline in haya2_orbit_reader:
            current_date = orbit.JulianDay(float(oneline[0])).getDatetime()
            datelist.append(current_date) # 日時
            orbital_element_list.append(orbit.orbitalElement(semi_major_axis = float(oneline[11]),
                                                             eccentricity = float(oneline[2]),
                                                             period = float(oneline[13]) / orbit.SECOND_PER_DAY,
                                                             inclination = float(oneline[4]),
                                                             ascending_node = float(oneline[5]),
                                                             argument_periapsis = float(oneline[6]),
                                                             epoch = current_date,
                                                             mean_anomaly = float(oneline[9])))
    with open(args.planets_orbit, "r") as planets_orbit_csv:
        planets_orbit_reader = csv.reader(planets_orbit_csv)
        planets_orbit_element_list = []
        next(planets_orbit_reader)
        for oneline in planets_orbit_reader:
            current_date = orbit.JulianDay(float(oneline[1])).getDatetime()
            planets_orbit_element_list.append(orbit.orbitalElement(semi_major_axis = float(oneline[12]),
                                                                   eccentricity = float(oneline[3]),
                                                                   period = float(oneline[14]) / orbit.SECOND_PER_DAY,
                                                                   inclination = float(oneline[5]),
                                                                   ascending_node = float(oneline[6]),
                                                                   argument_periapsis = float(oneline[7]),
                                                                   epoch = current_date,
                                                                   mean_anomaly = float(oneline[10])))

    main(datelist = datelist, sc_orb_elem_list = orbital_element_list,
         earth_orb_elem = planets_orbit_element_list[0],
         csv_filename = args.output_csv,
         png_filename = args.output_png,
         step = args.png_step)
            
