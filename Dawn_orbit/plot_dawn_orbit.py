"""
探査機Dawnの軌道図をプロットする
イオンエンジンによる連続的な軌道変更を可視化する練習
"""


import csv
import os
import sys
import datetime
import argparse
import numpy as np
import copy
import matplotlib.pyplot as plt


# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbit


# 各イベントの日時
mars_flyby_date = datetime.datetime(2009,2,17)
vesta_arrival_date = datetime.datetime(2011,7,16)
vesta_departure_date = datetime.datetime(2012,9,5)
ceres_arrival_date = datetime.datetime(2015,3,6)
mission_end_date = datetime.datetime(2016,1,1)
mars_approach_propulsion_start_date = datetime.datetime(2007,12,17)
mars_approach_propulsion_end_date = datetime.datetime(2008,10,31)
vesta_approach_propulsion_start_date = datetime.datetime(2009,6,1)
vesta_approach_propulsion_end_date = vesta_arrival_date
ceres_approach_propulsion_start_date = vesta_departure_date
ceres_approach_propulsion_end_date = ceres_arrival_date


# プロットに関わる設定
ORBIT_SHAPE_DIVIDE_NUM = 180    # 軌道形状を描画する際、軌道を何分割して描画するか
DELTA_V_SCALE = 2.5e10            # 増速量のベクトルをプロットする倍率
SUN_COLOR = "yellow"
DAWN_COLOR = "orange"
DAWN_COLOR_PALE = "#ffcf89"
DAWN_TRACK_COLOR = "blue"
DAWN_ARROW_COLOR = "red"
EARTH_COLOR = "blue"
EARTH_COLOR_PALE = "#8080ff"
MARS_COLOR = "#ff5050"
MARS_COLOR_PALE = "#ff8080"
VESTA_COLOR = "#00ff00"
VESTA_COLOR_PALE = "#80ff80"
CERES_COLOR = "#cc00cc"
CERES_COLOR_PALE = "#ff91ff"
PLOT_IMAGE_SAVE_PATH = ".\\plot_images\\"


def main(dawn_orbital_element_filename, planets_orbital_element_filename):
    # csvファイルから惑星・小惑星の軌道要素のデータを読み込む（地球、火星、セレス、ヴェスタの順）
    with open(planets_orbital_element_filename, "r") as planets_orbit_csv:
        reader = csv.reader(planets_orbit_csv)
        planets_orbital_element = []
        planets_orbital_shape = []
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for current_line_csv in reader:
            orbital_element_current_line = orbit.orbitalElement(semi_major_axis = float(current_line_csv[12]),
                                                                eccentricity = float(current_line_csv[3]),
                                                                period = float(current_line_csv[14]) / orbit.SECOND_PER_DAY,
                                                                inclination = float(current_line_csv[5]),
                                                                ascending_node = float(current_line_csv[6]),
                                                                argument_periapsis = float(current_line_csv[7]),
                                                                epoch = orbit.JulianDay(float(current_line_csv[1])).getDatetime(),
                                                                mean_anomaly = float(current_line_csv[10]))
            planets_orbital_element.append(copy.deepcopy(orbital_element_current_line))
            orbital_shape_current_line = orbital_element_current_line.get_orbital_shape(ORBIT_SHAPE_DIVIDE_NUM)
            planets_orbital_shape.append(copy.deepcopy(orbital_shape_current_line))
    
    # csvファイルからDawn探査機の軌道要素の時系列データを読み込む
    mission_date_list = []       # 位置・速度を計算する時刻のリスト
    with open(dawn_orbital_element_filename, "r") as dawn_orbit_csv:
        dawn_orbital_element = []
        reader = csv.reader(dawn_orbit_csv)
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for current_line_csv in reader:
            current_time = orbit.JulianDay(float(current_line_csv[0])).getDatetime() # 日時：ユリウス通日→datetimeオブジェクトに変換
            mission_date_list.append(current_time)
            if vesta_arrival_date < current_time and current_time < vesta_departure_date:
                dawn_orbital_element.append(copy.deepcopy(planets_orbital_element[3])) # ヴェスタ周回軌道滞在時は、ヴェスタの軌道要素を用いる
                continue
            if ceres_arrival_date < current_time and current_time < mission_end_date:
                dawn_orbital_element.append(copy.deepcopy(planets_orbital_element[2])) # セレス周回軌道滞在時は、セレスの軌道要素を用いる
                continue
            orbital_element_current_line = orbit.orbitalElement(semi_major_axis = float(current_line_csv[11]),
                                                                eccentricity = float(current_line_csv[2]),
                                                                period = float(current_line_csv[13]) / orbit.SECOND_PER_DAY,
                                                                inclination = float(current_line_csv[4]),
                                                                ascending_node = float(current_line_csv[5]),
                                                                argument_periapsis = float(current_line_csv[6]),
                                                                epoch = current_time,
                                                                mean_anomaly = float(current_line_csv[9]))
            dawn_orbital_element.append(copy.deepcopy(orbital_element_current_line))

    # 軌道要素から、位置・速度・増速量の時系列データを計算して行列に格納する
    dawn_pos_velocity = np.zeros((len(dawn_orbital_element), 9)) # 各時刻につき x, y, z, vx, vy, vz, dlt_vx, dlt_vy, dlt_vz の9要素を計算する
    planets_pos = np.zeros((dawn_pos_velocity.shape[0], 12))     # 地球、火星、セレス、ヴェスタそれぞれのx, y, z 計12要素
    for index in range(len(dawn_orbital_element)):
        # dawnの位置・速度計算・増速量
        pos, velocity = dawn_orbital_element[index].get_position_and_velocity(mission_date_list[index])
        if index == 0:
            deltaV = np.zeros((3,1))
        else:
            deltaV = velocity - dawn_orbital_element[index - 1].get_position_and_velocity(mission_date_list[index])[1]
        
        dawn_pos_velocity[index, 0:3] = pos.T
        dawn_pos_velocity[index, 3:6] = velocity.T
        dawn_pos_velocity[index, 6:9] = deltaV.T

        # 惑星・小惑星の位置計算
        for planet_index in range(len(planets_orbital_element)):
            pos = planets_orbital_element[planet_index].get_position(mission_date_list[index])
            planets_pos[index, (planet_index * 3) : ((planet_index + 1) * 3)] = pos.T
        
    # 計算結果をcsvファイルに書き出す
    with open("test_out.csv", "w") as output_file:
        output_file.write("time,dawn_x,dawn_y,dawn_z,dawn_vx,dawn_vy,dawn_vz,dawn_delta_x,dawn_delta_y,dawn_delta_z," +
                         "earth_x,earth_y,earth_z,mars_x,mars_y,mars_z,ceres_x,ceres_y,ceres_z,vesta_x,vesta_y,vesta_z\n")
        for line in range(dawn_pos_velocity.shape[0]):
            output_file.write("%s," % mission_date_list[line].strftime("%Y/%m/%d %H:%M:%S"))
            for column in range(dawn_pos_velocity.shape[1]):
                output_file.write("%10e," % dawn_pos_velocity[line][column])
            for column in range(planets_pos.shape[1]):
                output_file.write("%10e," % planets_pos[line][column])
            output_file.write("\n")


    # 計算結果をプロットしていく
    fig = plt.figure()
    if not os.path.exists(PLOT_IMAGE_SAVE_PATH):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(PLOT_IMAGE_SAVE_PATH)
    for index in range(0, len(mission_date_list), 1):
        # グラフの準備
        ax = fig.add_subplot(111)
        ax.axis([-5e8, 5e8, -5e8, 5e8])
        ax.set_aspect('equal')
        plt.xticks([-5e8, -2.5e8, 0, 2.5e8, 5e8])
        plt.yticks([-5e8, -2.5e8, 0, 2.5e8, 5e8])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # グラフの描画
        ax.plot(planets_orbital_shape[0][:, 0], planets_orbital_shape[0][:, 1], color = EARTH_COLOR_PALE, zorder = 1) # 地球軌道
        ax.plot(planets_orbital_shape[1][:, 0], planets_orbital_shape[1][:, 1], color = MARS_COLOR_PALE, zorder = 1) # 火星軌道
        ax.plot(planets_orbital_shape[2][:, 0], planets_orbital_shape[2][:, 1], color = CERES_COLOR_PALE, zorder = 1) # セレス軌道
        ax.plot(planets_orbital_shape[3][:, 0], planets_orbital_shape[3][:, 1], color = VESTA_COLOR_PALE, zorder = 1) # ヴェスタ軌道

        ax.plot(0, 0, color = 'yellow', marker = "o", markersize = 10, zorder = 5) # 太陽
        ax.plot(planets_pos[index, 0], planets_pos[index, 1], color = EARTH_COLOR, marker = "o", markersize = 8, zorder = 5) # 地球
        ax.plot(planets_pos[index, 3], planets_pos[index, 4], color = MARS_COLOR, marker = "o", markersize = 8, zorder = 5) # 火星
        ax.plot(planets_pos[index, 6], planets_pos[index, 7], color = CERES_COLOR, marker = "o", markersize = 8, zorder = 5) # セレス
        ax.plot(planets_pos[index, 9], planets_pos[index, 10], color = VESTA_COLOR, marker = "o", markersize = 8, zorder = 5) # ヴェスタ
        

        # 目標天体とDawnの会合予想時刻での予測位置を描画
        if mars_approach_propulsion_start_date < mission_date_list[index] and mission_date_list[index] < mars_flyby_date: # 火星アプローチ中
            mars_flyby_pos = planets_orbital_element[1].get_position(mars_flyby_date)
            dawn_flyby_pos = dawn_orbital_element[index].get_position(mars_flyby_date)
            ax.plot(mars_flyby_pos[0], mars_flyby_pos[1], marker = "o", markersize = 8,
                    markerfacecolor = MARS_COLOR_PALE, markeredgecolor = MARS_COLOR, zorder = 5)
            ax.plot(dawn_flyby_pos[0], dawn_flyby_pos[1], marker = "s", markersize = 8,
                    markerfacecolor = DAWN_COLOR_PALE, markeredgecolor = DAWN_COLOR, zorder = 5)
        if mars_flyby_date < mission_date_list[index] and mission_date_list[index] < vesta_arrival_date: # ヴェスタアプローチ中
            vesta_rendez_vous_pos = planets_orbital_element[3].get_position(vesta_arrival_date)
            dawn_rendez_vous_pos = dawn_orbital_element[index].get_position(vesta_arrival_date)
            ax.plot(vesta_rendez_vous_pos[0], vesta_rendez_vous_pos[1], marker = "o", markersize = 8,
                    markerfacecolor = VESTA_COLOR_PALE, markeredgecolor = VESTA_COLOR, zorder = 5)
            ax.plot(dawn_rendez_vous_pos[0], dawn_rendez_vous_pos[1], marker = "s", markersize = 8,
                    markerfacecolor = DAWN_COLOR_PALE, markeredgecolor = DAWN_COLOR, zorder = 5)
        if vesta_departure_date < mission_date_list[index] and mission_date_list[index] < ceres_arrival_date: # セレスアプローチ中
            ceres_rendez_vous_pos = planets_orbital_element[2].get_position(ceres_arrival_date)
            dawn_rendez_vous_pos = dawn_orbital_element[index].get_position(ceres_arrival_date)
            ax.plot(ceres_rendez_vous_pos[0], ceres_rendez_vous_pos[1], marker = "o", markersize = 8,
                    markerfacecolor = CERES_COLOR_PALE, markeredgecolor = CERES_COLOR, zorder = 5)
            ax.plot(dawn_rendez_vous_pos[0], dawn_rendez_vous_pos[1], marker = "s", markersize = 8,
                    markerfacecolor = DAWN_COLOR_PALE, markeredgecolor = DAWN_COLOR, zorder = 5)

        # Dawnの軌道(弾道軌道）を描画
        if mars_approach_propulsion_start_date < mission_date_list[index]:
            dawn_orbit_shape_current_time = dawn_orbital_element[index].get_orbital_shape(ORBIT_SHAPE_DIVIDE_NUM)
            ax.plot(dawn_orbit_shape_current_time[:, 0], dawn_orbit_shape_current_time[:, 1], color = DAWN_COLOR_PALE, zorder = 7)

        # Dawnの軌跡を描画
        ax.plot(dawn_pos_velocity[0:index, 0], dawn_pos_velocity[0:index, 1], color = DAWN_TRACK_COLOR, zorder = 7)

        # Dawnのイオンエンジンによる増速ベクトルを描画
        if ((mars_approach_propulsion_start_date < mission_date_list[index] and mission_date_list[index] < mars_approach_propulsion_end_date) or
            (vesta_approach_propulsion_start_date < mission_date_list[index] and mission_date_list[index] < vesta_approach_propulsion_end_date) or
            (ceres_approach_propulsion_start_date < mission_date_list[index] and mission_date_list[index] < ceres_approach_propulsion_end_date)):
            ax.arrow(dawn_pos_velocity[index, 0], dawn_pos_velocity[index, 1],
                     dawn_pos_velocity[index, 6] * DELTA_V_SCALE, dawn_pos_velocity[index, 7] * DELTA_V_SCALE,
                     head_width = 1e7, fc = DAWN_ARROW_COLOR , ec = DAWN_ARROW_COLOR, zorder = 15)

        # Dawnの位置を描画
        ax.plot(dawn_pos_velocity[index, 0], dawn_pos_velocity[index, 1], marker = "s", markersize = 8, color = DAWN_COLOR, zorder = 10) # Dawn

        # 日付を出力
        plt.text(2.5e8, 4e8, mission_date_list[index].strftime("%Y/%m/%d"))
        
        # グラフ出力
    
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("orbit_%05d.png" % index)
        fig.savefig(output_png_file_name, dpi = 100)

        # グラフを消去
        plt.clf()
    return

    
if __name__ == "__main__":
    # コマンドライン引数の解析
    parser = argparse.ArgumentParser()
    parser.add_argument("--Dawn_orbit",
                        action = "store",
                        default = "Dawn_orbital_elements.csv",
                        help = "File name of the orbital element of Dawn space craft (CSV format)",
                        type = str)
    parser.add_argument("--planets_orbit",
                        action = "store",
                        default = "planets_orbital_elements.csv",
                        help = "File name of the orbital element of the planets(Earth, Mars, Vesta, Ceres) (CSV format)",
                        type = str)
    args = parser.parse_args()         # 引数解析
    main(dawn_orbital_element_filename = args.Dawn_orbit,
         planets_orbital_element_filename = args.planets_orbit)
    
