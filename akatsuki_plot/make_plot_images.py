
"""
地球、金星、あかつきの位置をプロットする
"""

import datetime
import math
import numpy as np
import numpy.linalg
import scipy
import matplotlib.pyplot as plt
import matplotlib._png
import matplotlib.offsetbox
import sys, os

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbitalElement


# 惑星、探査機の軌道要素
venus_orbit= orbitalElement.orbitalElement(semi_major_axis    = 1.0894012e8,
                                           eccentricity       = 6.760221e-3,
                                           period             = 2.246997e2,
                                           inclination        = 3.39457,
                                           ascending_node     = 7.664348e1,
                                           argument_periapsis = 5.4915412e1,
                                           epoch              = datetime.datetime(2015,12,1,0,0,0),
                                           mean_anomaly       = 2.610122)
earth_orbit = orbitalElement.orbitalElement(semi_major_axis    = 1.495978e8,
                                            eccentricity       = 1.669898e-2,
                                            period             = 3.652561e2,
                                            inclination        = 2.125084e-3,
                                            ascending_node     = 1.744898e2,
                                            argument_periapsis = 2.884966e2,
                                            epoch              = datetime.datetime(2015,12,1,0,0,0),
                                            mean_anomaly       = 3.264275e2)
akatsuki_orbit_1 = orbitalElement.orbitalElement(semi_major_axis    = 1.33494e8, # 打ち上げ～金星会合(2010/5/21～2010/12/7)
                                                 eccentricity       = 1.94446e-1,
                                                 period             = 3.07895e2,
                                                 inclination        = 2.02598,
                                                 ascending_node     = 5.95877e1,
                                                 argument_periapsis = 3.84701e1,
                                                 epoch              = datetime.datetime(2010,9,1,0,0,0),
                                                 mean_anomaly       = 2.46525e2)
akatsuki_orbit_2 = orbitalElement.orbitalElement(semi_major_axis    = 1.01149e8, # 金星会合～軌道修正(2010/12/8～2011/11/6)
                                                 eccentricity       = 9.76441e-2,
                                                 period             = 2.03074e2,
                                                 inclination        = 3.51351,
                                                 ascending_node     = 7.74481e1,
                                                 argument_periapsis = 2.44859e2,
                                                 epoch              = datetime.datetime(2011,1,1,0,0,0),
                                                 mean_anomaly       = 1.71857e2)
akatsuki_orbit_3 = orbitalElement.orbitalElement(semi_major_axis    = 9.9751790e7, # 軌道修正～金星再会合(2011/11/6～2015/12/1)
                                                 eccentricity       = 8.500495e-2,
                                                 period             = 1.988575e2,
                                                 inclination        = 3.51351,
                                                 ascending_node     = 7.74481e1,
                                                 argument_periapsis = 2.44859e2,
                                                 epoch              = datetime.datetime(2011,11,6,0,0,0),
                                                 mean_anomaly       = 0)

# あかつきの軌道の変化する日時
launch_from_earth = datetime.datetime(2010,5,21) # 地球からの打ち上げ
venus_1st_approach = datetime.datetime(2010,12,7) # 金星との1回目の会合
orbital_correction = datetime.datetime(2011,11,6) # 軌道修正
venus_2nd_approach = datetime.datetime(2015,11,30) # 金星との2回目の会合

# 顔アイコン関連の設定
perigee_th = 9.24e7              # 太陽との距離がこの値以下なら「近日点付近」
venus_1st_approach_start = venus_1st_approach - datetime.timedelta(days = 30) # 金星会合直前
venus_1st_approach_end = venus_1st_approach + datetime.timedelta(days = 30) # 金星会合直後
orbital_correction_start = orbital_correction - datetime.timedelta(days = 20) # 軌道修正直前
orbital_correction_end = orbital_correction + datetime.timedelta(days = 20) # 軌道修正直後
venus_2nd_approach_start = venus_2nd_approach - datetime.timedelta(days = 30) # 金星再会合直前

# 地球、金星、あかつきの軌道の形状データを作成する
earth_orbit_shape = earth_orbit.get_orbital_shape(180)
venus_orbit_shape = venus_orbit.get_orbital_shape(180)
akatsuki_orbit_shape_1 = akatsuki_orbit_1.get_orbital_shape(180)
akatsuki_orbit_shape_2 = akatsuki_orbit_2.get_orbital_shape(180)
akatsuki_orbit_shape_3 = akatsuki_orbit_3.get_orbital_shape(180)

# 顔アイコンを読込む
icon_akatsuki_normal = matplotlib.offsetbox.OffsetImage(matplotlib._png.read_png(open("icon_image\\akatsuki-kun_normal_small.png")))
icon_akatsuki_damepo = matplotlib.offsetbox.OffsetImage(matplotlib._png.read_png(open("icon_image\\akatsuki-kun_damepo_small.png")))
icon_akatsuki_hot = matplotlib.offsetbox.OffsetImage(matplotlib._png.read_png(open("icon_image\\akatsuki-kun_hot_small.png")))
icon_akatsuki_kirixtu = matplotlib.offsetbox.OffsetImage(matplotlib._png.read_png(open("icon_image\\akatsuki-kun_kirixtu_small.png")))
icon_kinsei = matplotlib.offsetbox.OffsetImage(matplotlib._png.read_png(open("icon_image\kinsei-chan_small.png")))

def select_akatsuki_orbit(current_time):
    """
    日時により、あかつきの軌道要素と軌道の形状データを選択する
    """
    if current_time < launch_from_earth:
        return earth_orbit, earth_orbit_shape
    if current_time < venus_1st_approach:
        return akatsuki_orbit_1, akatsuki_orbit_shape_1
    if current_time < orbital_correction:
        return akatsuki_orbit_2, akatsuki_orbit_shape_2
    if current_time < venus_2nd_approach:
        return akatsuki_orbit_3, akatsuki_orbit_shape_3
    return venus_orbit

def select_akatsuki_face_icon(current_time, akatsuki_pos):
    """
    条件により、あかつきの顔アイコンを選択する
    """
    if current_time > venus_1st_approach_start and current_time <= venus_1st_approach:
        return icon_akatsuki_kirixtu
    if current_time > venus_1st_approach and current_time <= venus_1st_approach_end:
        return icon_akatsuki_damepo
    if current_time > orbital_correction_start and current_time < orbital_correction_end:
        return icon_akatsuki_kirixtu
    if current_time > venus_2nd_approach_start:
        return icon_akatsuki_kirixtu
    
    distance_from_sun = numpy.linalg.norm(akatsuki_pos)
    if distance_from_sun < perigee_th:
        return icon_akatsuki_hot

    return icon_akatsuki_normal

# プロットの準備
fig = plt.figure()

# 計算条件のパラメータ                     
delta_t = datetime.timedelta(days = 0.5) # 計算間隔

# 打ち上げから2回目の会合までの位置を逐次計算する
current_time = launch_from_earth
frame = 1

while current_time < venus_2nd_approach:
    # 軌道の形状と位置を算出
    current_time_str = current_time.strftime("%b %Y")
    current_time_str_for_filename = current_time.strftime("%Y%m%d")
    earth_pos = earth_orbit.get_position(current_time)
    venus_pos = venus_orbit.get_position(current_time)
    akatsuki_orbit, akatsuki_orbit_shape = select_akatsuki_orbit(current_time)
    akatsuki_pos = akatsuki_orbit.get_position(current_time)
    akatsuki_wo_correction_pos = akatsuki_orbit_2.get_position(current_time)
    
    # グラフの準備
    ax = fig.add_subplot(111)

    ax.axis([-2e8, 2e8, -2e8, 2e8])
    ax.set_aspect('equal')
    plt.xticks([-2e8, -1e8, 0, 1e8, 2e8])
    plt.yticks([-2e8, -1e8, 0, 1e8, 2e8])
    plt.xlabel("[km]")
    plt.ylabel("[km]")

    # グラフのプロット

    if current_time >= orbital_correction: # 軌道修正後には、軌道修正無しの場合の描画を行う
        ax.plot(akatsuki_orbit_shape_2[:, 0], akatsuki_orbit_shape_2[:, 1], color = "#a0a0a0", lw = 1)
    
    ax.plot(earth_orbit_shape[:, 0], earth_orbit_shape[:, 1], color = "#8080ff", lw = 2) # 地球軌道
    ax.plot(venus_orbit_shape[:, 0], venus_orbit_shape[:, 1], color = "#ffe17e", lw = 2) # 金星軌道
    ax.plot(akatsuki_orbit_shape[:, 0], akatsuki_orbit_shape[:, 1], color = "#80ff80", lw = 2) # あかつきの軌道

    if current_time >= orbital_correction:
        ax.plot(akatsuki_wo_correction_pos[0], akatsuki_wo_correction_pos[1], color = "#a0a0a0", marker = "s", markersize = 8)

    ax.plot(0, 0, color = 'yellow', marker = "o", markersize = 20) # 太陽
    ax.plot(earth_pos[0], earth_pos[1], color = "blue", marker = "o", markersize = 15) # 地球
    ax.plot(venus_pos[0], venus_pos[1], color = "orange", marker = "o", markersize = 15) # 金星
    ax.plot(akatsuki_pos[0], akatsuki_pos[1], color = "#00ff00", marker = "s", markersize = 8) # あかつき
    plt.text(1e8, 1.5e8, current_time_str)
    
    # 顔アイコンの出力
    offset_length = 50
    akatsuki_phase = math.atan2(akatsuki_pos[1], akatsuki_pos[0])
    venus_phase = math.atan2(venus_pos[1], venus_pos[0])
    
    annotation_box_akatsuki = matplotlib.offsetbox.AnnotationBbox(select_akatsuki_face_icon(current_time, akatsuki_pos),
                                                                  xy = (akatsuki_pos[0], akatsuki_pos[1]),
                                                                  xybox = (-offset_length * math.cos(akatsuki_phase),
                                                                           -offset_length * math.sin(akatsuki_phase)),
                                                                  boxcoords = "offset points",
                                                                  arrowprops = dict(arrowstyle = "->"))
    annotation_box_kinsei = matplotlib.offsetbox.AnnotationBbox(icon_kinsei,
                                                                xy = (venus_pos[0], venus_pos[1]),
                                                                xybox = (offset_length * math.cos(venus_phase),
                                                                         offset_length * math.sin(venus_phase)),
                                                                boxcoords = "offset points",
                                                                arrowprops = dict(arrowstyle = "->"))
    
    ax.add_artist(annotation_box_akatsuki)
    ax.add_artist(annotation_box_kinsei)
    
    # グラフ出力
    # output_png_file_name = "orbit_%05d_%s.png" % (frame, current_time_str_for_filename)
    output_png_file_name = "orbit_%05d.png" % frame

    fig.savefig(output_png_file_name, dpi = 100)

    # 後処理
    current_time += delta_t
    frame += 1
    plt.clf()

