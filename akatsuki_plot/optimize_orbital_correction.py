import datetime
import numpy as np
import numpy.linalg
import scipy.constants as const
import math
import copy
import sys, os

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import orbitalElement as orbitalElement


DELTA_V_MIN = 200               # 軌道修正の最適解の探索範囲(下限) [m/s]
DELTA_V_MAX = 350               # 軌道修正の最適解の探索範囲(上限) [m/s]
DELTA_V_STEP = 1                # 軌道修正の最適解探索の刻み幅 1[m/s]

VENUS_SECOND_APPROACH_START = datetime.datetime(2015,11,1,0,0,0)
VENUS_SECOND_APPROACH_END   = datetime.datetime(2015,12,31,0,0,0)


const_adv = const.physical_constants

G = const_adv['Newtonian constant of gravitation'][0] # 万有引力定数
Ms = 1.989e30                                        # 太陽の質量

venus_orbit= orbitalElement.orbitalElement(semi_major_axis    = 1.0894012e8, # 金星の軌道
                                           eccentricity       = 6.760221e-3,
                                           period             = 2.246997e2,
                                           inclination        = 3.39457,
                                           ascending_node     = 7.664348e1,
                                           argument_periapsis = 5.4915412e1,
                                           epoch              = datetime.datetime(2015,12,1,0,0,0),
                                           mean_anomaly       = 2.610122)
akatsuki_orbit_2 = orbitalElement.orbitalElement(semi_major_axis    = 1.01149e8, # 金星会合～軌道修正(2010/12/8～2011/11/6)
                                                 eccentricity       = 9.76441e-2,
                                                 period             = 2.03074e2,
                                                 inclination        = 3.51351,
                                                 ascending_node     = 7.74481e1,
                                                 argument_periapsis = 2.44859e2,
                                                 epoch              = datetime.datetime(2011,1,1,0,0,0),
                                                 mean_anomaly       = 1.71857e2)

def calc_oribital_element_from_perigee_status(r_perigee, v_perigee):
    """
    近日点通過速度、近日点距離から、軌道要素を計算する
    """
    r_perigee_SI = r_perigee * 1e3 # [km]→[m]
    v_perigee_SI = v_perigee * 1e3 # [km/s]→[m/s]
    r_times_v = r_perigee_SI * v_perigee_SI # 面積速度の積(一定値)[m^2/s]
    GM = G * Ms                                  # 地球質量 × 万有引力定数
    v_apogee_SI = (GM / r_times_v) - math.sqrt((GM ** 2 / r_times_v ** 2) + v_perigee_SI ** 2 - (2 * GM / r_perigee_SI))
    r_apogee_SI = r_times_v / v_apogee_SI

    semi_major_axis_SI = (r_perigee_SI + r_apogee_SI) / 2
    eccentricity = (r_apogee_SI - semi_major_axis_SI) / semi_major_axis_SI
    mean_anomaly_per_second = (1 + eccentricity) * v_apogee_SI / (semi_major_axis_SI * math.sqrt(1 - eccentricity ** 2))
    period_SI = 2 * math.pi / mean_anomaly_per_second

    orbital_element = {'semi_major_axis' : semi_major_axis_SI / 1e3,
                       'eccentricity' : eccentricity,
                       'period' : period_SI / orbitalElement.SECOND_PER_DAY}
    return orbital_element

original_v_perigee = akatsuki_orbit_2.v_perigee
original_r_perigee = akatsuki_orbit_2.r_perigee
original_v_apogee = akatsuki_orbit_2.v_apogee
original_r_apogee = akatsuki_orbit_2.r_apogee

result = []

outputFile = open("optimize_result.csv", "w")

for delta_v in range(DELTA_V_MIN, DELTA_V_MAX, DELTA_V_STEP): # 減速量を振る
    # 減速量から新しい軌道要素を算出
    new_v_perigee = original_v_perigee - delta_v / 1e3        # 軌道修正後の近日点通過速度[km/s]
    new_orbital_element = calc_oribital_element_from_perigee_status(r_perigee = original_r_perigee, v_perigee = new_v_perigee)
    akatsuki_orbit_3 = copy.deepcopy(akatsuki_orbit_2)
    akatsuki_orbit_3.epoch = datetime.datetime(2011,11,6,0,0,0) # 近日点通過日時 = 軌道修正日時
    akatsuki_orbit_3.mean_anomaly_epoch = 0
    akatsuki_orbit_3.eccentricity = new_orbital_element['eccentricity']
    akatsuki_orbit_3.semi_major_axis = new_orbital_element['semi_major_axis']
    akatsuki_orbit_3.period = new_orbital_element['period']

    akatsuki_orbit_3._update_valuables()
    

    # 最接近する日時と距離を求める
    current_date = VENUS_SECOND_APPROACH_START
    minimum_distance = 1.5e8      # 最接近距離の初期値(約1AU)
    approach_date_str = ""
    while current_date < VENUS_SECOND_APPROACH_END:
        venus_pos = venus_orbit.get_position(current_date)
        akatsuki_pos = akatsuki_orbit_3.get_position(current_date)
        distance = math.sqrt((akatsuki_pos[0] - venus_pos[0])**2 + (akatsuki_pos[1] - venus_pos[1]) ** 2 + (akatsuki_pos[2] - venus_pos[2]) ** 2)
        current_date += datetime.timedelta(days = 1.0)
        if distance < minimum_distance:
            minimum_distance = distance
            approach_date_str = current_date.strftime("%Y%m%d")
    result.append([delta_v, minimum_distance, approach_date_str])

    outputFile.write("%e, %e, %e, %e, %e, %s," % (delta_v, akatsuki_orbit_3.eccentricity,
                                           akatsuki_orbit_3.semi_major_axis, akatsuki_orbit_3.period,
                                            minimum_distance, approach_date_str))

    outputFile.write("%e, %e, %e\n" % (akatsuki_orbit_3.eccentricity, akatsuki_orbit_3.semi_major_axis, akatsuki_orbit_3.period))
    
outputFile.close()

