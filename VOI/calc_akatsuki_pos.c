/* 太陽、金星、地球、あかつきの位置・速度の時系列データを
   spice kernalから抽出し出力する */

#include <stdio.h>
#include <stdlib.h>
#include <cspice/SpiceUsr.h>

#define KERNEL_LIST_FILENAME "kernel_list.txt" /* 読み込むkernelのファイル名のリスト */
#define START_UTC_STR "2015-12-01T00:00:00"    /* 計算開始日時 */
#define END_UTC_STR "2016-01-01T00:00:00"      /* 計算終了日時 */
#define STEP_SEC_PLANET 3600                          /* 太陽、惑星の位置の計算間隔[sec] */
#define STEP_SEC_SC 5                                 /* あかつきの位置・速度の計算間隔[sec] */
#define OUTPUT_FILE_NAME_SP "sun_planets_pos.csv" /* 太陽・惑星の計算結果の出力先ファイル名 */
#define OUTPUT_FILE_NAME_SC "sc_pv.csv"           /* あかつきの位置・速度の出力先ファイル名 */
#define FRAME "ECLIPJ2000"                  /* 計算する座標系(黄道座標系) */
#define REF_POINT "SUN"                     /* 基準点(太陽) */
#define ORBIT_SHAPE_START_UTC_STR "2014-12-7T00:00:00" /* 軌道の形状作成用のデータの開始日時 */
#define ORBIT_SHAPE_START_UTC_STR_AKATSUKI "2015-7-2T00:00:00" /* 軌道の形状作成用のデータの開始日時(あかつきのみ) */
#define ORBIT_SHAPE_END_UTC_STR "2015-12-7T00:00:00" /* 軌道の形状作成用のデータの終了日時 */
#define ORBIT_SHAPE_STEP_SEC 86400                    /* 軌道の形状作成用データの計算間隔 [sec] */
#define OUTPUT_FILE_NAME_ORBTIT_SHAPE "orbit_shape.csv" /* 軌道の形状データの出力ファイル名 */

int main(void)
{
    SpiceDouble et, et_start, et_end, et_akatsuki_start;             /* 時刻[sec] */
    SpiceChar utc_str[80];     /* UTC時刻文字列を格納する */
    SpiceDouble pv[6];          /* 位置、速度を格納する配列 */
    SpiceDouble lt;             /* 光秒 */
    FILE *output_file;          /* 出力先ファイル */

    /* カーネルの読み込み */
    furnsh_c(KERNEL_LIST_FILENAME);

    /* 計算開始、終了時刻の取得 */
    utc2et_c(START_UTC_STR, &et_start);
    utc2et_c(END_UTC_STR, &et_end);

    /* 太陽・惑星の位置の出力 */
    output_file = fopen(OUTPUT_FILE_NAME_SP, "w");
        
    for(et = et_start; et <= et_end; et += STEP_SEC_PLANET){
        int column;
        et2utc_c(et, "ISOC", 0, 79, utc_str); /* 現在時刻の文字列を作成 */
        fprintf(output_file, "%s", utc_str);  /* 現在時刻を出力 */
        spkezr_c("SUN", et, FRAME, "NONE", REF_POINT, pv, &lt); /* 太陽の位置の計算と出力 */
        for(column = 0; column < 3; column++){
            fprintf(output_file, ",%.12e", pv[column]);
        }
        spkezr_c("VENUS", et, FRAME, "NONE", REF_POINT, pv, &lt); /* 金星の位置の計算と出力 */
        for(column = 0; column < 3; column++){
            fprintf(output_file, ",%.12e", pv[column]);
        }
        spkezr_c("EARTH", et, FRAME, "NONE", REF_POINT, pv, &lt); /* 地球の位置の計算と出力 */
        for(column = 0; column < 3; column++){
            fprintf(output_file, ",%.12e", pv[column]);
        }
        fprintf(output_file, "\n");
    }
    fclose(output_file);

    /* あかつきの位置・速度の出力 */
    output_file = fopen(OUTPUT_FILE_NAME_SC, "w");
    for(et = et_start; et <= et_end; et += STEP_SEC_SC){
        int column;
        spkezr_c("AKATSUKI", et, FRAME, "NONE", REF_POINT, pv, &lt); /* あかつきの位置・速度の計算と出力 */
        et2utc_c(et, "ISOC", 0, 79, utc_str); /* 現在時刻の文字列を作成 */
        fprintf(output_file, "%s", utc_str);  /* 現在時刻を出力 */
        for(column = 0; column < 6; column++){
            fprintf(output_file, ",%.12e", pv[column]);
        }
        fprintf(output_file, "\n");
    }

    fclose(output_file);

    /* 軌道の形状データの出力 */
    output_file = fopen(OUTPUT_FILE_NAME_ORBTIT_SHAPE, "w");
    utc2et_c(ORBIT_SHAPE_START_UTC_STR, &et_start);
    utc2et_c(ORBIT_SHAPE_END_UTC_STR, &et_end);
    utc2et_c(ORBIT_SHAPE_START_UTC_STR_AKATSUKI, &et_akatsuki_start);
    
    for(et = et_start; et <= et_end; et += ORBIT_SHAPE_STEP_SEC){
        et2utc_c(et, "ISOC", 0, 79, utc_str); /* 現在時刻の文字列を作成 */
        fprintf(output_file, "%s", utc_str);  /* 現在時刻を出力 */
        int column;
        spkezr_c("VENUS", et, FRAME, "NONE", REF_POINT, pv, &lt); /* 金星の位置の計算と出力 */
        for(column = 0; column < 3; column++){
            fprintf(output_file, ",%.12e", pv[column]);
        }
        spkezr_c("EARTH", et, FRAME, "NONE", REF_POINT, pv, &lt); /* 地球の位置の計算と出力 */
        for(column = 0; column < 3; column++) {
            fprintf(output_file, ",%.12e", pv[column]);
        }

        if(et > et_akatsuki_start){
            spkezr_c("AKATSUKI", et, FRAME, "NONE", REF_POINT, pv, &lt); /* あかつきの位置の計算と出力 */
            for(column = 0; column < 3; column++) {
                fprintf(output_file, ",%.12e", pv[column]);
            }
        }else{
            fprintf(output_file, ",,,");
        }
        fprintf(output_file, "\n");
    }

    fclose(output_file);
                    
    return 0;
}

