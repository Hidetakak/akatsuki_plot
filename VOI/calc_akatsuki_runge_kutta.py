# -*- coding:utf-8 -*-
"""
あかつきの軌道予測をルンゲクッタ法を用いて計算する
"""

import csv
import datetime
import os

import numpy as np
from numpy.linalg import norm
from scipy.integrate import ode
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt


SP_POS_FILE_NAME = "sun_planets_pos.csv" # 惑星・あかつきの位置を記したファイル名
SC_PV_FILE_NAME = "sc_pv.csv"            # あかつきの位置・速度を記したファイル名
GM_FILE_NAME = "GM_list.csv"      # 太陽、惑星のGM値のファイル名
DT_DELTA_V_STEP = 1                       # 増速量の計算間隔[index](5秒相当)
DT_PREDICT_STEP = 60                      # この先の予測進路の計算間隔[index]
DT_LONG_PLOT_STEP = 180                   # 長期間のプロットの間隔[index]
OUT_FILE_NAME = "rk4_predict2.csv"
DV_START_DATE = datetime.datetime(2015,12,6,23,0,0) # 増速量の計算開始日時
# DV_START_DATE = datetime.datetime(2015,12,6,23,45,0) # 増速量の計算開始日時
DV_END_DATE = datetime.datetime(2015,12,7,1,0,0)    # 増速量の計算終了日時
VENUS_PASS_DATE = datetime.datetime(2015,12,7,0,0,0) # 金星最接近日時
DV_TH = 0.1                                         # 増速量の有無の判定閾値[m/s]

PLOT_IMAGE_SAVE_PATH = ".\\plot_images\\"
LARGE_PLOT_FILENAME = "venus_fix_large"
SMALL_PLOT_FILENAME = "venus_fix_small"
LARGE_LONG_PLOT_FILENAME = "venus_fix_large_long"
VENUS_FACE_COLOR = "#ffcc66"
AKATSUKI_FACE_COLOR = "#00ff00"
AKATSUKI_EDGE_COLOR = "black"
AKATSUKI_WO_DV_FACE_COLOR = "#c0c0c0"
AKATSUKI_WO_DV_EDGE_COLOR = "#404040"
AKATSUKI_TRACK_COLOR = "#00ff00"
AKATSUKI_PREDICT_TRACK_COLOR = "#cc66ff"
AKATSUKI_WO_DV_TRACK_COLOR = "#404040"
AKATSUKI_TRACK_DV_COLOR = "#ff0000"
AKATSUKI_ACC_ARROW_COLOR = "#ff0000"
ACC_SCALE = 2.5e4               # 1[m/s2]をxx[km]の長さとしてプロットする
ARROW_LENGTH_SMALL_PLOT = 0.8e4
ARROW_LENGTH_LARGE_PLOT = 1.5e5
ARROW_WIDTH_SMALL_PLOT = ARROW_LENGTH_SMALL_PLOT * 0.15
ARROW_WIDTH_LARGE_PLOT = ARROW_LENGTH_LARGE_PLOT * 0.15
ARROW_HEAD_WIDTH_SMALL_PLOT = ARROW_WIDTH_SMALL_PLOT * 2.5
ARROW_HEAD_WIDTH_LARGE_PLOT = ARROW_WIDTH_LARGE_PLOT * 2.5
ARROW_START_POS_SMALL_PLOT = 1.2e4
ARROW_START_POS_LARGE_PLOT = 1.5e5
SUN_DIRECTION_EDGE_COLOR = "#996633"
SUN_DIRECTION_FACE_COLOR = "#ffff00"
EARTH_DIRECTION_EDGE_COLOR = "#0000ff"
EARTH_DIRECTION_FACE_COLOR = "#6666ff"


def ISOFormat2Datetime(str_iso):
    """
    ISOフォーマットの文字列からdatetimeオブジェクトを返す
    """
    dt = datetime.datetime.strptime(str_iso, "%Y-%m-%dT%H:%M:%S")
    return dt


def readTimeLineDataFormCSV(filename):
    """
    csvファイルから時系列データを読み込む
    時刻データ：csvファイルではutc-iso形式→timestamp形式で格納する
    """
    with open(filename, "r") as input_file:
        csv_reader = csv.reader(input_file)
        time_list = []
        data_list = []
        for one_line_list in csv_reader:
            time_list.append(ISOFormat2Datetime(one_line_list[0]).timestamp())
            data_list.append(list(map(float, one_line_list[1:])))
        time_array = np.array(time_list)
        data_array = np.array(data_list)
        return time_array, data_array


def dpvdt(t, pv, planet_pos_f, GM):
    """
    位置、速度の微分値を返す
    t : 時刻[sec]
    pv : 位置(3)速度(3)の6要素のベクトル[km][km/sec]
    planet_pos_f: 太陽、各惑星の位置(3 x n)[km]を求める関数
    GM : 太陽、各惑星のGM値(n)
    """
    # print("t = {}, pv = {}".format(t, pv))
    n_object = GM.shape[0]      # 重力源の数
    ac = np.zeros(3)            # 重力源による加速度
    current_planet_pos = planet_pos_f(t) # 現在の重力源の位置
    for object in range(n_object): # 重力源ごとに影響を計算する
        object_pos = current_planet_pos[3 * object: 3 * object + 3] # 重力源の位置
        # print("  objet_pos[{}] = {}".format(object, object_pos))
        vec_sc2ob = object_pos - pv[0:3]                        # 宇宙機から重力源へのベクトル
        # print("  vec = {}".format(vec_sc2ob))
        distance = norm(vec_sc2ob)
        # print("  distance = {}".format(distance)) # 宇宙機から重力源への距離
        ac += GM[object] / distance ** 3 * vec_sc2ob            # 重力源による加速度[km/sec^2]
    result = np.zeros(6)                                        # 微分結果の格納用
    result[0:3] = pv[3:6]
    result[3:6] = ac
    return result

if __name__ == "__main__":
    # ファイルから必要な情報を読み込む
    print("csvファイルから軌道情報を読み込み中 ... ", end = "", flush = True)
    sp_timeline, sp_pos = readTimeLineDataFormCSV(SP_POS_FILE_NAME) # 太陽、惑星の位置データを読み込む
    sc_timeline, sc_pv = readTimeLineDataFormCSV(SC_PV_FILE_NAME)   # あかつきの位置・速度データを読み込む
    GM = np.loadtxt(GM_FILE_NAME)                                   # 太陽、惑星のGM値を読み込む
    print("完了")

    print("太陽、惑星の位置の補間式を作成中 ... ", end = "", flush = True)
    sp_pos_f = interp1d(sp_timeline, sp_pos.T, kind = "cubic") # 太陽、惑星の位置のスプライン補間関数を作成
    print("完了")
    sp_pos_full = sp_pos_f(sc_timeline).T # 太陽、惑星の位置の一覧(あかつきの位置データと時系列をそろえた)
    sc_pos_from_venus = sc_pv[:, 0:3] - sp_pos_full[:, 3:6] # 金星基準のあかつきの位置
    
    # 増速量計算の計算範囲（あかつきの時系列データのインデックス範囲）、計算間隔
    dv_calc_start_index = np.searchsorted(sc_timeline, DV_START_DATE.timestamp()) # 計算開始index
    dv_calc_end_index = np.searchsorted(sc_timeline, DV_END_DATE.timestamp()) # 計算終了index
    dv_step_sec = DT_DELTA_V_STEP * (sc_timeline[1] - sc_timeline[0]) # 計算間隔[sec]
    
    # 軌道投入噴射なしの場合の計算用のODEクラスを作る
    ode_rk_wo_dv = ode(dpvdt)
    ode_rk_wo_dv.set_integrator("dopri5")
    # 軌道投入噴射なしの場合の位置のデータを作る（描画用に粗く)
    sc_timeline_rough = sc_timeline[::DT_PREDICT_STEP]
    sc_pos_wo_dv_rough = sc_pv[::DT_PREDICT_STEP, 0:3].copy()
    ode_rk_wo_dv.set_initial_value(y = sc_pv[0], t = sc_timeline_rough[0])
    ode_rk_wo_dv.set_f_params(sp_pos_f, GM)
    for index in range(1, sc_timeline_rough.shape[0]):
        ode_rk_wo_dv.integrate(sc_timeline_rough[index])
        sc_pos_wo_dv_rough[index, :] = ode_rk_wo_dv.y[0:3]
    sc_pos_wo_dv_rough_from_venus = sc_pos_wo_dv_rough - sp_pos_full[::DT_PREDICT_STEP, 3:6]

    # np.savetxt("wo_dv.csv", sc_pos_wo_dv_rough, delimiter = ",")
    # np.savetxt("wo_dv2.csv", sc_pos_wo_dv_rough_from_venus, delimiter = ",")
    # sc_pos_rough = sc_pv[::DT_PREDICT_STEP, 0:3]
    # sc_pos_rough_from_venus = sc_pos_rough - sp_pos_f(sc_timeline_rough).T[:, 3:6]
    # np.savetxt("with_dv.csv", sc_pos_rough, delimiter = ",")
    # np.savetxt("with_dv2.csv", sc_pos_rough_from_venus, delimiter = ",")
    # sc_pos_from_venus = sc_pv[:,0:3] - sp_pos_f(sc_timeline).T[:, 3:6]
    # np.savetxt("with_dv3.csv", sc_pv, delimiter = ",")
    # np.savetxt("with_dv4.csv", sc_pos_from_venus, delimiter = ",")
                       
    # 軌道投入噴射なしの場合の計算ODEの初期条件を再設定
    ode_rk_wo_dv.set_initial_value(y = sc_pv[dv_calc_start_index], t = sc_timeline[dv_calc_start_index])

    fig = plt.figure()
    if not os.path.exists(PLOT_IMAGE_SAVE_PATH):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(PLOT_IMAGE_SAVE_PATH)

    venus_pass_index = np.searchsorted(sc_timeline, VENUS_PASS_DATE.timestamp()) # 金星通過のindex
    frame = 1
    current_index_rough = 0
    
    for current_index in range(0,sc_timeline.shape[0],(DT_LONG_PLOT_STEP)):
        # 描画の準備（縮小図）
        ax = fig.add_subplot(111)
        ax.axis([-0.5e6, 0.5e6, -0.5e6, 0.5e6])
        ax.set_aspect('equal')
        plt.xticks([-0.5e6, -0.25e6, 0, 0.25e6, 0.5e6])
        plt.yticks([-0.5e6, -0.25e6, 0, 0.25e6, 0.5e6])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # 地球、太陽の方向の計算
        sun_direction_vec = sp_pos_full[current_index, 0:3] - sp_pos_full[current_index, 3:6]
        earth_direction_vec = sp_pos_full[current_index, 6:9] - sp_pos_full[current_index, 3:6]
        sun_direction_vec /= norm(sun_direction_vec)
        earth_direction_vec /= norm(earth_direction_vec)
        
        # グラフの描画
        ax.plot(0, 0, marker = "o", markersize = 8,
                markerfacecolor = VENUS_FACE_COLOR, markeredgecolor = VENUS_FACE_COLOR, zorder = 1)
        sc_wo_dv_from_venus = ode_rk_wo_dv.y[0:3] - sp_pos_full[current_index, 3:6] # 金星基準のあかつきの位置（軌道修正噴射をしない場合)
        ax.plot(sc_pos_from_venus[current_index, 0],
                sc_pos_from_venus[current_index, 1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_FACE_COLOR,
                markeredgecolor = AKATSUKI_EDGE_COLOR, zorder = 11) # あかつきの現在位置
        ax.plot(sc_pos_wo_dv_rough_from_venus[current_index_rough, 0],
                sc_pos_wo_dv_rough_from_venus[current_index_rough, 1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_WO_DV_FACE_COLOR,
                markeredgecolor = AKATSUKI_WO_DV_EDGE_COLOR, zorder = 10) # あかつき（軌道修正噴射しない場合)
        current_index_rough += int(DT_LONG_PLOT_STEP / DT_PREDICT_STEP)
        ax.plot(sc_pos_wo_dv_rough_from_venus[:, 0],
                sc_pos_wo_dv_rough_from_venus[:, 1],
                color = AKATSUKI_WO_DV_TRACK_COLOR, zorder = 1) # 軌道修正噴射しない場合の軌道予測
        if current_index < venus_pass_index:
            ax.plot(sc_pos_wo_dv_rough_from_venus[:, 0],
                    sc_pos_wo_dv_rough_from_venus[:, 1],
                    color = AKATSUKI_PREDICT_TRACK_COLOR, zorder = 2) # 金星通過前は軌道修正噴射しない場合の軌道予測
        else:
            ax.plot(sc_pos_from_venus[:, 0],
                    sc_pos_from_venus[:, 1],
                    color = AKATSUKI_PREDICT_TRACK_COLOR, zorder = 2) # 金星通過後は軌道修正噴射した場合の軌道予測
                    
        ax.plot(sc_pos_from_venus[:current_index, 0],
                sc_pos_from_venus[:current_index, 1],
                color = AKATSUKI_TRACK_COLOR, zorder = 2, linewidth = 2) # あかつきのこれまでの軌跡
        ax.arrow(sun_direction_vec[0] * ARROW_START_POS_LARGE_PLOT,
                 sun_direction_vec[1] * ARROW_START_POS_LARGE_PLOT,
                 sun_direction_vec[0] * ARROW_LENGTH_LARGE_PLOT,
                 sun_direction_vec[1] * ARROW_LENGTH_LARGE_PLOT,
                 width = ARROW_WIDTH_LARGE_PLOT,
                 head_width = ARROW_HEAD_WIDTH_LARGE_PLOT,
                 fc = SUN_DIRECTION_FACE_COLOR, ec = SUN_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 太陽の方向の矢印
        ax.arrow(earth_direction_vec[0] * ARROW_START_POS_LARGE_PLOT,
                 earth_direction_vec[1] * ARROW_START_POS_LARGE_PLOT,
                 earth_direction_vec[0] * ARROW_LENGTH_LARGE_PLOT,
                 earth_direction_vec[1] * ARROW_LENGTH_LARGE_PLOT,
                 width = ARROW_WIDTH_LARGE_PLOT,
                 head_width = ARROW_HEAD_WIDTH_LARGE_PLOT,
                 fc = EARTH_DIRECTION_FACE_COLOR, ec = EARTH_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 太陽の方向の矢印

        # テキストを出力
        current_time = sc_timeline[current_index]
        time_str_utc = datetime.datetime.fromtimestamp(current_time).strftime("%Y/%m/%d %H:%M:%S UTC")
        time_str_jst = (datetime.datetime.fromtimestamp(current_time) + datetime.timedelta(hours = 9)).strftime("%Y/%m/%d %H:%M:%S JST")
        plt.text(-0.4e6, 4e5, "{}".format(time_str_utc), fontsize = 18)
        plt.text(-0.4e6, 3.25e5, "{}".format(time_str_jst), fontsize = 18)
        # plt.text(-0.4e9, 2.5e8, "Delta V = {:3.1f} [m/s]".format(dv_total), fontsize = 18)
        
        # plt.text(0, 1.8e8, "Delta V = {:3.1f} [m/s]".format(dv_total))
        # グラフ出力
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("{}_{:05d}.png".format(LARGE_LONG_PLOT_FILENAME, frame))
        frame += 1
        fig.savefig(output_png_file_name, dpi = 100)
        plt.clf()
                       



        
    current_index = dv_calc_start_index
    frame = 1
    dv_total = 0
    dv_start_index = dv_calc_end_index
    dv_end_index = dv_calc_start_index

    while current_index < dv_calc_end_index:
        current_time = sc_timeline[current_index]
        # ルンゲクッタ法による積分計算クラスを作る
        ode_rk = ode(dpvdt)
        ode_rk.set_integrator("dopri5") # Runge-Kutta4(5)で計算を行う
        ode_rk.set_initial_value(y = sc_pv[current_index], t = current_time)
        ode_rk.set_f_params(sp_pos_f, GM)

        # ルンゲクッタ法により次のステップの値を求める
        ode_rk.integrate(ode_rk.t + dv_step_sec)
        current_index += DT_DELTA_V_STEP
        current_time += dv_step_sec
        # current_sc_pv = sc_pv[current_index]
        # current_planet_pos = sp_pos_full[current_index]
        dv_vec = sc_pv[current_index, 3:6] - ode_rk.y[3:6]
        dv = norm(dv_vec) * 1000 # 増速量を求める[km/s]→[m/s]に変換する
        acc_vec = dv_vec * 1000 / dv_step_sec # 加速度ベクトル[m/s2]
        dv_total += dv
        if dv > DV_TH:          # 軌道修正噴射の開始、終了時刻を更新する
            if current_index > dv_end_index:
                dv_end_index = current_index
            if current_index < dv_start_index:
                dv_start_index = current_index

        # ルンゲクッタ法により、軌道修正噴射をしなかった場合の位置、速度を求める
        ode_rk_wo_dv.integrate(current_time)

        # 予測進路を作成する
        predict_size = len(range(current_index, sc_timeline.shape[0], DT_PREDICT_STEP))
        predict_pv = np.zeros((predict_size, 3))
        predict_pv[0, :] = sc_pv[current_index, 0:3] - sp_pos_full[current_index][3:6]
        ode_rk.set_initial_value(y = sc_pv[current_index], t = current_time)
        predict_current_index = 1
        for predict_index in range(current_index + DT_PREDICT_STEP, sc_timeline.shape[0], DT_PREDICT_STEP):
            ode_rk.integrate(sc_timeline[predict_index])
            predict_pv[predict_current_index, :] = ode_rk.y[0:3] - sp_pos_full[predict_index, 3:6]
            predict_current_index += 1
        # 地球、太陽の方向を計算する
        sun_direction_vec = sp_pos_full[current_index, 0:3] - sp_pos_full[current_index, 3:6]
        earth_direction_vec = sp_pos_full[current_index, 6:9] - sp_pos_full[current_index, 3:6]
        sun_direction_vec /= norm(sun_direction_vec)
        earth_direction_vec /= norm(earth_direction_vec)
        
        # 描画の準備（拡大図）
        ax = fig.add_subplot(111)
        ax.axis([-2.5e4, 2.5e4, -2.5e4, 2.5e4])
        ax.set_aspect('equal')
        plt.xticks([-2e4, -1e4, 0, 1e4, 2e4])
        plt.yticks([-2e4, -1e4, 0, 1e4, 2e4])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # グラフの描画
        venus_plot = plt.Circle((0, 0), 6e3, color = VENUS_FACE_COLOR) # 金星を半径6e3[km] = 6000[km]の円で描く
        ax.add_patch(venus_plot)
        sc_wo_dv_from_venus = ode_rk_wo_dv.y[0:3] - sp_pos_full[current_index, 3:6] # 金星基準のあかつきの位置（軌道修正噴射をしない場合)
        ax.plot(sc_pos_from_venus[current_index, 0],
                sc_pos_from_venus[current_index, 1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_FACE_COLOR,
                markeredgecolor = AKATSUKI_EDGE_COLOR, zorder = 11) # あかつきの現在位置
        ax.plot(sc_wo_dv_from_venus[0],
                sc_wo_dv_from_venus[1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_WO_DV_FACE_COLOR,
                markeredgecolor = AKATSUKI_WO_DV_EDGE_COLOR, zorder = 10) # あかつき（軌道修正噴射しない場合)
        ax.plot(sc_pos_wo_dv_rough_from_venus[:, 0],
                sc_pos_wo_dv_rough_from_venus[:, 1],
                color = AKATSUKI_WO_DV_TRACK_COLOR, zorder = 1) # 軌道修正噴射しない場合の軌道予測
        ax.plot(predict_pv[:, 0], predict_pv[:, 1],
                color = AKATSUKI_PREDICT_TRACK_COLOR) # 今後の予測進路
        ax.plot(sc_pos_from_venus[:current_index, 0],
                sc_pos_from_venus[:current_index, 1],
                color = AKATSUKI_TRACK_COLOR, zorder = 2, linewidth = 2) # あかつきのこれまでの軌跡
        if dv_total > DV_TH:                  # 軌道修正噴射をしている期間の軌跡
            ax.plot(sc_pos_from_venus[dv_start_index:dv_end_index, 0],
                    sc_pos_from_venus[dv_start_index:dv_end_index, 1],
                    color = AKATSUKI_TRACK_DV_COLOR, zorder = 3, linewidth = 2)
        if dv > DV_TH:
            ax.arrow(sc_pos_from_venus[current_index, 0],
                     sc_pos_from_venus[current_index, 1],
                     acc_vec[0] * ACC_SCALE, acc_vec[1] *ACC_SCALE,
                     zorder = 12, fc = AKATSUKI_ACC_ARROW_COLOR, ec = AKATSUKI_ACC_ARROW_COLOR,
                     head_width = 1e3)
        ax.arrow(sun_direction_vec[0] * ARROW_START_POS_SMALL_PLOT,
                 sun_direction_vec[1] * ARROW_START_POS_SMALL_PLOT,
                 sun_direction_vec[0] * ARROW_LENGTH_SMALL_PLOT,
                 sun_direction_vec[1] * ARROW_LENGTH_SMALL_PLOT,
                 width = ARROW_WIDTH_SMALL_PLOT,
                 head_width = ARROW_HEAD_WIDTH_SMALL_PLOT,
                 fc = SUN_DIRECTION_FACE_COLOR, ec = SUN_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 太陽の方向の矢印
        ax.arrow(earth_direction_vec[0] * ARROW_START_POS_SMALL_PLOT,
                 earth_direction_vec[1] * ARROW_START_POS_SMALL_PLOT,
                 earth_direction_vec[0] * ARROW_LENGTH_SMALL_PLOT,
                 earth_direction_vec[1] * ARROW_LENGTH_SMALL_PLOT,
                 width = ARROW_WIDTH_SMALL_PLOT,
                 head_width = ARROW_HEAD_WIDTH_SMALL_PLOT,
                 fc = EARTH_DIRECTION_FACE_COLOR, ec = EARTH_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 地球の方向の矢印

        # テキストを出力
        time_str_utc = datetime.datetime.fromtimestamp(current_time).strftime("%Y/%m/%d %H:%M:%S UTC")
        time_str_jst = (datetime.datetime.fromtimestamp(current_time) + datetime.timedelta(hours = 9)).strftime("%Y/%m/%d %H:%M:%S JST")
        plt.text(-1.5e4, 2.2e4, "{}".format(time_str_utc), fontsize = 18)
        plt.text(-1.5e4, 1.9e4, "{}".format(time_str_jst), fontsize = 18)
        plt.text(-1.5e4, 1.6e4, "Delta V = {:3.1f} [m/s]".format(dv_total), fontsize = 18)
        # グラフ出力
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("{}_{:05d}.png".format(SMALL_PLOT_FILENAME, frame))
        fig.savefig(output_png_file_name, dpi = 100)

        # グラフを消去
        plt.clf()

        # 描画の準備（縮小図）
        ax = fig.add_subplot(111)
        ax.axis([-0.5e6, 0.5e6, -0.5e6, 0.5e6])
        ax.set_aspect('equal')
        plt.xticks([-0.5e6, -0.25e6, 0, 0.25e6, 0.5e6])
        plt.yticks([-0.5e6, -0.25e6, 0, 0.25e6, 0.5e6])
        plt.xlabel("[km]")
        plt.ylabel("[km]")
        # グラフの描画
        ax.plot(0, 0, marker = "o", markersize = 8,
                markerfacecolor = VENUS_FACE_COLOR, markeredgecolor = VENUS_FACE_COLOR, zorder = 1)
        sc_wo_dv_from_venus = ode_rk_wo_dv.y[0:3] - sp_pos_full[current_index, 3:6] # 金星基準のあかつきの位置（軌道修正噴射をしない場合)
        ax.plot(sc_pos_from_venus[current_index, 0],
                sc_pos_from_venus[current_index, 1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_FACE_COLOR,
                markeredgecolor = AKATSUKI_EDGE_COLOR, zorder = 11) # あかつきの現在位置
        ax.plot(sc_wo_dv_from_venus[0],
                sc_wo_dv_from_venus[1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_WO_DV_FACE_COLOR,
                markeredgecolor = AKATSUKI_WO_DV_EDGE_COLOR, zorder = 10) # あかつき（軌道修正噴射しない場合)
        ax.plot(sc_pos_wo_dv_rough_from_venus[:, 0],
                sc_pos_wo_dv_rough_from_venus[:, 1],
                color = AKATSUKI_WO_DV_TRACK_COLOR, zorder = 1) # 軌道修正噴射しない場合の軌道予測
        ax.plot(predict_pv[:, 0], predict_pv[:, 1],
                color = AKATSUKI_PREDICT_TRACK_COLOR) # 今後の予測進路
        ax.plot(sc_pos_from_venus[:current_index, 0],
                sc_pos_from_venus[:current_index, 1],
                color = AKATSUKI_TRACK_COLOR, zorder = 2, linewidth = 2) # あかつきのこれまでの軌跡
        ax.arrow(sun_direction_vec[0] * ARROW_START_POS_LARGE_PLOT,
                 sun_direction_vec[1] * ARROW_START_POS_LARGE_PLOT,
                 sun_direction_vec[0] * ARROW_LENGTH_LARGE_PLOT,
                 sun_direction_vec[1] * ARROW_LENGTH_LARGE_PLOT,
                 width = ARROW_WIDTH_LARGE_PLOT,
                 head_width = ARROW_HEAD_WIDTH_LARGE_PLOT,
                 fc = SUN_DIRECTION_FACE_COLOR, ec = SUN_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 太陽の方向の矢印
        ax.arrow(earth_direction_vec[0] * ARROW_START_POS_LARGE_PLOT,
                 earth_direction_vec[1] * ARROW_START_POS_LARGE_PLOT,
                 earth_direction_vec[0] * ARROW_LENGTH_LARGE_PLOT,
                 earth_direction_vec[1] * ARROW_LENGTH_LARGE_PLOT,
                 width = ARROW_WIDTH_LARGE_PLOT,
                 head_width = ARROW_HEAD_WIDTH_LARGE_PLOT,
                 fc = EARTH_DIRECTION_FACE_COLOR, ec = EARTH_DIRECTION_EDGE_COLOR,
                 alpha = 0.5, zorder = 20)   # 太陽の方向の矢印

        # テキストを出力
        time_str_utc = datetime.datetime.fromtimestamp(current_time).strftime("%Y/%m/%d %H:%M:%S UTC")
        time_str_jst = (datetime.datetime.fromtimestamp(current_time) + datetime.timedelta(hours = 9)).strftime("%Y/%m/%d %H:%M:%S JST")
        plt.text(-0.4e6, 4e5, "{}".format(time_str_utc), fontsize = 18)
        plt.text(-0.4e6, 3.25e5, "{}".format(time_str_jst), fontsize = 18)
        plt.text(-0.4e6, 2.5e5, "Delta V = {:3.1f} [m/s]".format(dv_total), fontsize = 18)
        
        # plt.text(0, 1.8e8, "Delta V = {:3.1f} [m/s]".format(dv_total))
        # グラフ出力
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("{}_{:05d}.png".format(LARGE_PLOT_FILENAME, frame))
        fig.savefig(output_png_file_name, dpi = 100)

        # グラフを消去
        plt.clf()
        
        frame += 1









