# -*- coding:utf-8 -*-
"""
金星、地球、あかつきの軌道を描く
軌道投入の詳細図は別スクリプトで作成
"""

import csv
import datetime
import os

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d


SP_POS_FILE_NAME = "sun_planets_pos.csv" # 惑星・あかつきの位置を記したファイル名
SC_PV_FILE_NAME = "sc_pv.csv"            # あかつきの位置・速度を記したファイル名
ORBIT_SHAPE_FILE_NAME = "orbit_shape.csv" # 軌道の形状情報のファイル名
DT_PLOT_STEP = 180                   # プロットの間隔[index]
VENUS_PASS_DATE = datetime.datetime(2015,12,7,0,0,0) # 金星最接近日時

PLOT_IMAGE_SAVE_PATH = ".\\plot_images\\"
ORBIT_PLOT_FILE_NAME = "orbit_plot" # 軌道プロットの出力ファイル名
VENUS_FACE_COLOR = "#ffcc66"
VENUS_EDGE_COLOR = "#000000"
VENUS_ORBIT_COLOR = "#ffcc66"
AKATSUKI_FACE_COLOR = "#00ff00"
AKATSUKI_EDGE_COLOR = "black"
AKATSUKI_ORBIT_COLOR = "#66ff66"
SUN_FACE_COLOR = "#ffff00"
SUN_EDGE_COLOR = "#000000"
EARTH_FACE_COLOR = "#0000ff"
EARTH_EDGE_COLOR = "#000000"
EARTH_ORBIT_COLOR = "#6666ff"

def ISOFormat2Datetime(str_iso):
    """
    ISOフォーマットの文字列からdatetimeオブジェクトを返す
    """
    dt = datetime.datetime.strptime(str_iso, "%Y-%m-%dT%H:%M:%S")
    return dt


def readTimeLineDataFormCSV(filename):
    """
    csvファイルから時系列データを読み込む
    時刻データ：csvファイルではutc-iso形式→timestamp形式で格納する
    """
    with open(filename, "r") as input_file:
        csv_reader = csv.reader(input_file)
        time_list = []
        data_list = []
        for one_line_list in csv_reader:
            time_list.append(ISOFormat2Datetime(one_line_list[0]).timestamp())
            data_list.append(list(map(float, one_line_list[1:])))
        time_array = np.array(time_list)
        data_array = np.array(data_list)
        return time_array, data_array


if __name__ == "__main__":
    # ファイルから必要な情報を読み込む
    print("csvファイルから軌道情報を読み込み中 ... ", end = "", flush = True)
    orbit_shape = readTimeLineDataFormCSV(ORBIT_SHAPE_FILE_NAME)[1] # 軌道の形状情報を読み込む
    sp_timeline, sp_pos = readTimeLineDataFormCSV(SP_POS_FILE_NAME) # 太陽、惑星の位置データを読み込む
    sc_timeline, sc_pv = readTimeLineDataFormCSV(SC_PV_FILE_NAME)   # あかつきの位置・速度データを読み込む
    print("完了")

    print("太陽、惑星の位置の補間式を作成中 ... ", end = "", flush = True)
    sp_pos_f = interp1d(sp_timeline, sp_pos.T, kind = "cubic") # 太陽、惑星の位置のスプライン補間関数を作成
    sp_pos_full = sp_pos_f(sc_timeline).T # 太陽、惑星の位置の一覧(あかつきの位置データと時系列をそろえた)
    print("完了")

    # プロットの準備
    fig = plt.figure()
    if not os.path.exists(PLOT_IMAGE_SAVE_PATH):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(PLOT_IMAGE_SAVE_PATH)

    frame = 1
    for index in range(0, sc_timeline.shape[0], DT_PLOT_STEP):
        current_time = datetime.datetime.fromtimestamp(sc_timeline[index]) # 現在時刻
        # 描画の準備（縮小図）
        ax = fig.add_subplot(111)
        ax.axis([-2e8, 2e8, -2e8, 2e8])
        ax.set_aspect('equal')
        plt.xticks([-2e8, -1e8, 0, 1e8, 2e8])
        plt.yticks([-2e8, -1e8, 0, 1e8, 2e8])
        plt.xlabel("[km]")
        plt.ylabel("[km]")

        # 太陽を描画
        ax.plot(0, 0, marker = "o", markersize = 10,
                markerfacecolor = SUN_FACE_COLOR, markeredgecolor = SUN_EDGE_COLOR,
                zorder = 1)
        # 金星、地球の軌道を描画
        ax.plot(orbit_shape[:, 0], orbit_shape[:, 1],
                color = VENUS_ORBIT_COLOR, linewidth = 1, zorder = 10)
        ax.plot(orbit_shape[:, 3], orbit_shape[:, 4],
                color = EARTH_ORBIT_COLOR, linewidth = 1, zorder = 11)
        # あかつきの軌道を描画（軌道投入前のみ)
        if current_time < VENUS_PASS_DATE:
            ax.plot(orbit_shape[:, 6], orbit_shape[:, 7],
                    color = AKATSUKI_ORBIT_COLOR, linewidth = 1, zorder = 12)

        # 金星、地球、あかつきをプロット
        ax.plot(sp_pos_full[index, 3], sp_pos_full[index, 4],
                marker = "o", markersize = 8,
                markerfacecolor = VENUS_FACE_COLOR, markeredgecolor = VENUS_EDGE_COLOR,
                zorder = 20)
        ax.plot(sp_pos_full[index, 6], sp_pos_full[index, 7],
                marker = "o", markersize = 8,
                markerfacecolor = EARTH_FACE_COLOR, markeredgecolor = EARTH_EDGE_COLOR,
                zorder = 21)
        ax.plot(sc_pv[index, 0], sc_pv[index, 1],
                marker = "s", markersize = 8,
                markerfacecolor = AKATSUKI_FACE_COLOR, markeredgecolor = AKATSUKI_EDGE_COLOR,
                zorder = 22)

        # テキストを出力
        # time_str_utc = datetime.datetime.fromtimestamp(current_time).strftime("%Y/%m/%d %H:%M:%S UTC")
        # time_str_jst = (datetime.datetime.fromtimestamp(current_time) + datetime.timedelta(hours = 9)).strftime("%Y/%m/%d %H:%M:%S JST")
        # plt.text(-0.4e6, 4e5, "{}".format(time_str_utc), fontsize = 18)
        # plt.text(-0.4e6, 3.25e5, "{}".format(time_str_jst), fontsize = 18)

        # グラフ出力
        output_png_file_name = PLOT_IMAGE_SAVE_PATH + ("{}_{:05d}.png".format(ORBIT_PLOT_FILE_NAME, frame))
        frame += 1
        fig.savefig(output_png_file_name, dpi = 100)
        plt.clf()
