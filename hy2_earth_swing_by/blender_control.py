# -*- coding:utf-8 -*-

import os
import csv
import numpy as np
import bpy


# Blenderオブジェクト名
HY2_DUMMY = bpy.data.objects['Hayabusa Dummy']
SUN_FOR_EAERTH = bpy.data.objects['Sun_for_Earth_P']
EARTH = bpy.data.objects['Earth_UV']
BCAM_EARTH = bpy.data.objects['B-Cam_Earth_P']

HY = bpy.data.objects['HAYABUSA_SC']
SUN_FOR_HY = bpy.data.objects['Sun_P']
BCAM_HY = bpy.data.objects['B-Cam_HY_P2']
SUN_LIGHT_FOR_HY = bpy.data.lamps['Sun_Main_PlusX']

EARTH_MATERIAL = bpy.data.materials['Earth_Surface']

# SCENE名
EARTH_SCENE = bpy.data.scenes['earth']
HAYABUSA_SCENE = bpy.data.scenes['hayabusa']

# CAMERA名
HY_SCENE_CAMERA = {'B-CAM_HY': bpy.data.objects['B-Cam_HY']}
EARTH_SCENE_CAMERA1 = {'ONC-T': bpy.data.objects['ONC-T'],
                       'ONC-W1': bpy.data.objects['ONC-W1'],
                       'ONC-W2': bpy.data.objects['ONC-W2']}
EARTH_SCENE_CAMERA2 = {'B-CAM_EARTH': bpy.data.objects['B-Cam_Earth']}


# スケール
EARTH_SCALE = 0.001

# 読み込むファイル名
CSV_FILENAME = 'datas_for_blender.csv'


def read_csv(filename):
    """
    csvファイルから情報を読み取る
    :param filname: csvファイル名
    :type filename: str
    :return: 読み取った情報
             'utc' : 時刻(UTC)の文字列
             'earth_pos' : 地球の位置（太陽基準）
             'earth_q' : 地球の姿勢(クォータニオン)
             'sun_pos' : 太陽の位置(すべて0が入っている)
             'sun_q' : 地球基準の太陽の方位(クォータニオン)
             'BCam_pos' : 後方カメラの位置
             'BCam_q' : 後方カメラの姿勢(クォータニオン)
             'Sun_energy' : 太陽光の強度(地球の影を考慮)
    :rtype: dict
    """
    
    with open(filename, "r") as csv_file:
        reader = csv.reader(csv_file)
        utc_str_list = []
        earth_pos_q_list = []
        hy2_pos_q_list = []
        sun_pos_q_list = []
        Bcam_pos_q_list = []
        sun_energy_list = []
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for oneline in reader:
            utc_str_list.append(oneline[0])
            earth_pos_q_list.append([float(s) for s in oneline[1:8]])
            hy2_pos_q_list.append([float(s) for s in oneline[8:15]])
            sun_pos_q_list.append([float(s) for s in oneline[15:22]])
            Bcam_pos_q_list.append([float(s) for s in oneline[22:29]])
            sun_energy_list.append(1 - float(oneline[29]))
        earth_pos_q = np.array(earth_pos_q_list)
        hy2_pos_q = np.array(hy2_pos_q_list)
        sun_pos_q = np.array(sun_pos_q_list)
        Bcam_pos_q = np.array(Bcam_pos_q_list)
        sun_energy = np.array(sun_energy_list)
        earth_pos = earth_pos_q[:, :3]
        earth_q = earth_pos_q[:, 3:]
        hy2_pos = hy2_pos_q[:, :3]
        hy2_q = hy2_pos_q[:, 3:]
        sun_pos = sun_pos_q[:, :3]
        sun_q = sun_pos_q[:, 3:]
        Bcam_pos = Bcam_pos_q[:, :3]
        Bcam_q = Bcam_pos_q[:, 3:]
        result = {'utc': utc_str_list,
                  'earth_pos': earth_pos, 'earth_q': earth_q,
                  'hy2_pos': hy2_pos, 'hy2_q': hy2_q,
                  'sun_pos': sun_pos, 'sun_q': sun_q,
                  'Bcam_pos': Bcam_pos, 'Bcam_q': Bcam_q,
                  'sun_energy' : sun_energy}
        
    return result


def set_earth_scene(info):
    """
    地球レンダリング用のシーンの設定
    :param info: csvから読み取った情報
    :type info: dict
    """
    
    # フレームのセット
    n = len(info['utc'])        # 総フレーム数
    EARTH_SCENE.frame_start = 1
    EARTH_SCENE.frame_end = n

    # 各フレームに情報を設定
    bpy.context.window.screen.scene = EARTH_SCENE
    for frame in range(n):
        EARTH_SCENE.frame_set(frame + 1)
        set_pos_and_orientation(target_object = EARTH,
                                pos = np.zeros(3), orientation = info['earth_q'][frame, :],
                                scale = EARTH_SCALE)
        set_pos_and_orientation(target_object = HY2_DUMMY,
                                pos = info['hy2_pos'][frame, :], orientation = info['hy2_q'][frame, :],
                                scale = EARTH_SCALE)
        set_pos_and_orientation(target_object = SUN_FOR_EAERTH,
                                pos = info['sun_pos'][frame, :], orientation = info['sun_q'][frame, :],
                                scale = EARTH_SCALE)
        set_pos_and_orientation(target_object = BCAM_EARTH,
                                pos = info['Bcam_pos'][frame, :], orientation = info['Bcam_q'][frame, :],
                                scale = EARTH_SCALE)
        bpy.context.scene.timeline_markers.new(info['utc'][frame], frame = frame+1)

    return
    

def set_hayabusa_scene(info):
    """
    はやぶさ2レンダリング用のシーンの設定
    :param info: csvから読み取った情報
    :type info: dict
    """
    
    # フレームのセット
    n = len(info['utc'])        # 総フレーム数
    HAYABUSA_SCENE.frame_start = 1
    HAYABUSA_SCENE.frame_end = n
    
    # 各フレームに情報を設定
    bpy.context.window.screen.scene = HAYABUSA_SCENE
    for frame in range(n):
        HAYABUSA_SCENE.frame_set(frame + 1)
        set_pos_and_orientation(target_object = HY,
                                pos = np.zeros(3), orientation = info['hy2_q'][frame, :], scale = 1)
        set_pos_and_orientation(target_object = SUN_FOR_HY,
                                pos = info['sun_pos'][frame, :], orientation = info['sun_q'][frame, :], scale = 1)
        set_pos_and_orientation(target_object = BCAM_HY,
                                pos = np.zeros(3), orientation = info['Bcam_q'][frame, :], scale = 1)
        bpy.context.scene.timeline_markers.new(info['utc'][frame], frame = frame+1)
        SUN_LIGHT_FOR_HY.energy = info['sun_energy'][frame]
        SUN_LIGHT_FOR_HY.keyframe_insert(data_path = 'energy')
                                
    return

def set_pos_and_orientation(target_object, pos, orientation, scale):
    """
    オブジェクトの位置、姿勢を設定する
    :param target_object: 位置、姿勢を設定するオブジェクト
    :type targaet_object: bpy_types.Object
    :param pos: オブジェクトの位置
    :type pos: numpy.ndarray
    :param orientation: オブジェクトの姿勢
    :type orientation: numpy.ndarray
    :param scale: 縮尺
    :type scale: float
    """
    target_object.location = list(pos * scale)
    target_object.rotation_quaternion = list(orientation)
    target_object.keyframe_insert(data_path = 'location')
    target_object.keyframe_insert(data_path = 'rotation_quaternion')
    return


def render_HY_scene():
    """
    はやぶさ2をレンダリングする
    """
    bpy.context.window.screen.scene = HAYABUSA_SCENE
    original_path = os.path.split(os.path.split(__file__)[0])[0]
    filepath = '{}\\HY2_render'.format(original_path)
    for k, v in HY_SCENE_CAMERA.items():
        bpy.context.scene.camera = v
        filepath_full = '{}\\{}'.format(filepath, k)
        # 保存先のフォルダがなかったら作成する
        if not os.path.exists(filepath_full):
            print("画像を出力するディレクトリが存在しないので作成します", flush = True)
            os.makedirs(filepath_full)
        print("rendering ... (CAMERA = {})".format(k), flush = True)
        HAYABUSA_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
        bpy.ops.render.render(animation=True)
        os.chdir(original_path)
    return


def render_EARTH_scene():
    """
    地球をレンダリングする
    """
    bpy.context.window.screen.scene = EARTH_SCENE
    original_path = os.path.split(os.path.split(__file__)[0])[0]
    filepath = '{}\\EARTH_render'.format(original_path)

    # 搭載カメラ視野をレンダリングする
    EARTH_MATERIAL.emit = 0     # 地球の影の部分は発光させない
    for k, v in EARTH_SCENE_CAMERA1.items():
        bpy.context.scene.camera = v
        filepath_full = '{}\\{}'.format(filepath, k)
        # 保存先のフォルダがなかったら作成する
        if not os.path.exists(filepath_full):
            print("画像を出力するディレクトリが存在しないので作成します", flush = True)
            os.makedirs(filepath_full)
        print("rendering ... (CAMERA = {})".format(k), flush = True)
        EARTH_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
        bpy.ops.render.render(animation=True)
        os.chdir(original_path)

    # 後方カメラ視野をレンダリングする
    EARTH_MATERIAL.emit = 0.05  # 地球の影の部分も発光させる
    for k, v in EARTH_SCENE_CAMERA2.items():
        bpy.context.scene.camera = v
        filepath_full = '{}\\{}'.format(filepath, k)
        # 保存先のフォルダがなかったら作成する
        if not os.path.exists(filepath_full):
            print("画像を出力するディレクトリが存在しないので作成します", flush = True)
            os.makedirs(filepath_full)
        print("rendering ... (CAMERA = {})".format(k), flush = True)
        EARTH_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
        bpy.ops.render.render(animation=True)
        os.chdir(original_path)
        
    return


def render_all():
    """
    すべてのレンダリングを行う
    """
    render_EARTH_scene()
    render_HY_scene()
    return


if __name__ == "__main__":
    info = read_csv(CSV_FILENAME)
    set_earth_scene(info)
    set_hayabusa_scene(info)
    # render_all()
