# -*- coding:utf-8 -*-

import os
import sys
import datetime

import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt

import spiceypy as spice

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import SpiceyPyExt as spiceExt


# spice kernel 読み込み関連
CWD = os.path.dirname(os.path.abspath(__file__)) # このスクリプトのあるフォルダ
MK_REL_PATH = "spice_bundle\spice_kernels\mk"    # Meta Kernelのあるフォルダ（相対）
MK_ABS_PATH = "{}\{}".format(CWD, MK_REL_PATH)   # Meta Kernelのあるフォルダ（絶対）
MK_FILENAME = "hyb2_v01.tm"                      # Meta Kernelのファイル名

# SPICEで使用する名前
HAYABUSA2_BODY = "HAYABUSA2"    # はやぶさ2
HAYABUSA2_FRAME = "HAYABUSA2_SC_BUS_PRIME" # はやぶさ2の座標系
# EARTH_BODY = "EARTH BARYCENTER"
EARTH_BODY = "EARTH"            # 地球（BARYCENTERを用いると、地球の真の位置とは異なる）
EARTH_FRAME = "IAU_EARTH"       # 地球の座標系(Z : 自転軸、+X : 経度0°)
SS_BARYCENTER = "SOLAR SYSTEM BARYCENTER" # 太陽系重心位置
SUN_BODY = "SUN"                          # 太陽
INERTIAL_FRAME = "ECLIPJ2000"             # 慣性座標系 = 黄道座標系

# 日付
START_DT = datetime.datetime(2015, 12, 2, 15, 0) # 計算開始
END_DT = datetime.datetime(2015, 12, 4, 5, 0, 0) # 計算終了
FLYBY_DT = datetime.datetime(2015, 12, 3, 10, 8, 7) # 地球スウィングバイ
DELTA_T = 20                                        # 計算間隔[sec]

# 太陽、地球の大きさ
SUN_RAD = 695700                # 太陽半径[km]
EARTH_RAD = 6371                # 地球半径[km]

# カメラ視点
BCAM_X_ANGLE = 5                # 後方カメラ視線 X軸周りの回転角[deg]
BCAM_Y_ANGLE = -10              # 後方カメラ視線 Y軸周りの回転角[deg]

# Blender用の数値データ書き込み先
CSV_FILENAME = "datas_for_blender.csv"


def extract_pos_and_orientation():
    """
    地球、はやぶさ2、太陽の位置、方位をSPICEから抽出する
    : return : 抽出結果（辞書形式）
               'ET' : 計算時刻の一覧
               'EARTH' : 地球に関する情報
                  'pos' : 地球の位置（太陽基準）
                  'quaternion' : 座標回転量（黄道座標系基準）
               'HY2' : はやぶさ2に関する情報
                  'pos' : はやぶさ2の位置（地球基準）
                  'quaternion' : 座標回転量（黄道座標系基準）
               'SUN' : 太陽に関する情報
                  'pos' : 太陽の位置（すべて0としてある）
                  'quaternion' : 地球から見た太陽の方位 「+X方向に太陽がある状態」からの座標系回転量として計算
    : rtype : dict
    """

    print("地球、はやぶさ2の位置、姿勢を抽出中 ... ", end = "", flush = True)
    # 計算時刻の準備
    start_et = spiceExt.datetime2et(START_DT)
    end_et = spiceExt.datetime2et(END_DT)
    et_array = np.arange(start_et, end_et, DELTA_T) # 計算時刻一覧
    n = et_array.shape[0]       # 計算点数

    # 地球に関する情報の抽出
    earth_pos = spiceExt.spkezr(EARTH_BODY, et_array, INERTIAL_FRAME, "NONE", SS_BARYCENTER)[0][:, :3]
    earth_q = spiceExt.m2q(spiceExt.pxform(EARTH_FRAME, INERTIAL_FRAME, et_array))

    # はやぶさ2に関する情報の抽出
    hy2_pos = spiceExt.spkezr(HAYABUSA2_BODY, et_array, INERTIAL_FRAME, "NONE", EARTH_BODY)[0][:, :3]
    hy2_q = spiceExt.m2q(spiceExt.pxform(HAYABUSA2_FRAME, INERTIAL_FRAME, et_array))

    # 太陽に関する情報の抽出
    sun_direction = -earth_pos / norm(earth_pos, axis = 1).reshape((n, 1))
    sun_elev = np.arcsin(sun_direction[:, 2])
    sun_azim = np.arctan2(sun_direction[:, 1], sun_direction[:, 0])
    sun_q = spiceExt.m2q(spiceExt.eul2m(-sun_azim, sun_elev, np.zeros(n), 3, 2, 1))

    print("完了")
    
    # 情報を辞書にまとめて出力
    earth_info = {'pos': earth_pos, 'quaternion': earth_q}
    hy2_info = {'pos': hy2_pos, 'quaternion': hy2_q}
    sun_info = {'pos': np.zeros((n, 3)), 'quaternion': sun_q}
    result = {'ET': et_array, 'EARTH': earth_info, 'HY2': hy2_info, 'SUN': sun_info}
    return result


def calc_BCam_orientation(info):
    """
    後方カメラの視線方向を計算する
    （はやぶさ2から地球をみた方向を基準に視線方向を定める）
    :param info: 地球、はやぶさ2、太陽の位置、姿勢情報
    :type info: dict
    :return: infoに後方カメラ情報を追加したもの
              'BCAM' : 後方カメラ情報
                'pos' : はやぶさ2位置と同じ値
                'quaternion' : 後方カメラの視線方向
    :rtype: dict
    """

    print("後方からのカメラの視線方向を計算中 ... ", end = "", flush = True)
    
    # 計算の準備
    n = info['ET'].shape[0]     # 計算点数
    # X軸周りの回転行列
    BCam_offset_XRot = np.array([[np.cos(np.deg2rad(BCAM_X_ANGLE)), -np.sin(np.deg2rad(BCAM_X_ANGLE)), 0],
                                 [np.sin(np.deg2rad(BCAM_X_ANGLE)),  np.cos(np.deg2rad(BCAM_X_ANGLE)), 0],
                                 [0, 0, 1]])
    # Y軸周りの回転行列
    BCam_offset_YRot = np.array([[ np.cos(np.deg2rad(BCAM_Y_ANGLE)), 0, np.sin(np.deg2rad(BCAM_Y_ANGLE))],
                                 [0, 1, 0],
                                 [-np.sin(np.deg2rad(BCAM_Y_ANGLE)),  0, np.cos(np.deg2rad(BCAM_Y_ANGLE))]])
    # X軸回転→Y軸回転の順に回転する行列
    BCam_offset_XYRot = BCam_offset_YRot.dot(BCam_offset_XRot)

    # はやぶさから地球を見た方向を抽出する
    vecZ_all = -info['HY2']['pos']
    vecZ_all /= norm(vecZ_all, axis = 1).reshape((n, 1)) # 長さを1に正規化
    BCam_M = np.zeros((n, 3, 3))
    for idx in range(n):
        vecZ = vecZ_all[idx, :] # はやぶさ2→地球方向をZ方向とする
        vecX0 = np.array((1, 0, 0)) # X方向は黄道座標系+X方向と極力そろえる
        vecX = vecX0 - np.sum((vecZ * vecX0)) * vecZ
        vecX /= norm(vecX)
        vecY = np.cross(vecZ, vecX) # Y方向は、Z,Xに対して右手系を為すように決める
        # はやぶさ2→地球方向をZ方向に変換する行列を作る
        BCam_M[idx, :, 0] = vecX
        BCam_M[idx, :, 1] = vecY
        BCam_M[idx, :, 2] = vecZ
        # X軸Y軸周りに回転する（はやぶさ2と地球が完全に重ならないようにするため）
        BCam_M[idx, :, :] = BCam_offset_XYRot.dot(BCam_M[idx, :, :])
    BCam_q = spiceExt.m2q(BCam_M) # 座標変換行列をクォータニオン化
    result = {'pos': info['HY2']['pos'], 'quaternion':BCam_q}
    info['BCAM'] = result

    print("完了")

    return info


def calc_sun_shadow_rate(info):
    """
    地球の影に入っているかどうかを判定する
    :param info: 地球、はやぶさ2、太陽の位置、姿勢情報
    :type info: dict
    :return: infoに地球の影情報を追加したもの
              'shadow_rate' : 太陽が何割隠されているか
    :rtype: dict
    """

    print("地球の影情報を計算中 ... ", end = "", flush = True)
    
    distance_s = norm(info['EARTH']['pos'], axis = 1) # はやぶさ2から太陽までの距離≒地球から太陽までの距離
    distance_e = norm(info['HY2']['pos'], axis = 1)   # はやぶさ2から地球までの距離
    angle_s_e = np.arccos(np.sum((-info['EARTH']['pos']) * (-info['HY2']['pos']), axis = 1)
                          / (distance_s * distance_e)) # 太陽-はやぶさ-地球の為す角[rad]
    vis_rad_s = np.arcsin(SUN_RAD / distance_s) # 太陽の視半径[rad]
    vis_rad_e = np.arcsin(EARTH_RAD / distance_e) # 地球の視半径[rad]
    shadow_rate = (vis_rad_s + vis_rad_e - angle_s_e) / (2 * vis_rad_s) # 太陽が地球に隠される割合
    shadow_rate[shadow_rate < 0] = 0                                    # 太陽が完全に見えている状態
    shadow_rate[shadow_rate > 1] = 1                                    # 太陽が完全に隠されている状態
    info['shadow_rate'] = shadow_rate

    print("完了")
    
    return info


def write_csv(info):
    """
    Blender用の情報を書き出す
    :param info: 地球、はやぶさ2、太陽の位置、姿勢情報
    :type info: dict
    """
    
    with open(CSV_FILENAME, "w") as csv_file:
        print("計算結果を{}に書き出し中 ... ".format(CSV_FILENAME), end = "", flush = True)
        n = info['ET'].shape[0]
        # ヘッダ
        csv_file.write("UTC,earth_pos,,,earth_quaternion,,,,hy2_pos,,,hy2_quaternion,,,,sun_pos,,,sun_quaternion,,,,BCAM_pos,,,BCAM_quaternion,,,,shadow\n")
        for idx in range(n):
            # UTC
            csv_file.write("{}".format(spiceExt.et2datetime(info['ET'][idx]).strftime("%Y%m%dT%H%M%S")))
            # earth
            for xyz in info['EARTH']['pos'][idx,:]:
                csv_file.write(",{:.6e}".format(xyz))
            for wxyz in info['EARTH']['quaternion'][idx,:]:
                csv_file.write(",{:.6e}".format(wxyz))
            # hy2
            for xyz in info['HY2']['pos'][idx,:]:
                csv_file.write(",{:.6e}".format(xyz))
            for wxyz in info['HY2']['quaternion'][idx,:]:
                csv_file.write(",{:.6e}".format(wxyz))
            # sun
            for xyz in info['SUN']['pos'][idx,:]:
                csv_file.write(",{:.6e}".format(xyz))
            for wxyz in info['SUN']['quaternion'][idx,:]:
                csv_file.write(",{:.6e}".format(wxyz))
            # bcam
            for xyz in info['BCAM']['pos'][idx,:]:
                csv_file.write(",{:.6e}".format(xyz))
            for wxyz in info['BCAM']['quaternion'][idx,:]:
                csv_file.write(",{:.6e}".format(wxyz))
            # shadow
            csv_file.write(",{:.6e}".format(info['shadow_rate'][idx]))
            csv_file.write("\n")
        print("完了")
            
    return


def make_velocity_plot(info):
    """
    速度のプロットを作成する
    :param info: 地球、はやぶさ2、太陽の位置、姿勢情報
    :type info: dict
    """

    print("速度のプロットを出力中 ... ", flush = True)
    
    # 速度データの準備
    hy2_sun_v = spiceExt.spkezr(HAYABUSA2_BODY, info['ET'],
                                INERTIAL_FRAME, "NONE", SUN_BODY)[0][:, 3:]    # 太陽固定系での速度ベクトル
    hy2_earth_v = spiceExt.spkezr(HAYABUSA2_BODY, info['ET'],
                                  INERTIAL_FRAME, "NONE", EARTH_BODY)[0][:, 3:] # 地球固定系での速度ベクトル
    t = info['ET'] - spiceExt.datetime2et(FLYBY_DT) # 地球最接近を基準とした時刻[sec]
    hy2_sun_v_norm = norm(hy2_sun_v, axis = 1)      # 太陽固定系での速さ
    hy2_earth_v_norm = norm(hy2_earth_v, axis = 1)  # 地球固定系での速さ

    # プロットの準備
    total_frame = t.shape[0]    # 総フレーム数
    # 保存先のフォルダがなかったら作成する
    save_path = ".//velocity_plot//"      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    # プロットを実施する
    frame = 0
    fig = plt.figure(figsize = (7.2, 4.4))
    plt.style.use('dark_background')
    fig.patch.set_facecolor('black')

    for idx in range(total_frame):
        frame += 1
        save_file_name = "{}velocity_{:05d}.png".format(save_path, frame)
        
        print("  {:05d} / {:05d}".format(frame, total_frame), end = "", flush = True)
        
        vs_sun = plt.subplot(2,1,1)
        vs_earth = plt.subplot(2,1,2)

        vs_sun.set_xlim((np.min(t), np.max(t)))
        vs_sun.set_ylim((30, 34))
        # vs_sun.set_xlabel('Time [sec]')
        vs_sun.set_ylabel('太陽に対する速度\n[km/s]')
        # vs_sun.set_xticks()
        vs_sun.set_yticks((30, 32, 34))
        
        vs_earth.set_xlim((np.min(t), np.max(t)))
        vs_earth.set_ylim((4, 12))
        vs_earth.set_xlabel('Time [sec]')
        vs_earth.set_ylabel('地球に対する速度\n[km/s]')
        # vs_earth.set_xticks()
        vs_earth.set_yticks((4, 8, 12))
        
        vs_sun.plot(t[:idx], hy2_sun_v_norm[:idx], lw = 2, color = 'yellow')
        vs_earth.plot(t[:idx], hy2_earth_v_norm[:idx], lw = 2, color = 'cyan')

        # 画像保存とプロット消去
        plt.savefig(save_file_name)
        plt.clf()

        print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b", end = "", flush = True)
        
    print("完了")
    
    return


def make_info_png(info):
    """
    その他の情報をPNG画像で出力する
    :param info: 地球、はやぶさ2、太陽の位置、姿勢情報
    :type info: dict
    """

    print("その他の情報を画像化して出力中 ... ", flush = True)
    
    # 保存先のフォルダがなかったら作成する
    save_path = ".//info_png_plot//"      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    # プロットを実施する
    frame = 0
    fig = plt.figure(figsize = (7.2, 4.4))
    plt.style.use('dark_background')
    fig.patch.set_facecolor('black')
    total_frame = info['ET'].shape[0]
    t_flyby = info['ET'] - spiceExt.datetime2et(FLYBY_DT) # 地球最接近を基準とした時刻[sec]
    distance_e = norm(info['HY2']['pos'], axis = 1)       # 地球中心までの距離[km]
    distance_es = np.round((distance_e - EARTH_RAD) / 10) * 10 # 地球表面までの距離 10km単位で丸める
    
    for idx in range(total_frame):
        frame += 1
        save_file_name = "{}info_{:05d}.png".format(save_path, frame)
        print("  {:05d} / {:05d}".format(frame, total_frame), end = "", flush = True)

        ax = plt.subplot(111)
        dt_UTC = spiceExt.et2datetime(info['ET'][idx])
        dt_JST = dt_UTC + datetime.timedelta(hours = 9)

        utc_str = dt_UTC.strftime("%Y-%m-%d %H:%M:%S (UTC)")
        jst_str = dt_JST.strftime("%Y-%m-%d %H:%M:%S (JST)")

        flyby_sub_str = "地球最接近 {:+,.0f} [sec]".format(t_flyby[idx])

        distance_e_str = "地球中心までの距離 {:,.0f} [km]".format(distance_e[idx])
        distance_es_str = "地表までの距離 約{:,.0f} [km]".format(distance_es[idx])

        ax.text(0.1, 0.1, utc_str)
        ax.text(0.1, 0.3, jst_str)
        ax.text(0.1, 0.5, flyby_sub_str)
        ax.text(0.1, 0.7, distance_e_str)
        ax.text(0.1, 0.9, distance_es_str)
        
        # 画像保存とプロット消去
        plt.savefig(save_file_name)
        plt.clf()
        print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b", end = "", flush = True)
    print("\n完了")
    return


if __name__ == "__main__":
    # kernel読み込み
    print("Spice Kernelを読み込み中...", end = "", flush = True)
    os.chdir(MK_ABS_PATH)
    spice.furnsh(MK_FILENAME)
    os.chdir(CWD)
    print("\n完了")

    # 位置、方向、影の有無を計算
    info = extract_pos_and_orientation()
    info = calc_BCam_orientation(info)
    info = calc_sun_shadow_rate(info)

    # Blender用に書き出し
    write_csv(info)

    # 速度をプロットする
    make_velocity_plot(info)
    # その他の情報をプロットする
    make_info_png(info)

