# -*- coding:utf-8 -*-

import os
import csv
import numpy as np
from numpy.linalg import norm
import bpy


# Blenderオブジェクト名
HY2_P = bpy.data.objects['Hayabusa2_SC_P'] # はやぶさ2機体+イオンエンジン
IES = [bpy.data.objects['Ion_Thrust_A'],   # はやぶさ2のイオンエンジン
       bpy.data.objects['Ion_Thrust_B'],
       bpy.data.objects['Ion_Thrust_C'],
       bpy.data.objects['Ion_Thrust_D']]
ORBITAL_PLANE = bpy.data.objects['HY2_Orbital_Plane'] # はやぶさ2軌道面
SUN_DIRECTION = bpy.data.objects['Sun_Direction']     # 太陽方向
HY2_V = bpy.data.objects['Orbital_Track']             # はやぶさ2進行方向
HY2_ZAXIS = bpy.data.objects['HY2_Body_and_Zaxis']    # はやぶさ2とZ軸のオブジェクト
HY2_FOR_PRECESSION = bpy.data.objects['HAYABUSA_SC_for_precession'] # 歳差運動描画用のはやぶさ2ボディ
ZAXIS_PASS_PONIT = bpy.data.objects['Zaxis_pass_point'] # XZ平面上ではやぶさ2のZ軸が通過するポイント
HY2_PRECESSION_CENTER = bpy.data.objects['precession_center_point'] # XZ平面上のはやぶさ2の歳差運動の中心点

# scene名
ORBIT_PLOT_SCENE = bpy.data.scenes['orbit_plot'] # EDVEGA軌道のシーン
PRECCESION_PLOT_SCENE = bpy.data.scenes['HY2_precession'] # はやぶさ2の歳差シーン


# 読み込むファイル名
CSV_FILENAME = 'for_blender.csv'


def read_csv(filename):
    """
    csvファイルから情報を読み取る
    :param filname: csvファイル名
    :type filename: str
    :return: 読み取った情報
             'UTC' : 時刻(UTC)の文字列
             'HY2_POS' : はやぶさ2の位置
             'HY2_Q' : はやぶさ2の姿勢(クォータニオン)
             'ORBITAL_PLANE_POS' : 軌道面の位置
             'ORBITAL_PLANE_Q' : 軌道面の姿勢（クォータニオン）
             'SUN_POS': 太陽の位置
             'SUN_Q': 太陽の方向(クォータニオン）
             'HY2_V_POS' : はやぶさ2の進行方向の表示位置
             'HY2_V_Q' : はやぶさ2の進行方向の向き（クォータニオン）
             'IES_FLAG' : イオンエンジンの運転状態
    :rtype: dict
    """
    
    with open(filename, "r") as csv_file:
        reader = csv.reader(csv_file)
        utc_str_list = []
        hy2_pos_q_list = []
        orbital_plane_pos_q_list = []
        sun_pos_q_list = []
        hy2v_pos_q_list = []
        ies_flag_list = []
        next(reader)            # 1行目はヘッダなので読み飛ばす
        for oneline in reader:
            utc_str_list.append(oneline[0])
            hy2_pos_q_list.append([float(s) for s in oneline[1:8]])
            orbital_plane_pos_q_list.append([float(s) for s in oneline[8:15]])
            sun_pos_q_list.append([float(s) for s in oneline[15:22]])
            hy2v_pos_q_list.append([float(s) for s in oneline[22:29]])
            ies_flag_list.append([(s == '1') for s in oneline[29:]])
        hy2_pos_q = np.array(hy2_pos_q_list)
        orbital_plane_pos_q = np.array(orbital_plane_pos_q_list)
        sun_pos_q = np.array(sun_pos_q_list)
        hy2v_pos_q = np.array(hy2v_pos_q_list)
        ies_flag = np.array(ies_flag_list)

        hy2_pos = hy2_pos_q[:, :3]
        hy2_q = hy2_pos_q[:, 3:]
        orbital_plaen_pos = orbital_plane_pos_q[:, :3]
        orbital_plane_q = orbital_plane_pos_q[:, 3:]
        sun_pos = sun_pos_q[:, :3]
        sun_q = sun_pos_q[:, 3:]
        hy2v_pos = hy2v_pos_q[:, :3]
        hy2v_q = hy2v_pos_q[:, 3:]
        
        result = {'UTC': utc_str_list,
                  'HY2_POS': hy2_pos, 'HY2_Q': hy2_q,
                  'ORBITAL_PLANE_POS': orbital_plaen_pos, 'ORBITAL_PLANE_Q': orbital_plane_q,
                  'SUN_POS': sun_pos, 'SUN_Q': sun_q,
                  'HY2_V_POS': hy2v_pos, 'HY2_V_Q': hy2v_q,
                  'IES_FLAG': ies_flag}
        
    return result


def set_orbit_plot_scene(info):
    """
    軌道プロット用のシーンの設定
    :param info: csvから読み取った情報
    :type info: dict
    """
    # 既に設定されているアニメーションを消去する
    clear_animation(ORBIT_PLOT_SCENE)
    
    # フレームの設定
    step = 1
    n = len(info['UTC'])        # 総フレーム数
    ORBIT_PLOT_SCENE.frame_start = 1
    ORBIT_PLOT_SCENE.frame_end = int(n/step)

    # 各フレームに情報を設定していく
    bpy.context.window.screen.scene = ORBIT_PLOT_SCENE
    frame = 0
    for idx in range(0,n,step):
        frame += 1
        print("{}/{}".format(frame, n))
        ORBIT_PLOT_SCENE.frame_set(frame)
        set_pos_and_orientation(target_object = HY2_P,
                                pos = np.zeros(3), orientation = info['HY2_Q'][idx, :],
                                scale = 1)
        set_pos_and_orientation(target_object = ORBITAL_PLANE,
                                pos = info['ORBITAL_PLANE_POS'][idx, :],
                                orientation = info['ORBITAL_PLANE_Q'][idx, :],
                                scale = 1)
        set_pos_and_orientation(target_object = SUN_DIRECTION,
                                pos = info['SUN_POS'][idx, :],
                                orientation = info['SUN_Q'][idx, :],
                                scale = 1)
        set_pos_and_orientation(target_object = HY2_V,
                                pos = info['HY2_V_POS'][idx, :],
                                orientation = info['HY2_V_Q'][idx, :],
                                scale = 1)
        
        for ies_num in range(4): # イオンエンジン運転状態のセット
            IES[ies_num].hide = not info['IES_FLAG'][idx, ies_num]
            IES[ies_num].hide_render = not info['IES_FLAG'][idx, ies_num]
            IES[ies_num].keyframe_insert(data_path = 'hide_render')
            IES[ies_num].keyframe_insert(data_path = 'hide')
            for child in IES[ies_num].children:
                child.hide_render = not info['IES_FLAG'][idx, ies_num]
                child.hide = not info['IES_FLAG'][idx, ies_num]
                child.keyframe_insert(data_path = 'hide_render')
                child.keyframe_insert(data_path = 'hide')
                                
        bpy.context.scene.timeline_markers.new(info['UTC'][idx], frame = frame)
    return


def clear_animation(scene):
    """
    指定したシーンのアニメーションをすべて解除する
    :param scene: アニメーションを消去したいシーン
    """
    # clear all animation data
    # bpy.context.window.screen.scene = ORBIT_PLOT_SCENE

    for ob in scene.objects:
        ob.animation_data_clear()

    # clear frame setting
    scene.frame_start = 1
    scene.frame_end = 1
    scene.frame_set(1)
    scene.timeline_markers.clear()  

    return    


def set_pos_and_orientation(target_object, pos, orientation, scale):
    """
    オブジェクトの位置、姿勢を設定する
    :param target_object: 位置、姿勢を設定するオブジェクト
    :type targaet_object: bpy_types.Object
    :param pos: オブジェクトの位置
    :type pos: numpy.ndarray
    :param orientation: オブジェクトの姿勢
    :type orientation: numpy.ndarray
    :param scale: 縮尺
    :type scale: float
    """
    target_object.location = list(pos * scale)
    target_object.rotation_quaternion = list(orientation)
    target_object.keyframe_insert(data_path = 'location')
    target_object.keyframe_insert(data_path = 'rotation_quaternion')
    return


# def render_HY_scene():
#     """
#     はやぶさ2をレンダリングする
#     """
#     bpy.context.window.screen.scene = HAYABUSA_SCENE
#     original_path = os.path.split(os.path.split(__file__)[0])[0]
#     filepath = '{}\\HY2_render'.format(original_path)
#     for k, v in HY_SCENE_CAMERA.items():
#         bpy.context.scene.camera = v
#         filepath_full = '{}\\{}'.format(filepath, k)
#         # 保存先のフォルダがなかったら作成する
#         if not os.path.exists(filepath_full):
#             print("画像を出力するディレクトリが存在しないので作成します", flush = True)
#             os.makedirs(filepath_full)
#         print("rendering ... (CAMERA = {})".format(k), flush = True)
#         HAYABUSA_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
#         bpy.ops.render.render(animation=True)
#         os.chdir(original_path)
#     return


# def render_EARTH_scene():
#     """
#     地球をレンダリングする
#     """
#     bpy.context.window.screen.scene = EARTH_SCENE
#     original_path = os.path.split(os.path.split(__file__)[0])[0]
#     filepath = '{}\\EARTH_render'.format(original_path)

#     # 搭載カメラ視野をレンダリングする
#     EARTH_MATERIAL.emit = 0     # 地球の影の部分は発光させない
#     for k, v in EARTH_SCENE_CAMERA1.items():
#         bpy.context.scene.camera = v
#         filepath_full = '{}\\{}'.format(filepath, k)
#         # 保存先のフォルダがなかったら作成する
#         if not os.path.exists(filepath_full):
#             print("画像を出力するディレクトリが存在しないので作成します", flush = True)
#             os.makedirs(filepath_full)
#         print("rendering ... (CAMERA = {})".format(k), flush = True)
#         EARTH_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
#         bpy.ops.render.render(animation=True)
#         os.chdir(original_path)

#     # 後方カメラ視野をレンダリングする
#     EARTH_MATERIAL.emit = 0.05  # 地球の影の部分も発光させる
#     for k, v in EARTH_SCENE_CAMERA2.items():
#         bpy.context.scene.camera = v
#         filepath_full = '{}\\{}'.format(filepath, k)
#         # 保存先のフォルダがなかったら作成する
#         if not os.path.exists(filepath_full):
#             print("画像を出力するディレクトリが存在しないので作成します", flush = True)
#             os.makedirs(filepath_full)
#         print("rendering ... (CAMERA = {})".format(k), flush = True)
#         EARTH_SCENE.render.filepath = "{}\\{}".format(filepath_full, k)
#         bpy.ops.render.render(animation=True)
#         os.chdir(original_path)
        
#     return


# def render_all():
#     """
#     すべてのレンダリングを行う
#     """
#     render_EARTH_scene()
#     render_HY_scene()
#     return

def calc_q_vec_to_vec(from_vec, to_vec):
    """
    from_vecをto_vecに移す回転操作のクォータニオンを求める
    :param from_vec: 回転操作前のベクトル
    :type from_vec: numpy.ndarray
    :param to_vec: 回転操作後のベクトル
    :type to_vec: numpy.ndarray
    :return: 回転操作を表すクォータニオン
    :rtpye: numpy.ndarray
    """
    # from_vec, to_vecの次元をそろえる
    if from_vec.ndim == 1:
        from_vec = from_vec.reshape((1, from_vec.shape[0]))
    if to_vec.ndim == 1:
        to_vec = to_vec.reshape((1, to_vec.shape[0]))

    if from_vec.shape[1] != 3:
        print('from_vecの次元{}が正しくありません'.format(from_vec.shape))
    if to_vec.shape[1] != 3:
        print('to_vecの次元{}が正しくありません'.format(to_vec.shape))
    if from_vec.shape[0] == 1 and to_vec.shape[0] != 1:
        from_vec_temp = np.zeros(to_vec.shape)
        from_vec_temp[:, :] = from_vec[0, :]
        from_vec = from_vec_temp
    elif to_vec.shape[0] == 1 and from_vec.shape[0] != 1:
        to_vec_temp = np.zeros(from_vec.shape)
        to_vec_temp[:, :] = to_vec[0, :]
        to_vec = to_vec_temp
    if from_vec.shape != to_vec.shape:
        print('from_vecの次元{}とto_vecの次元{}が整合していません'.format(from_vec.shape, to_vec.shape))

    # 回転操作をクォータニオンで記述する
    rot_q = np.zeros((from_vec.shape[0], 4)) # 計算結果(クォータニオンを格納する配列）

    from_vec /= norm(from_vec, axis = 1).reshape((from_vec.shape[0], 1))  # 長さを1に正規化
    to_vec /= norm(to_vec, axis = 1).reshape((to_vec.shape[0], 1))

    angle = np.arccos(np.sum(from_vec * to_vec, axis = 1)) # fromとtoの為す角（内積のarccos）
    rot_q[:, 0] = np.cos(angle / 2)                        # クォータニオンの第一項はcos theta/2
    for idx in range(rot_q.shape[0]):
        rot_axis = np.cross(from_vec[idx, :], to_vec[idx, :]) # 回転軸の向き
        rot_axis /= norm(rot_axis)
        rot_q[idx, 1:4] = rot_axis * np.sin(angle[idx] / 2) # 第2項から4項は回転軸の向き * sin theta/2

    return rot_q


def set_HY2_precession(start_frame, end_frame, start_theta, end_theta, center, radius, phase):
    """
    はやぶさ2の歳差を描画する（歳差運動はXZ平面で決め打ち）
    param start_frame: 描画開始フレーム
    type start_frame: int
    param end_frame: 描画終了フレーム
    type end_frame: int
    param start_theta: 描画開始時の歳差運動の位相[deg]
    type start_theta: float
    param end_theta: 描画終了時の歳差運動の位相[deg]
    type start_theta: float
    param center: 歳差運動の中心点(xc, yc, zc)
    type center: numpy.ndarray
    param radius: 歳差運動の半径(y = yc平面上の値)
    type radisu: float
    param phase: はやぶさ2のz軸周りの位相角[deg]
    type phase: float
    """

    # z軸の通過点の座標を計算
    total_frame = end_frame - start_frame
    theta = np.arange(np.deg2rad(start_theta), np.deg2rad(end_theta),
                      np.deg2rad(end_theta - start_theta) / total_frame) # 歳差運動の角度（フレーム毎）
    z_axis_pass_point = np.zeros((theta.shape[0], 3))
    z_axis_pass_point[:, 0] = center[0] + radius * np.cos(theta)
    z_axis_pass_point[:, 1] = center[1]
    z_axis_pass_point[:, 2] = center[2] + radius * np.sin(theta)

    # # クォータニオンの計算
    # hy2_q = calc_q_vec_to_vec(from_vec = np.array([0,0,1]), to_vec = np.copy(z_axis_pass_point))

    r = norm(z_axis_pass_point, axis = 1)
    x_angle = np.arcsin(z_axis_pass_point[:, 2] / r)
    z_angle = np.arctan2(-z_axis_pass_point[:, 0], z_axis_pass_point[:, 1])
    
    # オブジェクトの動きを設定していく
    bpy.context.window.screen.scene = PRECCESION_PLOT_SCENE
    frame = start_frame
    PRECCESION_PLOT_SCENE.frame_set(frame)
    
    set_pos_and_orientation(target_object = HY2_PRECESSION_CENTER,
                            pos = center, orientation = np.array([1,0,0,0]),
                            scale = 1)
    HY2_FOR_PRECESSION.rotation_euler = [0, np.deg2rad(phase), 0]
    HY2_FOR_PRECESSION.keyframe_insert(data_path = 'rotation_euler')
    
    for idx in range(0,total_frame):
        # print("frame = {}, {}/{}".format(frame, idx, total_frame))
        PRECCESION_PLOT_SCENE.frame_set(frame)
        
        HY2_ZAXIS.location = [0, 0, 0]
        #print(z_angle[idx])
        HY2_ZAXIS.rotation_euler = [x_angle[idx], 0, z_angle[idx]]
        HY2_ZAXIS.keyframe_insert(data_path = 'location')
        HY2_ZAXIS.keyframe_insert(data_path = 'rotation_euler')

        set_pos_and_orientation(target_object = ZAXIS_PASS_PONIT,
                                pos = z_axis_pass_point[idx, :], orientation = np.array([1,0,0,0]),
                                scale = 1)
        frame += 1
    PRECCESION_PLOT_SCENE.frame_set(frame - 1)
    set_pos_and_orientation(target_object = HY2_PRECESSION_CENTER,
                            pos = center, orientation = np.array([1,0,0,0]),
                            scale = 1)
    HY2_FOR_PRECESSION.rotation_euler = [0, np.deg2rad(phase), 0]
    HY2_FOR_PRECESSION.keyframe_insert(data_path = 'rotation_euler')

    return

        
if __name__ == "__main__":
    original_path = os.path.split(os.path.split(__file__)[0])[0]
    os.chdir(original_path)
    info = read_csv(CSV_FILENAME)
    #clear_animation(ORBIT_PLOT_SCENE)
    # set_orbit_plot_scene(info)
    # set_hayabusa_scene(info)
    # render_all()
    clear_animation(PRECCESION_PLOT_SCENE)
    
    set_HY2_precession(start_frame = 1, end_frame = 301,
                       start_theta = 0, end_theta = -360,
                       center = np.array([2, 30, 4]), radius = 3, phase = -90)
    set_HY2_precession(start_frame = 301, end_frame = 376,
                       start_theta = 135, end_theta = 45,
                       center = np.array([-5, 30, -5]), radius = 10, phase = -110)
    set_HY2_precession(start_frame = 476, end_frame = 777,
                       start_theta = 45, end_theta = 45-360,
                       center = np.array([np.cos(np.deg2rad(45)) * 8 -5, 30, np.sin(np.deg2rad(45)) * 8-5]), radius = 2, phase = -40)
    set_HY2_precession(start_frame = 778, end_frame = 779,
                       start_theta = 0, end_theta = 0.1,
                       center = np.array([2, 30, 4]), radius = 0, phase = -90)
    set_HY2_precession(start_frame = 808, end_frame = 809,
                       start_theta = 0, end_theta = 0.1,
                       center = np.array([2, 30, 4]), radius = 3, phase = -90)
    





    
