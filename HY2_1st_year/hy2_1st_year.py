# -*- coding:utf-8 -*-

import os
import sys
import datetime

import numpy as np
from numpy.linalg import norm, inv
from scipy.integrate import ode
import matplotlib.pyplot as plt

import spiceypy as spice

# 1階層上のディレクトリをパスに加える
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import SpiceyPyExt as spiceExt
import orbitalElement as oe

# spice kernel 読み込み関連
CWD = os.path.dirname(os.path.abspath(__file__)) # このスクリプトのあるフォルダ
MK_REL_PATH = "spice_bundle\spice_kernels\mk"    # Meta Kernelのあるフォルダ（相対）
MK_ABS_PATH = "{}\{}".format(CWD, MK_REL_PATH)   # Meta Kernelのあるフォルダ（絶対）
MK_FILENAME = "hyb2_v01.tm"                      # Meta Kernelのファイル名

RYUGU_SPK = "./Ryugu_spice/2162173.bsp"

# SPICEで使用する名前
HAYABUSA2_BODY = "HAYABUSA2"    # はやぶさ2
HAYABUSA2_FRAME = "HAYABUSA2_SC_BUS_PRIME" # はやぶさ2の座標系
# EARTH_BODY = "EARTH BARYCENTER"
EARTH_BODY = "EARTH"            # 地球（BARYCENTERを用いると、地球の真の位置とは異なる）
EARTH_FRAME = "IAU_EARTH"       # 地球の座標系(Z : 自転軸、+X : 経度0°)
MARS_BODY = "MARS BARYCENTER"   # 火星
SS_BARYCENTER = "SOLAR SYSTEM BARYCENTER" # 太陽系重心位置
SUN_BODY = "SUN"                          # 太陽
RYUGU_BODY = "2162173"                    # リュウグウ
INERTIAL_FRAME = "ECLIPJ2000"             # 慣性座標系 = 黄道座標系
VENUS_BODY = "VENUS"                      # 金星
JUPITER_BODY = "JUPITER BARYCENTER"       # 木星

# 重力源情報 (SPICEデータの名称、GM値[km^3 / s^-2]
G_SOURCES = [{'NAME':SUN_BODY,   'GM':132712440041.939400},
             {'NAME':VENUS_BODY, 'GM':324858.592000},
             {'NAME':EARTH_BODY, 'GM':398600.435436},
             {'NAME':MARS_BODY,  'GM':42828.375214},
             {'NAME':JUPITER_BODY, 'GM':126712764.800000}]

# 日付
LAUNCH_DT = datetime.datetime(2014,12,3,4,22,4)
PLOT_PERIOD = {'START': datetime.datetime(2014, 12, 3, 7, 0), # はやぶさ2打ち上げ(打ち上げは4:22だがSPKファイルが存在するのは7:00から)
               'END': datetime.datetime(2015, 12, 31)}         # SPKファイルは'15/12/31まで
PLOT_PERIOD_ET = {}
HY2_SWING_BY_DT = datetime.datetime(2015,12,3, 10, 8, 7) # 地球スウィングバイ日時
HY2_SWING_BY_ET = 0
NEAR_EARTH_PERIOD_DT = [{'START': PLOT_PERIOD['START'],  # 地球近傍の期間（位置、速度から求めた軌道要素が不正確となる）
                         'END' : PLOT_PERIOD['START'] + datetime.timedelta(days = 5),
                         'ALT' : PLOT_PERIOD['START'] + datetime.timedelta(days = 5)}, # 上記期間の軌道要素は、←の値を代用する
                        {'START': HY2_SWING_BY_DT - datetime.timedelta(days = 5),
                         'END' : HY2_SWING_BY_DT,
                         'ALT' : HY2_SWING_BY_DT - datetime.timedelta(days = 5)},
                        {'START': HY2_SWING_BY_DT,
                         'END' :  HY2_SWING_BY_DT + datetime.timedelta(days = 5),
                         'ALT' : HY2_SWING_BY_DT + datetime.timedelta(days = 5)}]
NEAR_EARTH_PERIOD_ET = []

# イオンエンジンの噴射タイミング（ラフ）と使用スラスター一覧：増速量などのグラフから目読み
# 完全に自動で判別するのは困難なため目読みと発表資料からある程度アシストする
IES_TIMING = [{'START': datetime.datetime(2014,12,23), # イオンエンジンAの点火確認
               'END' : datetime.datetime(2014,12,24, 6),
               'THRUSTERS' : 'A'},
              {'START': datetime.datetime(2014,12,24,7), # イオンエンジンBの点火確認
               'END' : datetime.datetime(2014,12,25, 3),
               'THRUSTERS' : 'B'},
              {'START': datetime.datetime(2014,12,25,6), # イオンエンジンCの点火確認
               'END': datetime.datetime(2014,12,26,6),
               'THRUSTERS': 'C'},
              {'START': datetime.datetime(2014,12,26, 12), # イオンエンジンDの点火確認1
               'END': datetime.datetime(2014,12,28),
               'THRUSTERS': 'D'},
              {'START': datetime.datetime(2015,1,10), # イオンエンジンDの点火確認2
               'END': datetime.datetime(2015,1,12),
               'THRUSTERS': 'D'},
              {'START': datetime.datetime(2015,1,12), # イオンエンジンACの組み合わせ試験
               'END': datetime.datetime(2015,1,13),
               'THRUSTERS': 'AC'},
              {'START': datetime.datetime(2015,1,13), # イオンエンジンCDの組み合わせ試験
               'END': datetime.datetime(2015,1,14),
               'THRUSTERS': 'CD'},
              {'START': datetime.datetime(2015,1,14), # イオンエンジンADの組み合わせ試験
               'END': datetime.datetime(2015,1,15),
               'THRUSTERS': 'AD'},
              {'START': datetime.datetime(2015,1,15), # イオンエンジンACの組み合わせ試験
               'END': datetime.datetime(2015,1,16),
               'THRUSTERS': 'AC'},
              {'START': datetime.datetime(2015,1,16), # イオンエンジンACDの組み合わせ試験
               'END': datetime.datetime(2015,1,17),
               'THRUSTERS': 'ACD'},
              {'START': datetime.datetime(2015,1,19), # イオンエンジンADの組み合わせ試験
               'END': datetime.datetime(2015,1,21),
               'THRUSTERS': 'AD'},
              {'START': datetime.datetime(2015,2,24), # 長期運転
               'END': datetime.datetime(2015,3,22),
               'THRUSTERS': 'AD'},
              {'START': datetime.datetime(2015,5,12), # ACD運転
               'END': datetime.datetime(2015,5,14),
               'THRUSTERS': 'ACD'},
              {'START': datetime.datetime(2015,6,2), # 長期運転
               'END': datetime.datetime(2015,6,7),
               'THRUSTERS': 'AD'},
              {'START': datetime.datetime(2015,9,1), # 追加運転
               'END': datetime.datetime(2015,9,2),
               'THRUSTERS': 'AD'}]
IES_TIMING_ET = []

OWC_TIMING = [[datetime.datetime(2015,2,9,9,20),
               datetime.datetime(2015,2,20,9,)],
              [datetime.datetime(2015,3,26,8),
               datetime.datetime(2015,4,2,9,20),
               datetime.datetime(2015,4,4,6,20),
               datetime.datetime(2015,4,14,9,20),
               datetime.datetime(2015,4,16,9,20),
               datetime.datetime(2015,5,7,7,20)],
              [datetime.datetime(2015,6,9,7,40),
               datetime.datetime(2015,6,12,11,20),
               datetime.datetime(2015,6,15,7,40),
               datetime.datetime(2015,6,25,8,40),
               datetime.datetime(2015,6,30,12,00),
               datetime.datetime(2015,7,7,9,20),
               datetime.datetime(2015,8,11,3,40),
               datetime.datetime(2015,9,1,4,20)]]
OWC_TIMING_ET = []

# 計算の設定
DELTA_T = 10*60                                       # 計算間隔[sec]
# DELTA_T = 10*60*60                                       # 計算間隔[sec]

# イオンエンジン運転の判定
ANGLE_TH = 35                   # 機体の-X方向と推力の為す角の閾値[deg]
ACC_MAX_TH = 28e-3 / 600 / 1000 * 1.5 # 発生しうる加速度の上限値[km/s2]（これ以上の加速度はイオンエンジン以外の推力と考える）
                                      # 3台同時運転時の推力28[mN]、機体質量600[kgf]、マージン50%で見積もり
ACC_MIN_TH = ACC_MAX_TH / 20          # 加速度の下限値[km/s2]（これ以下の加速度はノイズと見なす）値はグラフより判断

# プロットの設定
PNG_FOLDER = "plot_image"       # 画像を格納するフォルダ名
PNG_FILENAME = "hy2_1st_year_plot" # 画像ファイル名
PLOT_DELTA_T = 30*60               # プロットの時間間隔[sec]
# PLOT_DELTA_T = 24*60*60               # プロットの時間間隔[sec]
PLANET_COLORS = {SUN_BODY:{'FACE':[1, 1, 0],
                           'EDGE':[1, 1, 0],
                           'TRACK':[1, 1, 0]},
                 EARTH_BODY:{'FACE':[0.19, 0.52, 0.61],
                             'EDGE':[0.57, 0.8, 0.86],
                             'TRACK':[0.19, 0.52, 0.61]},
                 MARS_BODY:{'FACE':[0.58, 0.21, 0.21],
                            'EDGE':[0.85, 0.59, 0.58],
                            'TRACK':[0.58, 0.21, 0.21]},
                 RYUGU_BODY:{'FACE':[0.46, 0.58, 0.23],
                             'EDGE':[0.76, 0.84, 0.61],
                             'TRACK':[0.46, 0.58, 0.23]}}
HY2_COLOR = {'FACE_NOW':[0.96, 0.59, 0.27],
             'EDGE_NOW':[0.96, 0.59, 0.27],
             'FACE_PREDICT':[0,0,0,0],
             'EDGE_PREDICT':[0.96, 0.59, 0.27],
             'TRACK':[0.96, 0.59, 0.27],
             'TRACK_OLD':[0.96, 0.59, 0.27],
             'TRACK_PREDICT':[0.96, 0.59, 0.27]}
DELTA_V_ARROW_COLOR = [1, 0, 0]
DELTA_V_SCALE = 1e8 / (28e-3 / 600 / 1000)   # イオンエンジン3基の最大推力による増速量 = 1e8[km]でプロットされる
                                             # 1e8[km] → 約4[m/s/day]に相当する
DELTA_V_ARROW_HEAD_WIDTH = 3e7               # 増速量を表す矢印の矢頭の幅[km]
DELTA_V_ARROW_WIDTH = DELTA_V_ARROW_HEAD_WIDTH / 3

IES_TEXT_COLORS = [{'NAME': 'Thruster A',
                    'FACE': [1, 0.314, 0.314],
                    'TEXT': [0, 0, 0],
                    'POS' : [12.5, 41]},
                   {'NAME': 'Thruster B',
                    'FACE': [1, 1, 0.314],
                    'TEXT': [0, 0, 0],
                    'POS' : [37.5, 41]},
                   {'NAME': 'Thruster C',
                    'FACE': [0.314, 1, 0.314],
                    'TEXT': [0, 0, 0],
                    'POS' : [12.5, 31]},
                   {'NAME': 'Thruster D',
                    'FACE': [0, 0.69, 1],
                    'TEXT': [0, 0, 0],
                    'POS' : [37.5, 31]},
                   {'NAME': 'OFF', # OFFになっているエンジンのテキスト描画色
                    'FACE': [0.25, 0.25, 0.25],
                    'TEXT': [0.75, 0.75, 0.75]}]
IES_TEXT_BOX_WH = [13, 8]
IES_SYMBOL_COLORS = [{'FACE': [1, 0.8, 1],
                      'EDGE': [1, 0, 0],
                      'POS': [22.5, 40.5]},
                     {'FACE': [1, 1, 0.6],
                      'EDGE': [0.4, 0.2, 0],
                      'POS': [27.5, 40.5]},
                     {'FACE': [0.6, 1, 0.6],
                      'EDGE': [0, 0.7, 0.3],
                      'POS': [22.5, 35.5]},
                     {'FACE': [0.8, 1, 1],
                      'EDGE': [0, 0, 1],
                      'POS': [27.5, 35.5]},
                     {'FACE': [0.25, 0.25, 0.25], # OFFになっているエンジンのシンボル描画色
                      'EDGE': [0.75, 0.75, 0.75]}]
RW_TEXT_COLORS = [{'FACE': [0.314, 1, 0.314],
                   'TEXT': [0, 0, 0]},
                  {'FACE': [0.25, 0.25, 0.25],
                   'TEXT': [0.75, 0.75, 0.75]}]
RW_TEXT_INFO = [{'TEXT': "RW-X",
                 'POS': [17.5, 14]},
                {'TEXT': "RW-Y",
                 'POS': [29.5, 14]},
                {'TEXT': "RW-Z1",
                 'POS': [17.5, 9]},
                {'TEXT': "RW-Z2",
                 'POS': [29.5, 9]}]

SUN_ANGLE_PLOT_COLORS = [(1,0,0),(1, 0.6, 0),(0.2,0.8,0.2),(0,0,1),(0.8, 0, 0.8)] # 太陽との相対角の描画色
SUN_ALGLE_HOLD_PERIOD = 7*24*60*60 # 太陽との相対角を描画する期間[sec]
SUN_ANGLE_PNG_FOLDER = "sun_angle"       # 画像を格納するフォルダ名
SUN_ANGLE_PNG_FILENAME = "sun_angle" # 画像ファイル名

RW_TEXT_BOX_WH = [11, 4]
MARKER_SIZE = {SUN_BODY: 10,
               EARTH_BODY : 6,
               MARS_BODY :6,
               RYUGU_BODY : 8,
               HAYABUSA2_BODY : 8}

# Blender用csvファイル書き出しの設定
CSV_FILENAMER_FOR_BLENDER = "for_blender.csv"


def d_dt_state(t, state_v):
    """
    state vector(位置と速度の6要素からなるベクトル)の微分を与える
    :param t:時刻(ET) [sec]
    :type t: float
    :param state_v:state vector
    :type state_v: numpy.ndarray
    :return d_state_v: state_vの時間微分
    :type d_state_v: numpy.ndarray
    """

    d_state_v = np.zeros(6)     # state_vの微分
    d_state_v[:3] = state_v[3:] # 位置の微分 = 速度

    for g_source in G_SOURCES:  # それぞれの重力源について計算を行う
        pos = spiceExt.spkezr(targ = g_source['NAME'],
                              et = t,
                              ref = INERTIAL_FRAME,
                              abcorr = "NONE",
                              obs = SS_BARYCENTER)[0][:3] # 重力源の位置
        pos_from_g = state_v[:3] - pos          # 重力源基準の探査機の位置
        d_state_v[3:] += -(g_source['GM'] * pos_from_g) / norm(pos_from_g) ** 3 # 重力源によって探査機に生じる加速度

    return d_state_v


def calc_delta_v(info):
    """
    はやぶさ2の増速量（天体からの引力以外の分）を求める
    :param info: SPICEから抽出したデータ
    :type info: dict
    :return: infoにイオンエンジン運転情報を追加したもの
    :rtype: dict
    """

    print("はやぶさ2の増速量を計算中 ... ", flush = True)
    n = info['DATA_NUM']     # 計算総点数
    delta_v = np.zeros((n, 3))  # delta_vを格納する配列
    state_vector = np.zeros((n, 6))
    state_vector[:, :3] = info[HAYABUSA2_BODY]['POS']
    state_vector[:, 3:] = info[HAYABUSA2_BODY]['VELOCITY']

    print("  {:05d}/{:05d}".format(0, n), end = "", flush = True)
    for idx in range(1, n):
        print("\b\b\b\b\b\b\b\b\b\b\b", end = "", flush = True)
        print("{:05d}/{:05d}".format(idx, n), end = "", flush = True)
        # 常微分方程式により1ステップ分慣性飛行をした場合の速度を求める
        ode_rk = ode(d_dt_state)
        ode_rk.set_initial_value(y = state_vector[idx - 1, :], t = info['ET_ARRAY'][idx - 1]) # 1つ前のステップを初期条件とする
        ode_rk.integrate(t = info['ET_ARRAY'][idx]) # 初期条件から現在の位置速度を予測する
        state_vector_wo_delta_v = ode_rk.y         # 噴射を行わない場合の位置、速度
        # 増速量 = SPICEデータから抽出した速度 - 慣性飛行の場合の速度
        delta_v[idx, :] = info[HAYABUSA2_BODY]['VELOCITY'][idx, :] - state_vector_wo_delta_v[3:]
        del ode_rk
    acceralation = delta_v / DELTA_T          # 加速度[km/s2]の単位にする
    del delta_v
    print("\n完了")
    
    print("イオンエンジンによる増速量を選別、使用したスラスターを特定中 ... ", end = "", flush = True)
    # 機体-X方向と加速度の角度を求める（イオンエンジンの推力の場合、機体は-x方向に加速するはず）
    sc_minus_x = -info[HAYABUSA2_BODY]['C_MATRIX'][:, :, 0] # 機体の-X方向@慣性座標系
    angle_dv_sc_minus_x = np.sum(acceralation * sc_minus_x, axis = 1) # 機体-X方向と加速度ベクトルとの内積
    angle_dv_sc_minus_x /= (norm(acceralation, axis = 1) * norm(sc_minus_x, axis = 1)) # 内積をベクトルの長さで割ってcosを求める
    angle_dv_sc_minus_x = np.rad2deg(np.arccos(angle_dv_sc_minus_x)) # 機体-X方向と加速度の為す角[deg]

    # 閾値によってイオンエンジン以外による加速、ノイズを除去する
    acceralation[angle_dv_sc_minus_x > ANGLE_TH, :] = 0 # 増速量が機体の-x方向と大きく異なるものはイオンエンジンの運転とは異なると判定する
    acceralation[norm(acceralation, axis = 1) > ACC_MAX_TH, :] = 0 # イオンエンジンの能力以上の加速の場合
    acceralation[norm(acceralation, axis = 1) < ACC_MIN_TH, :] = 0 # 加速が微小な場合

    # イオンエンジンの運転期間情報を元に、選別、使用しているイオンエンジンを集計する
    is_thrusting = (acceralation != 0).any(axis = 1) # 機体に有意な加速があるかどうか
    thruster_use = np.zeros((n, 4), dtype = bool) # スラスターの使用フラグ
    for idx in range(n):
        if not is_thrusting[idx]: # 機体の加速量が有意な量ではない場合はスキップ
            continue
        for ies_period in IES_TIMING_ET:
            if (ies_period['START'] <= info['ET_ARRAY'][idx] and # IESの運転期間に合致するか検査
                info['ET_ARRAY'][idx] <= ies_period['END']) :
                # イオンエンジンの使用フラグを立てる
                for engine_char in ies_period['THRUSTERS']:
                    thruster_use[idx, (ord(engine_char) - ord('A'))] = True
                break
        if not thruster_use[idx, :].any(): # どのスラスターの使用フラグも立たなかったら増速量を0とする
            acceralation[idx, :] = 0
    print("完了")
    
    print("各スラスターの累積運転時間を算出中 ... ", end = "", flush = True)
    # スラスターごとの累積運転時間を算出する
    thruster_operating_time = np.zeros((n, 4))
    for idx in range(n):
        thruster_operating_time[idx, :] = np.sum(thruster_use[:idx, :], axis = 0) * DELTA_T # [sec]
    print("完了")
    
    # 増速量の合計値を求める
    print("増速量の累積値を算出中 ... ", end = "", flush = True)
    delta_v_per_step = norm(acceralation, axis = 1) * DELTA_T * 1000 # 1ステップ毎の増速量 [m/s]
    total_delta_v = np.zeros(n)
    for idx in range(n):
        total_delta_v[idx] = np.sum(delta_v_per_step[:idx])
    print("完了")
    
    info[HAYABUSA2_BODY]['ACCERALATION'] = acceralation
    info[HAYABUSA2_BODY]['TOTAL_DELTA_V'] = total_delta_v
    info[HAYABUSA2_BODY]['THRUSTER_USE'] = thruster_use
    info[HAYABUSA2_BODY]['THRUSTER_OPERATING_TIME'] = thruster_operating_time
    
    return info


def extract_pos_v():
    """
    天体、探査機の位置、速度をSPK Kernelから抽出する
    :return: 時刻、各天体、探査機の情報などを辞書形式で格納したもの
             ['DATA_NUM'] : データ点数
             ['ET_ARRAY'] : Ephemeris time[sec] numpy.ndarray (n)
             ['DT_LIST'] : datetimeオブジェクトのリスト (n)
             [名称]['POS'] : 位置 numpy.ndarray ((n, 3))
             [名称]['VELOCITY'] : 速度 numpy.ndarray ((n, 3))
    :rtype: dict
    """
    info = {}                   # 抽出結果格納用の辞書
    # 計算対象の時間一覧
    et_array = np.arange(PLOT_PERIOD_ET['START'], PLOT_PERIOD_ET['END'], DELTA_T)
    info['DATA_NUM'] = et_array.shape[0]
    info['ET_ARRAY'] = et_array
    info['DT_LIST'] = [spiceExt.et2datetime(et) for et in et_array]
    
    # 位置、速度の抽出
    print("SPK KERNELから位置、速度を抽出中 ... ", flush = True)
    body_names = [SUN_BODY, EARTH_BODY, MARS_BODY, HAYABUSA2_BODY, RYUGU_BODY]
    for targ in body_names:
        print("  {}の位置、速度を抽出中 ... ".format(targ), end = "", flush = True)
        pos_and_v = spiceExt.spkezr(targ = targ, et = et_array, ref = INERTIAL_FRAME,
                                    abcorr = "NONE", obs = SS_BARYCENTER)[0]
        # pos_and_v = spiceExt.spkezr(targ = targ, et = et_array, ref = INERTIAL_FRAME,
        #                             abcorr = "NONE", obs = SUN_BODY)[0]
        pos_v_dict = {'POS': pos_and_v[:, :3], 'VELOCITY': pos_and_v[:, 3:]}
        info[targ] = pos_v_dict
        print("完了")
    print("完了")
        
    return info


def extract_attitude(info):
    """
    はやぶさ2の姿勢データをCK Kernelから抽出する
    :return: 天体、探査機の情報を辞書形式で格納したもの
            ['HAYABUSA2']['C_MATRIX']:はやぶさ2の姿勢(座標変換行列形式)
            ['HAYABUSA2']['QUATERNION']:はやぶさ2の姿勢(クォータニオン形式)
    :rtype: dict
    """
    print("CK KERNELから、はやぶさ2の姿勢情報を抽出中 ... ", end = "", flush = True)
    hy2_m = spiceExt.pxform(HAYABUSA2_FRAME, INERTIAL_FRAME, info['ET_ARRAY'])
    hy2_q = spiceExt.m2q(hy2_m)
    info[HAYABUSA2_BODY]['C_MATRIX'] = hy2_m
    info[HAYABUSA2_BODY]['QUATERNION'] = hy2_q
    print("完了")
    return info
    

def calc_HY2Z_sun_angle(info):
    """
    はやぶさ2の+Z軸と太陽方向との関係を求める
    :param info: 抽出した位置、姿勢情報
    :type info: dict
    :return: infoに太陽に対するはやぶさ2+Z軸方位を加えたもの
    :rtype: dict
    """

    print("はやぶさ2の機体Z軸と太陽の角度を計算中 ... ", end = "", flush = True)
    n = info['DATA_NUM'] # 総データ数
    hy2z_sun_angle = np.zeros((n, 2))

    hy2_v_direction = (info[HAYABUSA2_BODY]['VELOCITY'] / # はやぶさ2の速度方向
                       norm(info[HAYABUSA2_BODY]['VELOCITY'], axis = 1).reshape((n, 1)))
    sun_direction_from_hy2 = -(info[HAYABUSA2_BODY]['POS'] / # はやぶさ2→太陽方向
                               norm(info[HAYABUSA2_BODY]['POS'], axis = 1).reshape((n, 1)))
    for idx in range(n):
        # はやぶさ2・太陽固定の座標系の定義
        hy2_sun_frame_y = sun_direction_from_hy2[idx, :] # はやぶさ2→太陽方向をy方向とする
        hy2_sun_frame_z = np.cross(hy2_v_direction[idx, :], hy2_sun_frame_y) # はやぶさ2公転面の法線をz方向とする
        hy2_sun_frame_z /= norm(hy2_sun_frame_z)
        hy2_sun_frame_x = np.cross(hy2_sun_frame_y, hy2_sun_frame_z) # x方向を再定義
        hy2_sun_frame = np.array([hy2_sun_frame_x, hy2_sun_frame_y, hy2_sun_frame_z]).T
        # はやぶさ2のZ軸の方向を、はやぶさ2・太陽固定座標系に変換
        hy2_zaxis = info[HAYABUSA2_BODY]['C_MATRIX'][idx,:,:].dot(np.array([0, 0, 1]))
        hy2_zaxis_hy2_sun_frame =inv(hy2_sun_frame).dot(hy2_zaxis)
        # 太陽に対する角度を計算
        hy2z_sun_angle[idx, 0] = 90 - np.degrees(np.arctan2(hy2_zaxis_hy2_sun_frame[1], hy2_zaxis_hy2_sun_frame[0]))
        hy2z_sun_angle[idx, 1] = np.degrees(np.arcsin(hy2_zaxis_hy2_sun_frame[2]))

    print("完了")
    info[HAYABUSA2_BODY]['SUN_ANGLE'] = hy2z_sun_angle
    return info


def calc_et():
    """
    datetime形式で設定したイベント日時情報にET形式の情報を追加する
    結果はグローバル変数に追記する
    """
    # プロット期間
    global PLOT_PERIOD_ET
    for k, v in PLOT_PERIOD.items():
        v_et = spiceExt.datetime2et(v)
        PLOT_PERIOD_ET[k] = v_et

    # イオンエンジン運転期間
    global IES_TIMING_ET
    for ies_t in IES_TIMING:
        ies_t_et = {}
        for k, v in ies_t.items():
            if k == 'THRUSTERS':
                ies_t_et[k] = v
                continue
            v_et = spiceExt.datetime2et(v)
            ies_t_et[k] = v_et
        IES_TIMING_ET.append(ies_t_et)

    # OWC期間
    global OWC_TIMING_ET
    for owc_t in OWC_TIMING:
        owc_t_et = [spiceExt.datetime2et(dt) for dt in owc_t]
        OWC_TIMING_ET.append(owc_t_et)

    # 地球近傍にいる期間
    global NEAR_EARTH_PERIOD_ET
    for period in NEAR_EARTH_PERIOD_DT:
        period_et = {}
        for k, v in period.items():
            period_et[k] = spiceExt.datetime2et(v)
        NEAR_EARTH_PERIOD_ET.append(period_et)

    # 地球スウィングバイ日時
    global HY2_SWING_BY_ET
    HY2_SWING_BY_ET = spiceExt.datetime2et(HY2_SWING_BY_DT)
        
    return


def plot_owc_attitude():
    for owc_t_et in OWC_TIMING_ET:
        idx = [np.searchsorted(info['ET_ARRAY'], et) for et in owc_t_et]
        plt.plot(info[HAYABUSA2_BODY]['SUN_ANGLE'][idx[0]:idx[-1], 0],
                 info[HAYABUSA2_BODY]['SUN_ANGLE'][idx[0]:idx[-1], 1])
        plt.show()


def calc_earth_sun_fix_pos(earth_pos, pos):
    """
    地球太陽固定系での座標を求める
    :param earth_pos: 慣性座標系での地球の位置
    :type earth_pos: numpy.ndarray
    :param pos: 慣性座標系での位置
    :type pos: numpy.ndarray
    :return: 地球太陽固定系での座標
    :rtype: numpy.ndarray
    """
    n = earth_pos.shape[0]      # 計算点数
    earth_azim = np.arctan2(earth_pos[:, 1], earth_pos[:, 0]) # 太陽から見た地球の方位角
    rot_mat = np.zeros((n, 3, 3))                             # 慣性座標系→地球太陽固定系への回転行列
    rot_mat[:, 0, 0] = np.cos(earth_azim)
    rot_mat[:, 0, 1] = np.sin(earth_azim)
    rot_mat[:, 1, 0] = -np.sin(earth_azim)
    rot_mat[:, 1, 1] = np.cos(earth_azim)
    rot_mat[:, 2, 2] = 1
    pos_rel_earth = pos - earth_pos # 地球との相対位置

    result = np.zeros((n, 3))
    for idx in range(n):
        result[idx, :] = rot_mat[idx, :, :].dot(pos_rel_earth[idx, :])
    return result


def calc_hy2_orbital_element(info):
    """
    はやぶさ2の軌道要素を求める
    :param info: 抽出した位置、速度情報
    :type info: dict
    :return: infoに各時刻の軌道要素を追加したもの
    :rtype:dict
    """

    print("各時刻におけるはやぶさ2の軌道要素を計算中 ... ", end = "", flush = True)
    hy2_oe = []                 # 求めた軌道要素オブジェクトを格納するリスト
    n = info['DATA_NUM']        # 計算点数

    pos_hy2_sun = info[HAYABUSA2_BODY]['POS'] - info[SUN_BODY]['POS']
    v_hy2_sun = info[HAYABUSA2_BODY]['VELOCITY'] - info[SUN_BODY]['VELOCITY']
    
    for idx in range(n):
        hy2_oe.append(oe.make_orbital_element_from_pos_v(pos = pos_hy2_sun[idx, :],
                                                         v = v_hy2_sun[idx, :],
                                                         dt = info['DT_LIST'][idx],
                                                         GM = G_SOURCES[0]['GM']))
    # 地球近傍にいる期間は、位置・速度から求めた軌道要素が不正確になるため修正する
    for period_et in NEAR_EARTH_PERIOD_ET:
        start_idx = np.searchsorted(info['ET_ARRAY'], period_et['START'])
        end_idx = np.searchsorted(info['ET_ARRAY'], period_et['END'])
        alt_idx = np.searchsorted(info['ET_ARRAY'], period_et['ALT'])
        for idx in range(start_idx, end_idx):
            hy2_oe[idx] = hy2_oe[alt_idx]
    info[HAYABUSA2_BODY]['ORBITAL_ELEMENT'] = hy2_oe
    print("完了")
    return info


def check_hy2_orbital_element(info):
    """
    計算した軌道要素の検算（デバッグ用）
    """

    n = len(info['DT_LIST'])

    pos_err = np.zeros((n, 3))
    v_err = np.zeros((n, 3))

    for idx in range(n):
        pos_v_from_oe = info[HAYABUSA2_BODY]['ORBITAL_ELEMENT'][idx].get_position_and_velocity(info['DT_LIST'][idx])
        pos_err[idx, :] = pos_v_from_oe[0].ravel() + info[SUN_BODY]['POS'][idx, :] - info[HAYABUSA2_BODY]['POS'][idx, :]
        v_err[idx, :] = pos_v_from_oe[1].ravel() + info[SUN_BODY]['VELOCITY'][idx, :] - info[HAYABUSA2_BODY]['VELOCITY'][idx, :]
    plt.plot(info['DT_LIST'], norm(pos_err, axis = 1))
    plt.show()
    plt.plot(info['DT_LIST'], norm(v_err, axis = 1))
    plt.show()


def predict_hy2_pos(info):
    """
    地球スウィングバイ時のはやぶさ2位置を予測する
    """
    n = info['DATA_NUM']
    predict_pos = np.zeros((n, 3))

    for idx, hy2_oe in enumerate(info[HAYABUSA2_BODY]['ORBITAL_ELEMENT']):
        predict_pos[idx, :] = (hy2_oe.get_position_and_velocity(HY2_SWING_BY_DT)[0].ravel() +
                               info[SUN_BODY]['POS'][idx, :])
    swing_by_idx = np.searchsorted(info['ET_ARRAY'], HY2_SWING_BY_ET)
    swing_by_pos = info[HAYABUSA2_BODY]['POS'][swing_by_idx, :].reshape((1, 3))

    pos_err = predict_pos - swing_by_pos

    plt.plot(pos_err[:, 0], pos_err[:, 1])
    plt.show()

    plt.plot(info['DT_LIST'][:], pos_err[:, 0])
    plt.plot(info['DT_LIST'][:], pos_err[:, 1])
    plt.show()


def calc_planet_orbital_shape(info):
    """
    惑星の軌道データ（描画用）を計算する
    """
    planet_name = [EARTH_BODY, MARS_BODY, RYUGU_BODY] # 軌道データを計算する惑星の一覧
    for obj in planet_name:
        pos_planet_sun = info[obj]['POS'][0, :] - info[SUN_BODY]['POS'][0, :]
        v_planet_sun = info[obj]['VELOCITY'][0, :] - info[SUN_BODY]['VELOCITY'][0, :]
        planet_oe = oe.make_orbital_element_from_pos_v(pos = pos_planet_sun, v = v_planet_sun,
                                                       dt = info['DT_LIST'][0], GM = G_SOURCES[0]['GM'])
        info[obj]['ORBITAL_ELEMENT'] = planet_oe
        orbital_shape = planet_oe.get_orbital_shape(300)
        info[obj]['ORBITAL_SHAPE'] = orbital_shape

    return info


def add_RW_status(info):
    """
    リアクションホイールの運転状態を記録する
    """
    rw_status = np.zeros((info['DATA_NUM'], 4), dtype = bool)
    rw_status[:, 0] = True
    rw_status[:, 1] = True
    rw_status[:, 2] = True
    rw_status[:, 3] = False
    for owc_period in OWC_TIMING_ET:
        start_idx = np.searchsorted(info['ET_ARRAY'], owc_period[0])
        end_idx = np.searchsorted(info['ET_ARRAY'], owc_period[-1])
        rw_status[start_idx:end_idx, 0] = False
        rw_status[start_idx:end_idx, 1] = False
    info[HAYABUSA2_BODY]['RW_STATUS'] = rw_status
    return info


def make_plot(info):
    """
    軌道図などのプロットを作成し、連番の画像として保存する
    :param info: 探査機、天体の情報を格納した辞書
    :type info: dict
    """

    # 描画の前準備
    fig = plt.figure(figsize = (12.8, 7.2), facecolor = (0,0,0,0))
    plt.style.use('dark_background')
    save_path = ".//{}//".format(PNG_FOLDER)      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    current_frame = 0
    start_idx = 0
    # start_idx = np.searchsorted(info['ET_ARRAY'], IES_TIMING_ET[0]['START']) # IES運転中のみのデバッグ用
    end_idx = info['DATA_NUM'] - 1
    step_idx = int(PLOT_DELTA_T / DELTA_T)

    # 事前計算
    # 描画レイアウトの準備
    lx = 1/16                   # レイアウト用の単位 実際野長さがlx = lyとなる
    ly = 1/9
    n = info['DATA_NUM']
    predict_pos = np.zeros((n, 3))
    for idx, hy2_oe in enumerate(info[HAYABUSA2_BODY]['ORBITAL_ELEMENT']):
        predict_pos[idx, :] = (hy2_oe.get_position_and_velocity(HY2_SWING_BY_DT)[0].ravel() +
                               info[SUN_BODY]['POS'][idx, :])
    swing_by_idx = np.searchsorted(info['ET_ARRAY'], HY2_SWING_BY_ET)
    ies_operating_time_hr = info[HAYABUSA2_BODY]['THRUSTER_OPERATING_TIME'] / 3600 # [sec]→[hr]

    for idx in range(start_idx, end_idx, step_idx): # 1コマずつ描画する
        # 保存ファイル名の決定
        current_frame += 1
        save_filename = "{}{}_{:05d}.png".format(save_path, PNG_FILENAME, current_frame)

        # 日時などのプロット
        date_ax = fig.add_axes((0*lx, 7*ly, 6*lx, 2*ly), frameon = False,
                               xticks = [], yticks = [])
        date_ax.set_xlim([0, 60])
        date_ax.set_ylim([0, 20])
        utc_dt = info['DT_LIST'][idx] # 協定世界時
        utc_str = utc_dt.strftime('%Y-%m-%d %H:%M:%S (UTC)')
        jst_dt = utc_dt + datetime.timedelta(hours = 9) # 日本標準時
        jst_str = jst_dt.strftime('%Y-%m-%d %H:%M:%S (JST)')
        l_num = (utc_dt - LAUNCH_DT).days
        date_ax.text(x = 30, y = 19, ha = 'center', va = 'top',
                     s = utc_str, color = (1,1,1), fontsize = 15)
        date_ax.text(x = 30, y = 13, ha = 'center', va = 'top',
                     s = jst_str, color = (1,1,1), fontsize = 15)
        date_ax.text(x = 30, y = 7, ha = 'center', va = 'top',
                     s = "L+{:04d} [Day]".format(l_num), color = (1,1,1), fontsize = 15)
        # 軌道図（上面図のプロット）
        top_view_ax = fig.add_axes((1*lx, 2.9*ly, 3.6*lx, 3.6*ly))
        top_view_ax.set_xlim((-3e8, 3e8))
        top_view_ax.set_ylim((-3e8, 3e8))
        top_view_ax.set_xticks([-3e8, -2e8, -1e8, 0, 1e8, 2e8, 3e8])
        top_view_ax.tick_params(labelbottom="off")
        top_view_ax.set_ylabel('Y [km]', fontsize = 15)
        planets = [EARTH_BODY, MARS_BODY, RYUGU_BODY]
        for obj in planets:
            top_view_ax.plot(info[obj]['ORBITAL_SHAPE'][:, 0], info[obj]['ORBITAL_SHAPE'][:, 1],
                             color = PLANET_COLORS[obj]['TRACK'], lw = 2, zorder = 10)
            top_view_ax.plot(info[obj]['POS'][idx, 0], info[obj]['POS'][idx, 1],
                             marker = 'o', markersize = MARKER_SIZE[obj], zorder = 20,
                             markerfacecolor = PLANET_COLORS[obj]['FACE'],
                             markeredgecolor = PLANET_COLORS[obj]['EDGE'])
        top_view_ax.plot([0], [0], zorder = 20, markersize = MARKER_SIZE[SUN_BODY], marker = 'o',
                         markerfacecolor = PLANET_COLORS[SUN_BODY]['FACE'],
                         markeredgecolor = PLANET_COLORS[SUN_BODY]['EDGE'])
        hy2_orbital_shape = info[HAYABUSA2_BODY]['ORBITAL_ELEMENT'][idx].get_orbital_shape(200)
        top_view_ax.plot(hy2_orbital_shape[:, 0], hy2_orbital_shape[:, 1],
                         color = HY2_COLOR['TRACK'], lw = 2, zorder = 30)
        top_view_ax.plot(info[HAYABUSA2_BODY]['POS'][idx, 0], info[HAYABUSA2_BODY]['POS'][idx, 1],
                         marker = 's', markersize = MARKER_SIZE[HAYABUSA2_BODY], zorder = 40,
                         markerfacecolor = HY2_COLOR['FACE_NOW'],
                         markeredgecolor = HY2_COLOR['EDGE_NOW'])
        if info[HAYABUSA2_BODY]['THRUSTER_USE'][idx, :].any():
            top_view_ax.arrow(info[HAYABUSA2_BODY]['POS'][idx, 0], info[HAYABUSA2_BODY]['POS'][idx, 1],
                              info[HAYABUSA2_BODY]['ACCERALATION'][idx, 0] * DELTA_V_SCALE,
                              info[HAYABUSA2_BODY]['ACCERALATION'][idx, 1] * DELTA_V_SCALE,
                              fc = DELTA_V_ARROW_COLOR, ec = DELTA_V_ARROW_COLOR, zorder = 35,
                              head_width = DELTA_V_ARROW_HEAD_WIDTH, width = DELTA_V_ARROW_WIDTH)
                          
        # 軌道図（側面図のプロット）
        side_view_ax = fig.add_axes((1*lx, 0.8*ly, 3.6*lx, 1.2*ly), sharex = top_view_ax)
        side_view_ax.set_ylim((-1e8, 1e8))
        side_view_ax.set_xlim((-3e8, 3e8))
        side_view_ax.set_yticks([-1e8, -0, 1e8])
        side_view_ax.set_xticks([-3e8, -2e8, -1e8, -0, 1e8, 2e8, 3e8])
        side_view_ax.set_xlabel('X [km]', fontsize = 15)
        side_view_ax.set_ylabel('Z [km]', fontsize = 15)
        for obj in planets:
            side_view_ax.plot(info[obj]['ORBITAL_SHAPE'][:, 0], info[obj]['ORBITAL_SHAPE'][:, 2],
                              color = PLANET_COLORS[obj]['TRACK'], lw = 2, zorder = 10)
            # side_view_ax.plot(info[obj]['POS'][idx, 0], info[obj]['POS'][idx, 2],
            #                   marker = 'o', markersize = MARKER_SIZE[obj], zorder = 20,
            #                   markerfacecolor = PLANET_COLORS[obj]['FACE'],
            #                   markeredgecolor = PLANET_COLORS[obj]['EDGE'])
        # side_view_ax.plot([0], [0], zorder = 20, markersize = MARKER_SIZE[SUN_BODY], marker = 'o',
        #                   markerfacecolor = PLANET_COLORS[SUN_BODY]['FACE'],
        #                   markeredgecolor = PLANET_COLORS[SUN_BODY]['EDGE'])
        side_view_ax.plot(hy2_orbital_shape[:, 0], hy2_orbital_shape[:, 2],
                         color = HY2_COLOR['TRACK'], lw = 2, zorder = 30)
        side_view_ax.plot(info[HAYABUSA2_BODY]['POS'][idx, 0], info[HAYABUSA2_BODY]['POS'][idx, 2],
                          marker = 's', markersize = MARKER_SIZE[HAYABUSA2_BODY], zorder = 40,
                          markerfacecolor = HY2_COLOR['FACE_NOW'],
                          markeredgecolor = HY2_COLOR['EDGE_NOW'])
        if info[HAYABUSA2_BODY]['THRUSTER_USE'][idx, :].any():
            side_view_ax.arrow(info[HAYABUSA2_BODY]['POS'][idx, 0], info[HAYABUSA2_BODY]['POS'][idx, 2],
                               info[HAYABUSA2_BODY]['ACCERALATION'][idx, 0] * DELTA_V_SCALE,
                               info[HAYABUSA2_BODY]['ACCERALATION'][idx, 2] * DELTA_V_SCALE,
                               fc = DELTA_V_ARROW_COLOR, ec = DELTA_V_ARROW_COLOR, zorder = 35,
                               head_width = DELTA_V_ARROW_HEAD_WIDTH, width = DELTA_V_ARROW_WIDTH)

        # 地球・太陽固定系での描画
        es_fix_ax = fig.add_axes((lx*12.5, ly*0.75, lx*3, ly*3))
        es_fix_ax.set_xlim((-4e7, 4e7))
        es_fix_ax.set_ylim((-6e7, 2e7))
        es_fix_ax.set_xticks([-4e7, -2e7, 0, 2e7, 4e7])
        es_fix_ax.set_yticks([-6e7, -4e7, -2e7, 0, 2e7])
        es_fix_ax.set_xlabel('X [km]', fontsize = 15)        
        es_fix_ax.set_ylabel('Y [km]', fontsize = 15)
        hy2_predict_pos = np.zeros((swing_by_idx + 1, 3))
        if idx < swing_by_idx:  # スウィングバイ以前ならばスウィングバイまでの予測軌道を計算、表示する
            for iidx in range(idx, swing_by_idx + 1):
                hy2_predict_pos[iidx, :] = (info[HAYABUSA2_BODY]['ORBITAL_ELEMENT'][idx].get_position(info['DT_LIST'][iidx]).ravel()
                                            + info[SUN_BODY]['POS'][iidx, :])
            hy2_predict_pos_esfix = calc_earth_sun_fix_pos(info[EARTH_BODY]['POS'][idx:swing_by_idx+1, :], hy2_predict_pos[idx:swing_by_idx+1, :])
            es_fix_ax.plot(hy2_predict_pos_esfix[:, 0], hy2_predict_pos_esfix[:, 1], zorder = 10,
                           lw = 2, linestyle = ':', color = HY2_COLOR['TRACK_PREDICT'])
            es_fix_ax.plot(hy2_predict_pos_esfix[-1, 0], hy2_predict_pos_esfix[-1, 1], zorder = 20,
                           marker = 's', markersize = MARKER_SIZE[HAYABUSA2_BODY],
                           markerfacecolor = HY2_COLOR['FACE_PREDICT'], markeredgecolor = HY2_COLOR['EDGE_PREDICT'])
        es_fix_ax.plot(info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][:idx, 0], info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][:idx, 1],
                       lw = 2, color = HY2_COLOR['TRACK_OLD'], zorder = 10)
        es_fix_ax.plot(info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][idx, 0], info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][idx, 1],
                       marker = 's', markersize = MARKER_SIZE[HAYABUSA2_BODY], zorder = 30,
                       markerfacecolor = HY2_COLOR['FACE_NOW'], markeredgecolor = HY2_COLOR['EDGE_NOW'])
        es_fix_ax.plot([0], [0], markersize = MARKER_SIZE[EARTH_BODY], zorder = 15, marker = 'o',
                       markerfacecolor = PLANET_COLORS[EARTH_BODY]['FACE'],
                       markeredgecolor = PLANET_COLORS[EARTH_BODY]['EDGE'])
        # イオンエンジン・RWの運転状態の描画
        status_ax = fig.add_axes((11.5*lx, 3.5*ly, 4.5*lx, 5.5*ly), frameon = False,
                               xticks = [], yticks = [])
        status_ax.set_xlim([0, 45])
        status_ax.set_ylim([0, 55])
        status_ax.text(x = 25, y = 50, ha ='center', va = 'center',
                       s = 'Ion Engine Status', color = (1,1,1), fontsize = 15)
        for ies_num in range(4):
            if info[HAYABUSA2_BODY]['THRUSTER_USE'][idx, ies_num]:
                textcolor = IES_TEXT_COLORS[ies_num]['TEXT']
                facecolor = IES_TEXT_COLORS[ies_num]['FACE']
                symbol_facecolor = IES_SYMBOL_COLORS[ies_num]['FACE']
                symbol_edgecolor = IES_SYMBOL_COLORS[ies_num]['EDGE']
            else:
                textcolor = IES_TEXT_COLORS[-1]['TEXT']
                facecolor = IES_TEXT_COLORS[-1]['FACE']
                symbol_facecolor = IES_SYMBOL_COLORS[-1]['FACE']
                symbol_edgecolor = IES_SYMBOL_COLORS[-1]['EDGE']
            status_ax.add_patch(plt.Rectangle(xy = ((IES_TEXT_COLORS[ies_num]['POS'][0] - IES_TEXT_BOX_WH[0] / 2),
                                                    (IES_TEXT_COLORS[ies_num]['POS'][1] - IES_TEXT_BOX_WH[1] / 2)),
                                              facecolor = facecolor, zorder = 10,
                                              width = IES_TEXT_BOX_WH[0], height = IES_TEXT_BOX_WH[1]))
            status_ax.text(s = "{}\n{:3.1f} Hr.".format(IES_TEXT_COLORS[ies_num]['NAME'], ies_operating_time_hr[idx, ies_num]),
                           x = IES_TEXT_COLORS[ies_num]['POS'][0], y = IES_TEXT_COLORS[ies_num]['POS'][1],
                           fontsize = 12, color = textcolor, ha = 'center', va = 'center', zorder = 20)
            status_ax.add_patch(plt.Circle(radius = 1.5, zorder = 20,
                                            xy = (IES_SYMBOL_COLORS[ies_num]['POS'][0], IES_SYMBOL_COLORS[ies_num]['POS'][1]),
                                            facecolor = symbol_facecolor, edgecolor = symbol_edgecolor))

        status_ax.add_patch(plt.Rectangle(xy = (20, 33), width = 10, height = 10,
                                           facecolor = (0.25, 0.25, 0.25), edgecolor = (0.84, 0.84, 0.84), lw = 2, zorder = 10))
        status_ax.text(x = 25, y = 25, color = (1,1,1), fontsize = 12, ha = 'center', va = 'center',
                       s = 'Total Delta V {:2.1f}[m/s]'.format(info[HAYABUSA2_BODY]['TOTAL_DELTA_V'][idx]))
        status_ax.text(x = 25, y = 20, ha ='center', va = 'center',
                       s = 'Reaction Wheels Status', color = (1,1,1), fontsize = 15)
        for rw_num in range(4):
            if info[HAYABUSA2_BODY]['RW_STATUS'][idx, rw_num]:
                textcolor = RW_TEXT_COLORS[0]['TEXT']
                facecolor = RW_TEXT_COLORS[0]['FACE']
            else:
                textcolor = RW_TEXT_COLORS[1]['TEXT']
                facecolor = RW_TEXT_COLORS[1]['FACE']
            status_ax.add_patch(plt.Rectangle(xy = ((RW_TEXT_INFO[rw_num]['POS'][0] - RW_TEXT_BOX_WH[0] / 2),
                                                    (RW_TEXT_INFO[rw_num]['POS'][1] - RW_TEXT_BOX_WH[1] / 2)),
                                              width = RW_TEXT_BOX_WH[0], height = RW_TEXT_BOX_WH[1],
                                              facecolor = facecolor, zorder = 10))
            status_ax.text(x = RW_TEXT_INFO[rw_num]['POS'][0], y = RW_TEXT_INFO[rw_num]['POS'][1],
                           s = RW_TEXT_INFO[rw_num]['TEXT'], fontsize = 12, ha = 'center', va = 'center',
                           color = textcolor, zorder = 20)
                           
                
            
        # 画像の保存
        plt.savefig(save_filename, transparent = True)
        plt.clf()
    return


def make_sun_angle_plot(info):
    # 描画の前準備
    fig = plt.figure(figsize = (3.6, 3.6), facecolor = (0,0,0,0))
    plt.style.use('dark_background')
    save_path = ".//{}//".format(SUN_ANGLE_PNG_FOLDER)      # 保存先のパス
    if not os.path.exists(save_path):
        print("画像を出力するディレクトリが存在しないので作成します\n")
        os.makedirs(save_path)

    for owc_num, period in enumerate(OWC_TIMING_ET):
        event_idx = []
        data_per_plot = int(PLOT_DELTA_T / DELTA_T)
        for event_et in period:
            idx = np.searchsorted(info['ET_ARRAY'], event_et)
            # idx = int(idx / data_per_plot) * data_per_plot
            event_idx.append(idx)
        event_idx[0] = int(event_idx[0] / data_per_plot) * data_per_plot
        event_idx[-1] = int(event_idx[-1] / data_per_plot) * data_per_plot
        print(event_idx)
        plot_colors = np.zeros((info['DATA_NUM'], 4))
        plot_idx_range = int(SUN_ALGLE_HOLD_PERIOD / DELTA_T)
        for attitude_num in range(len(event_idx) - 1):
            plot_colors[event_idx[attitude_num]:event_idx[attitude_num + 1],0:3] = np.array(SUN_ANGLE_PLOT_COLORS[attitude_num % len(SUN_ANGLE_PLOT_COLORS)])
        current_frame = 0
        for idx in range(event_idx[0], event_idx[-1], data_per_plot):
            current_frame += 1
            save_filename = '{}{}_{}_{:05d}.png'.format(save_path, SUN_ANGLE_PNG_FILENAME, owc_num, current_frame)
            print(save_filename)
            ax = fig.add_axes((0.2, 0.2, 0.75, 0.75))
            ax.set_xlim((-10, 10))
            ax.set_ylim((-10, 10))
            ax.set_xlabel('[deg]')
            ax.set_ylabel('[deg]')
            ax.plot([-10,10],[0,0], color = (0.5, 0.5, 0.5), lw = 1)
            ax.plot([0,0],[-10,10], color = (0.5, 0.5, 0.5), lw = 1)
            for plot_idx in range(idx - plot_idx_range, idx + 1):
                markercolor = [rgb for rgb in plot_colors[plot_idx, :]]
                if plot_idx > event_idx[0]:
                    markercolor[-1] = (1 - (idx - plot_idx) / plot_idx_range)
                ax.plot(info[HAYABUSA2_BODY]['SUN_ANGLE'][plot_idx-1:plot_idx+1, 0],
                         info[HAYABUSA2_BODY]['SUN_ANGLE'][plot_idx-1:plot_idx+1, 1],
                         color = markercolor, lw = 1)
            plt.savefig(save_filename, transparent = True)
            plt.clf()
    return



def add_earth_sun_fix_pos(info):
    # 各天体の地球、太陽固定座標系での位置を求める
    obj_list = [HAYABUSA2_BODY, MARS_BODY, RYUGU_BODY]
    for obj in obj_list:
        info[obj]['EARTH_SUN_FIX'] = calc_earth_sun_fix_pos(info[EARTH_BODY]['POS'], info[obj]['POS'])

    return info


def calc_q_vec_to_vec(from_vec, to_vec):
    """
    from_vecをto_vecに移す回転操作のクォータニオンを求める
    :param from_vec: 回転操作前のベクトル
    :type from_vec: numpy.ndarray
    :param to_vec: 回転操作後のベクトル
    :type to_vec: numpy.ndarray
    :return: 回転操作を表すクォータニオン
    :rtpye: numpy.ndarray
    """
    # from_vec, to_vecの次元をそろえる
    if from_vec.ndim == 1:
        from_vec = from_vec.reshape((1, from_vec.shape[0]))
    if to_vec.ndim == 1:
        to_vec = to_vec.reshape((1, to_vec.shape[0]))

    if from_vec.shape[1] != 3:
        print('from_vecの次元{}が正しくありません'.format(from_vec.shape))
    if to_vec.shape[1] != 3:
        print('to_vecの次元{}が正しくありません'.format(to_vec.shape))
    if from_vec.shape[0] == 1 and to_vec.shape[0] != 1:
        from_vec_temp = np.zeros(to_vec.shape)
        from_vec_temp[:, :] = from_vec[0, :]
        from_vec = from_vec_temp
    elif to_vec.shape[0] == 1 and from_vec.shape[0] != 1:
        to_vec_temp = np.zeros(from_vec.shape)
        to_vec_temp[:, :] = to_vec[0, :]
        to_vec = to_vec_temp
    if from_vec.shape != to_vec.shape:
        print('from_vecの次元{}とto_vecの次元{}が整合していません'.format(from_vec.shape, to_vec.shape))

    # 回転操作をクォータニオンで記述する
    rot_q = np.zeros((from_vec.shape[0], 4)) # 計算結果(クォータニオンを格納する配列）

    from_vec /= norm(from_vec, axis = 1).reshape((from_vec.shape[0], 1))  # 長さを1に正規化
    to_vec /= norm(to_vec, axis = 1).reshape((to_vec.shape[0], 1))

    angle = np.arccos(np.sum(from_vec * to_vec, axis = 1)) # fromとtoの為す角（内積のarccos）
    rot_q[:, 0] = np.cos(angle / 2)                        # クォータニオンの第一項はcos theta/2
    for idx in range(rot_q.shape[0]):
        rot_axis = np.cross(from_vec[idx, :], to_vec[idx, :]) # 回転軸の向き
        rot_axis /= norm(rot_axis)
        rot_q[idx, 1:4] = rot_axis * np.sin(angle[idx] / 2) # 第2項から4項は回転軸の向き * sin theta/2

    return rot_q
                                     

def output_csv_for_blender(info):
    """
    blender用にCSVファイルを書き出す
    """
    # 軌道面の回転操作を計算する（軌道要素を利用する）
    op_normal = np.zeros((info['DATA_NUM'], 3)) # 軌道面の法線方向
    for idx, hy2_oe in enumerate(info[HAYABUSA2_BODY]['ORBITAL_ELEMENT']):
        rot_node = np.array([[np.cos(hy2_oe.ascending_node_rad), -np.sin(hy2_oe.ascending_node_rad), 0],
                             [np.sin(hy2_oe.ascending_node_rad), np.cos(hy2_oe.ascending_node_rad), 0],
                             [0, 0, 1]])
        rot_incl = np.array([[1, 0, 0],
                             [0, np.cos(hy2_oe.inclination_rad), -np.sin(hy2_oe.inclination_rad)],
                             [0, np.sin(hy2_oe.inclination_rad), np.cos(hy2_oe.inclination_rad)]])
        op_normal[idx, :] = rot_node.dot(rot_incl.dot(np.array([[0],[0], [1]]))).ravel()
        
    op_q = calc_q_vec_to_vec(from_vec = np.array([0,0,1]), to_vec = op_normal)

    # 太陽方向の回転操作を計算する
    sun_q = calc_q_vec_to_vec(from_vec = np.array([1,0,0]), to_vec = -info[HAYABUSA2_BODY]['POS'])

    # 軌道進行方向の回転操作を計算する
    vel_q = calc_q_vec_to_vec(from_vec = np.array([1,0,0]), to_vec = info[HAYABUSA2_BODY]['VELOCITY'])
    
    # ファイルに書き出す
    print("Blender用の情報を{}に書き出し中 ... ".format(CSV_FILENAMER_FOR_BLENDER), end = "", flush = True)
    with open(CSV_FILENAMER_FOR_BLENDER, "w") as output_file:
        # ヘッダ行の出力
        output_file.write('date,hayabusa2 pos,,,hayabusa2 quaternion,,,,')
        output_file.write('orbital_plane pos,,,orbital_plane quaternion,,,,sun pos,,,sun quaternion,,,,')
        output_file.write('velocity pos,,,velocity quaternion,,,,ion thruster,,,\n')
        step_idx = int(PLOT_DELTA_T / DELTA_T)
        for idx in range(0, info['DATA_NUM'], step_idx):
            output_file.write(info['DT_LIST'][idx].strftime("%Y-%m-%d %H:%M:%S"))
            for pos in info[HAYABUSA2_BODY]['POS'][idx, :]:
                output_file.write(",{:.5e}".format(pos))
            for q in info[HAYABUSA2_BODY]['QUATERNION'][idx, :]:
                output_file.write(",{:.5e}".format(q))
            output_file.write(",0,0,0")
            for q in op_q[idx, :]:
                output_file.write(",{:5e}".format(q))
            output_file.write(",0,0,0")
            for q in sun_q[idx, :]:
                output_file.write(",{:5e}".format(q))
            output_file.write(",0,0,0")
            for q in vel_q[idx, :]:
                output_file.write(",{:5e}".format(q))
            for ies in info[HAYABUSA2_BODY]['THRUSTER_USE'][idx, :]:
                if ies:
                    output_file.write(",1")
                else:
                    output_file.write(",0")
            output_file.write("\n")
    print("完了")
    return
    
    
if __name__ == "__main__":
    # SPICE KERNELを読み込む
    print("SPICE KERNELを読み込み中 ... ", end = "", flush = True)
    os.chdir(MK_ABS_PATH)
    spice.furnsh(MK_FILENAME)   # はやぶさ2、惑星の情報読み込み
    os.chdir(CWD)
    spice.furnsh(RYUGU_SPK)     # リュウグウの情報読み込み
    print("完了")

    calc_et()                   # datetime形式の日時データをET形式に変換しておく
    
    # 天体、探査機の位置、速度、姿勢情報をSpice Kernelから抽出
    info = extract_pos_v()
    info = extract_attitude(info)
    info = calc_HY2Z_sun_angle(info)
    info = calc_hy2_orbital_element(info)
    info = add_earth_sun_fix_pos(info)
    info = add_RW_status(info)
    info = calc_delta_v(info)
    info = calc_planet_orbital_shape(info)
    # make_plot(info)
    make_sun_angle_plot(info)


    # output_csv_for_blender(info)
    # predict_hy2_pos(info)
    # check_hy2_orbital_element(info)
    # info = calc_delta_v(info)
    
    # plot_owc_attitude()
    # info[HAYABUSA2_BODY]['EARTH_SUN_FIX'] = calc_earth_sun_fix_pos(info[EARTH_BODY]['POS'], info[HAYABUSA2_BODY]['POS'])
    # plt.plot(info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][:, 0], info[HAYABUSA2_BODY]['EARTH_SUN_FIX'][:, 1])
    # plt.show()
    
